// To parse this JSON data, do
//
//     final statusType0 = statusType0FromJson(jsonString);

import 'package:dac/services/base_service.dart';
import 'package:meta/meta.dart';

StatusType1 statusType1FromJsonMap(Map<String, dynamic> map) =>
    StatusType1.fromJson(map);

class StatusType1 {
  StatusType1({
    @required this.status,
    @required this.coords,
    @required this.speeds,
    @required this.currentTool,
    @required this.params,
    @required this.seq,
    @required this.sensors,
    @required this.temps,
    @required this.time,
  });

  final String status;
  final Coords coords;
  final Speeds speeds;
  final int currentTool;
  final Params params;
  final int seq;
  final Sensors sensors;
  final Temps temps;
  final double time;

  factory StatusType1.fromJson(Map<String, dynamic> json) => StatusType1(
        status: json["status"] == null ? null : json["status"],
        coords: json["coords"] == null ? null : Coords.fromJson(json["coords"]),
        speeds: json["speeds"] == null ? null : Speeds.fromJson(json["speeds"]),
        currentTool: json["currentTool"] == null ? null : json["currentTool"],
        params: json["params"] == null ? null : Params.fromJson(json["params"]),
        seq: json["seq"] == null ? null : json["seq"],
        sensors:
            json["sensors"] == null ? null : Sensors.fromJson(json["sensors"]),
        temps: json["temps"] == null ? null : Temps.fromJson(json["temps"]),
        time: cast<double>(json["time"]),
      );
}

class Coords {
  Coords({
    @required this.axesHomed,
    @required this.wpl,
    @required this.xyz,
    @required this.machine,
    @required this.extr,
  });

  final List<int> axesHomed;
  final int wpl;
  final List<double> xyz;
  final List<double> machine;
  final List<double> extr;

  factory Coords.fromJson(Map<String, dynamic> json) => Coords(
        axesHomed: json["axesHomed"] == null
            ? null
            : List<int>.from(json["axesHomed"].map((x) => x)),
        wpl: json["wpl"] == null ? null : json["wpl"],
        xyz: json["xyz"] == null
            ? null
            : List<double>.from(json["xyz"].map((x) => x.toDouble())),
        machine: json["machine"] == null
            ? null
            : List<double>.from(json["machine"].map((x) => x.toDouble())),
        extr: json["extr"] == null
            ? null
            : List<double>.from(json["extr"].map((x) => x.toDouble())),
      );
}

class Params {
  Params({
    @required this.atxPower,
    @required this.fanPercent,
    @required this.fanNames,
    @required this.speedFactor,
    @required this.extrFactors,
    @required this.babystep,
  });

  final int atxPower;
  final List<int> fanPercent;
  final List<String> fanNames;
  final double speedFactor;
  final List<double> extrFactors;
  final double babystep;

  factory Params.fromJson(Map<String, dynamic> json) => Params(
        atxPower: json["atxPower"] == null ? null : json["atxPower"],
        fanPercent: json["fanPercent"] == null
            ? null
            : List<int>.from(json["fanPercent"].map((x) => x)),
        fanNames: json["fanNames"] == null
            ? null
            : List<String>.from(json["fanNames"].map((x) => x)),
        speedFactor: cast<double>(json["speedFactor"]),
        extrFactors: json["extrFactors"] == null
            ? null
            : List<double>.from(
                json["extrFactors"].map((x) => cast<double>(x))),
        babystep: cast<double>(json["babystep"]),
      );
}

class Sensors {
  Sensors({
    @required this.probeValue,
    @required this.fanRpm,
  });

  final int probeValue;
  final List<int> fanRpm;

  factory Sensors.fromJson(Map<String, dynamic> json) => Sensors(
        probeValue: json["probeValue"] == null ? null : json["probeValue"],
        fanRpm: json["fanRPM"] == null
            ? null
            : List<int>.from(json["fanRPM"] is int
                ? [json["fanRPM"]]
                : json["fanRPM"].map((x) => x)),
      );
}

class Speeds {
  Speeds({
    @required this.requested,
    @required this.top,
  });

  final double requested;
  final double top;

  factory Speeds.fromJson(Map<String, dynamic> json) => Speeds(
        requested: cast<double>(json["requested"]),
        top: cast<double>(json["top"]),
      );
}

class Temps {
  Temps({
    @required this.tempsBed,
    @required this.current,
    @required this.state,
    @required this.tempsTools,
    @required this.extra,
  });

  final TempsBed tempsBed;
  final List<double> current;
  final List<int> state;
  final TempsTools tempsTools;
  final List<TempsExtra> extra;

  factory Temps.fromJson(Map<String, dynamic> json) => Temps(
        tempsBed: json["bed"] == null ? null : TempsBed.fromJson(json["bed"]),
        current: json["current"] == null
            ? null
            : List<double>.from(json["current"].map((x) => x.toDouble())),
        state: json["state"] == null
            ? null
            : List<int>.from(json["state"].map((x) => x)),
        tempsTools:
            json["tools"] == null ? null : TempsTools.fromJson(json["tools"]),
        extra: json["extra"] == null
            ? null
            : List<TempsExtra>.from(
                json["extra"].map((x) => TempsExtra.fromJson(x))),
      );
}

class TempsBed {
  TempsBed({
    @required this.current,
    @required this.active,
    @required this.standby,
    @required this.state,
    @required this.heater,
  });

  final double current;
  final double active;
  final double standby;
  final int state;
  final int heater;

  factory TempsBed.fromJson(Map<String, dynamic> json) => TempsBed(
        current: cast<double>(json["current"]),
        active: cast<double>(json["active"]),
        standby: cast<double>(json["standby"]),
        state: cast<int>(json["state"]),
        heater: cast<int>(json["heater"]),
      );
}

class TempsExtra {
  TempsExtra({
    @required this.name,
    @required this.temp,
  });

  final String name;
  final double temp;

  factory TempsExtra.fromJson(Map<String, dynamic> json) => TempsExtra(
        name: json["name"] == null ? null : json["name"],
        temp: cast<double>(json["temp"]),
      );
}

class TempsTools {
  TempsTools({
    @required this.active,
    @required this.standby,
  });

  final List<List<double>> active;
  final List<List<double>> standby;

  factory TempsTools.fromJson(Map<String, dynamic> json) => TempsTools(
        active: json["active"] == null
            ? null
            : List<List<double>>.from(json["active"]
                .map((x) => List<double>.from(x.map((x) => cast<double>(x))))),
        standby: json["standby"] == null
            ? null
            : List<List<double>>.from(json["standby"]
                .map((x) => List<double>.from(x.map((x) => cast<double>(x))))),
      );
}
