import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings {
  final bool watchEnabled;
  final int printerUpdateIntervalSeconds;
  final int lastSpace;

  factory Settings.fromSharedPreferences(SharedPreferences prefs) {
    return Settings(
      watchEnabled: prefs.getBool("watchEnabled") ?? false,
      printerUpdateIntervalSeconds:
          prefs.getInt("printerUpdateIntervalSeconds") ?? 5,
      lastSpace: prefs.getInt("lastSpace"),
    );
  }

  const Settings({
    @required this.watchEnabled,
    @required this.printerUpdateIntervalSeconds,
    @required this.lastSpace,
  });

  void saveSharedPreferences(SharedPreferences prefs) {
    prefs.setBool("watchEnabled", watchEnabled);
    prefs.setInt("printerUpdateIntervalSeconds", printerUpdateIntervalSeconds);
    prefs.setInt("lastSpace", lastSpace);
  }

  Settings copyWith({
    bool watchEnabled,
    int printerUpdateIntervalSeconds,
    int lastSpace,
  }) {
    if ((watchEnabled == null || identical(watchEnabled, this.watchEnabled)) &&
        (printerUpdateIntervalSeconds == null ||
            identical(printerUpdateIntervalSeconds,
                this.printerUpdateIntervalSeconds)) &&
        (lastSpace == null || identical(lastSpace, this.lastSpace))) {
      return this;
    }

    return new Settings(
      watchEnabled: watchEnabled ?? this.watchEnabled,
      printerUpdateIntervalSeconds:
          printerUpdateIntervalSeconds ?? this.printerUpdateIntervalSeconds,
      lastSpace: lastSpace ?? this.lastSpace,
    );
  }

  Settings copyWithSettings(Settings settings) {
    return copyWith(
        watchEnabled: settings.watchEnabled,
        printerUpdateIntervalSeconds: settings.printerUpdateIntervalSeconds,
        lastSpace: settings.lastSpace);
  }

  static Settings empty() {
    return Settings(
        watchEnabled: null,
        printerUpdateIntervalSeconds: null,
        lastSpace: null);
  }
}
