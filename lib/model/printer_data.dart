import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_job.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PrinterData extends Equatable {
  final Printer printer;
  final MachineStatus status;
  final Move move;
  final Bed bed;
  final List<Tool> tools;
  final List<Fan> fans;
  final Sensors sensors;
  final State state;
  final JobStatus jobStatus;

  const PrinterData(
    this.printer, {
    @required this.status,
    @required this.move,
    @required this.bed,
    @required this.tools,
    @required this.fans,
    @required this.sensors,
    @required this.state,
    @required this.jobStatus,
  });

  @override
  List<Object> get props =>
      [printer, status, move, bed, tools, fans, sensors, state, jobStatus];

  PrinterData copyWith(
    Printer printer, {
    MachineStatus status,
    Move move,
    Bed bed,
    List<Tool> tools,
    List<Fan> fans,
    Sensors sensors,
    State state,
    JobStatus jobStatus,
  }) {
    return new PrinterData(
      printer,
      status: status ?? this.status,
      move: move ?? this.move,
      bed: bed ?? this.bed,
      tools: tools ?? this.tools,
      fans: fans ?? this.fans,
      sensors: sensors ?? this.sensors,
      state: state ?? this.state,
      jobStatus: jobStatus ?? this.jobStatus,
    );
  }
}

class Fan extends Equatable {
  final int rpm;
  final int percent;

  Fan(this.rpm, this.percent);

  @override
  List<Object> get props => [rpm, percent];

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'rpm': this.rpm,
      'percent': this.percent,
    } as Map<String, dynamic>;
  }

  // String toJson() => json.encode(this.toMap());
}

enum HeaterState { OFF, STANDBY, ACTIVE, FAULT, TUNING, OFFLINE }

class Move extends Equatable {
  final List<Axis> axes;
  final MoveSpeed currentMove;
  final List<Extruder> extruders;
  final double speedFactor;

  Move(this.axes, this.currentMove, this.extruders, this.speedFactor);

  @override
  List<Object> get props => [axes, currentMove, extruders, speedFactor];
}

class Axis extends Equatable {
  final int drive;
  final bool homed;
  final double machinePosition;
  final double userPosition;

  Axis(this.drive, this.homed, this.machinePosition, this.userPosition);

  @override
  List<Object> get props => [drive, homed, machinePosition, userPosition];
}

class MoveSpeed extends Equatable {
  final double requestedSpeed;
  final double topSpeed;

  MoveSpeed(this.requestedSpeed, this.topSpeed);

  @override
  List<Object> get props => [requestedSpeed, topSpeed];
}

class Extruder extends Equatable {
  final int drive;
  final double factor;
  final double position;

  Extruder(this.drive, this.factor, this.position);

  @override
  List<Object> get props => [drive, factor, position];
}

class Sensors extends Equatable {
  final List<SensorAnalog> analog;
  final List<SensorProbe> probes;

  Sensors(this.analog, this.probes);

  @override
  List<Object> get props => [analog, probes];
}

class SensorAnalog extends Equatable {
  final double lastReading;
  final String name;

  SensorAnalog(this.lastReading, this.name);

  @override
  List<Object> get props => [lastReading, name];
}

class SensorProbe extends Equatable {
  final String value;

  SensorProbe(this.value);

  @override
  List<Object> get props => [value];
}

class State extends Equatable {
  final bool atxPower;
  final int currentTool;

  State(this.atxPower, this.currentTool);

  @override
  List<Object> get props => [atxPower, currentTool];
}

enum MachineStatus {
  READING_CONFIGURATION,
  FLASHING_FIRMWARE,
  HALTED,
  OFF,
  PAUSING_PRINT,
  RESUMING_PRINT,
  PRINT_PAUSED,
  SIMULATING_PRINT,
  PRINTING,
  CHANGING_TOOL,
  BUSY,
  IDLE
}

class Tool extends Equatable {
  final int toolNumber;
  final List<Heater> heaters;
  final List<TemperatureSet> temperaturesSets;

  Tool(this.toolNumber, this.heaters, this.temperaturesSets);

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'toolNumber': this.toolNumber,
      'heaters': this.heaters == null
          ? null
          : List<dynamic>.from(heaters.map((x) => x.toMap())),
      'temperaturesSets': this.temperaturesSets == null
          ? null
          : List<dynamic>.from(temperaturesSets.map((x) => x.toMap())),
    } as Map<String, dynamic>;
  }

  @override
  List<Object> get props => [toolNumber, heaters, temperaturesSets];
}

class Bed extends Equatable {
  final Heater heater;
  final TemperatureSet temperatureSet;

  Bed(this.heater, this.temperatureSet);

  @override
  List<Object> get props => [heater, temperatureSet];

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {'heater': this.heater, 'temperatureSet': this.temperatureSet}
        as Map<String, dynamic>;
  }
}

class Heater extends Equatable {
  final int number;
  final double current;
  final HeaterState state;

  Heater(this.number, this.current, this.state);

  @override
  List<Object> get props => [current, state, number];

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'number': this.number,
      'current': this.current,
      'state': describeEnum(this.state),
    } as Map<String, dynamic>;
  }
}

class TemperatureSet extends Equatable {
  final int heaterNumber;
  final double active;
  final double standby;

  TemperatureSet(this.heaterNumber, this.active, this.standby);

  @override
  List<Object> get props => [heaterNumber, active, standby];

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'heaterNumber': this.heaterNumber,
      'active': this.active,
      'standby': this.standby,
    } as Map<String, dynamic>;
  }
}

class PrinterInfo extends Equatable {
  final Printer printer;
  final int controllableFans;
  final double temperatureLimit;
  final int endstops;
  final String firmwareName;
  final String firmwareVersion;
  final String geometry;
  final int axes;
  final String axisNames;
  final int totalAxes;
  final int volumes;
  final int mountedVolumes;
  final String mode;
  final String name;
  final List<ToolInfo> tools;

  const PrinterInfo(
    this.printer, {
    @required this.controllableFans,
    @required this.temperatureLimit,
    @required this.endstops,
    @required this.firmwareName,
    @required this.firmwareVersion,
    @required this.geometry,
    @required this.axes,
    @required this.axisNames,
    @required this.totalAxes,
    @required this.volumes,
    @required this.mountedVolumes,
    @required this.mode,
    @required this.name,
    @required this.tools,
  });

  @override
  List<Object> get props => [
        printer,
        controllableFans,
        temperatureLimit,
        endstops,
        firmwareName,
        firmwareVersion,
        geometry,
        axes,
        axisNames,
        totalAxes,
        volumes,
        mountedVolumes,
        mode,
        name,
        tools,
      ];

  @override
  String toString() {
    return 'PrinterInfo{printer: $printer, firmwareName: $firmwareName, firmwareVersion: $firmwareVersion, geometry: $geometry, mode: $mode, name: $name}';
  }
}

class ToolInfo extends Equatable {
  final int number;
  final String name;
  final List<int> heaters;
  final List<int> drives;
  final int fans;
  final String filament;
  final List<double> offsets;

  @override
  List<Object> get props => [number, name];

  const ToolInfo({
    @required this.number,
    @required this.name,
    @required this.heaters,
    @required this.drives,
    @required this.fans,
    @required this.filament,
    @required this.offsets,
  });

  @override
  String toString() {
    return 'ToolInfo{number: $number, name: $name, heaters: $heaters}';
  }
}

class MCUTemp extends Equatable {
  final double minimum;
  final double current;
  final double maximum;

  MCUTemp(this.minimum, this.current, this.maximum);
  @override
  List<Object> get props => [minimum, current, maximum];
}

class VoltageIn extends Equatable {
  final double minimum;
  final double current;
  final double maximum;

  VoltageIn(this.minimum, this.current, this.maximum);
  @override
  List<Object> get props => [minimum, current, maximum];
}
