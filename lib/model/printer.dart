import 'package:dac/model/base_record.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class Printer extends Equatable implements BaseRecord {
  final int id;
  final String hostname;
  final String name;
  final int version;
  final bool enabled;

  const Printer({
    @required this.id,
    @required this.hostname,
    @required this.name,
    @required this.version,
    @required this.enabled,
  });

  @override
  List<Object> get props => [id];

  @override
  String toString() {
    return 'Printer{name: $name, hostname: $hostname, version: $version, enabled: $enabled }';
  }

  Printer copyWith({
    int id,
    String hostname,
    String name,
    int version,
    bool enabled,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (hostname == null || identical(hostname, this.hostname)) &&
        (name == null || identical(name, this.name)) &&
        (version == null || identical(version, this.version)) &&
        (enabled == null || identical(enabled, this.enabled))) {
      return this;
    }

    return new Printer(
      id: id ?? this.id,
      hostname: hostname ?? this.hostname,
      name: name ?? this.name,
      version: version ?? this.version,
      enabled: enabled ?? this.enabled,
    );
  }

  @override
  Printer setId(int id) {
    return copyWith(id: id);
  }
}
