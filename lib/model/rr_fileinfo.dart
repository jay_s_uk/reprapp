import 'package:dac/services/base_service.dart';
import 'package:meta/meta.dart';

FileInfo fileInfoFromJsonMap(Map<String, dynamic> map) =>
    FileInfo.fromJson(map);

class FileInfo {
  FileInfo({
    @required this.err,
    @required this.size,
    @required this.lastModified,
    @required this.height,
    @required this.firstLayerHeight,
    @required this.layerHeight,
    @required this.printTime,
    @required this.filament,
    @required this.printDuration,
    @required this.fileName,
    @required this.generatedBy,
  });

  final int err;
  final int size;
  final DateTime lastModified;
  final double height;
  final double firstLayerHeight;
  final double layerHeight;
  final int printTime;
  final List<double> filament;
  final int printDuration;
  final String fileName;
  final String generatedBy;

  factory FileInfo.fromJson(Map<String, dynamic> json) => FileInfo(
        err: cast<int>(json["err"]),
        size: cast<int>(json["size"]),
        lastModified: json["lastModified"] == null
            ? null
            : DateTime.parse(json["lastModified"]),
        height: cast<double>(json["height"]),
        firstLayerHeight: cast<double>(json["firstLayerHeight"]),
        layerHeight: cast<double>(json["layerHeight"]),
        printTime: cast<int>(json["printTime"]),
        filament: json["filament"] == null
            ? null
            : List<double>.from(json["filament"].map((x) => x.toDouble())),
        printDuration: cast<int>(json["printDuration"]),
        fileName: json["fileName"] == null ? null : json["fileName"],
        generatedBy: json["generatedBy"] == null ? null : json["generatedBy"],
      );
}
