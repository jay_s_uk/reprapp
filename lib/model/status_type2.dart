// To parse this JSON data, do
//
//     final statusType2 = statusType2FromJson(jsonString);

import 'dart:convert';

import 'package:meta/meta.dart';

StatusType2 statusType2FromJson(String str) =>
    StatusType2.fromJson(json.decode(str));

StatusType2 statusType2FromJsonMap(Map<String, dynamic> map) =>
    StatusType2.fromJson(map);

class StatusType2 {
  StatusType2({
    @required this.coldExtrudeTemp,
    @required this.coldRetractTemp,
    @required this.compensation,
    @required this.controllableFans,
    @required this.tempLimit,
    @required this.endstops,
    @required this.firmwareName,
    @required this.firmwareVersion,
    @required this.geometry,
    @required this.axes,
    @required this.totalAxes,
    @required this.axisNames,
    @required this.volumes,
    @required this.mountedVolumes,
    @required this.mode,
    @required this.name,
    @required this.probe,
    @required this.tools,
    @required this.mcutemp,
    @required this.vin,
    @required this.v12,
  });

  final double coldExtrudeTemp;
  final double coldRetractTemp;
  final String compensation;
  final int controllableFans;
  final double tempLimit;
  final int endstops;
  final String firmwareName;
  final String firmwareVersion;
  final String geometry;
  final int axes;
  final int totalAxes;
  final String axisNames;
  final int volumes;
  final int mountedVolumes;
  final String mode;
  final String name;
  final Probe probe;
  final List<Tool> tools;
  final Mcutemp mcutemp;
  final Mcutemp vin;
  final Mcutemp v12;

  factory StatusType2.fromJson(Map<String, dynamic> json) => StatusType2(
        coldExtrudeTemp:
            json["coldExtrudeTemp"] == null ? null : json["coldExtrudeTemp"],
        coldRetractTemp:
            json["coldRetractTemp"] == null ? null : json["coldRetractTemp"],
        compensation:
            json["compensation"] == null ? null : json["compensation"],
        controllableFans:
            json["controllableFans"] == null ? null : json["controllableFans"],
        tempLimit: json["tempLimit"] == null ? null : json["tempLimit"],
        endstops: json["endstops"] == null ? null : json["endstops"],
        firmwareName:
            json["firmwareName"] == null ? null : json["firmwareName"],
        firmwareVersion:
            json["firmwareVersion"] == null ? null : json["firmwareVersion"],
        geometry: json["geometry"] == null ? null : json["geometry"],
        axes: json["axes"] == null ? null : json["axes"],
        totalAxes: json["totalAxes"] == null ? null : json["totalAxes"],
        axisNames: json["axisNames"] == null ? null : json["axisNames"],
        volumes: json["volumes"] == null ? null : json["volumes"],
        mountedVolumes:
            json["mountedVolumes"] == null ? null : json["mountedVolumes"],
        name: json["name"] == null ? null : json["name"],
        probe: json["probe"] == null ? null : Probe.fromJson(json["probe"]),
        tools: json["tools"] == null
            ? null
            : List<Tool>.from(json["tools"].map((x) => Tool.fromJson(x))),
        mcutemp:
            json["mcutemp"] == null ? null : Mcutemp.fromJson(json["mcutemp"]),
        vin: json["vin"] == null ? null : Mcutemp.fromJson(json["vin"]),
        v12: json["v12"] == null ? null : Mcutemp.fromJson(json["v12"]),
      );
}

class Mcutemp {
  Mcutemp({
    @required this.min,
    @required this.cur,
    @required this.max,
  });

  final double min;
  final double cur;
  final double max;

  factory Mcutemp.fromJson(Map<String, dynamic> json) => Mcutemp(
        min: json["min"] == null ? null : json["min"].toDouble(),
        cur: json["cur"] == null ? null : json["cur"].toDouble(),
        max: json["max"] == null ? null : json["max"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "min": min == null ? null : min,
        "cur": cur == null ? null : cur,
        "max": max == null ? null : max,
      };
}

class Probe {
  Probe({
    @required this.threshold,
    @required this.height,
    @required this.type,
  });

  final int threshold;
  final double height;
  final int type;

  factory Probe.fromJson(Map<String, dynamic> json) => Probe(
        threshold: json["threshold"] == null ? null : json["threshold"],
        height: json["height"] == null ? null : json["height"].toDouble(),
        type: json["type"] == null ? null : json["type"],
      );

  Map<String, dynamic> toJson() => {
        "threshold": threshold == null ? null : threshold,
        "height": height == null ? null : height,
        "type": type == null ? null : type,
      };
}

class Tool {
  Tool({
    @required this.number,
    @required this.name,
    @required this.heaters,
    @required this.drives,
    @required this.axisMap,
    @required this.fans,
    @required this.filament,
    @required this.offsets,
  });

  final int number;
  final String name;
  final List<int> heaters;
  final List<int> drives;
  final List<List<int>> axisMap;
  final int fans;
  final String filament;
  final List<double> offsets;

  factory Tool.fromJson(Map<String, dynamic> json) => Tool(
        number: json["number"] == null ? null : json["number"],
        name: json["name"] == null ? null : json["name"],
        heaters: json["heaters"] == null
            ? null
            : List<int>.from(json["heaters"].map((x) => x)),
        drives: json["drives"] == null
            ? null
            : List<int>.from(json["drives"].map((x) => x)),
        axisMap: json["axisMap"] == null
            ? null
            : List<List<int>>.from(
                json["axisMap"].map((x) => List<int>.from(x.map((x) => x)))),
        fans: json["fans"] == null ? null : json["fans"],
        filament: json["filament"] == null ? null : json["filament"],
        offsets: json["offsets"] == null
            ? null
            : List<double>.from(json["offsets"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "number": number == null ? null : number,
        "name": name == null ? null : name,
        "heaters":
            heaters == null ? null : List<dynamic>.from(heaters.map((x) => x)),
        "drives":
            drives == null ? null : List<dynamic>.from(drives.map((x) => x)),
        "axisMap": axisMap == null
            ? null
            : List<dynamic>.from(
                axisMap.map((x) => List<dynamic>.from(x.map((x) => x)))),
        "fans": fans == null ? null : fans,
        "filament": filament == null ? null : filament,
        "offsets":
            offsets == null ? null : List<dynamic>.from(offsets.map((x) => x)),
      };
}
