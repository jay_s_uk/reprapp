import 'dart:convert';

import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'base_record.dart';

class Space extends Equatable implements BaseRecord {
  final int id;
  final String name;

  const Space({
    this.id,
    @required this.name,
  });

  @override
  List<Object> get props => [id];

  Space copyWith({
    int id,
    String name,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (name == null || identical(name, this.name))) {
      return this;
    }

    return new Space(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  @override
  Space setId(int id) {
    return copyWith(id: id);
  }

  @override
  String toString() {
    return 'Space{id: $id, name: $name}';
  }
}

class SpaceTile extends Equatable implements BaseRecord {
  final int id;
  final int spaceId;
  final TileKey tileKey;
  final TileSize tileSize;
  final SpaceTileSettings tileSettings;
  final int location;

  SpaceTile(
      {this.id,
      @required this.spaceId,
      @required this.tileKey,
      @required this.tileSize,
      this.tileSettings,
      this.location});

  @override
  List<Object> get props => [id, spaceId, tileKey, tileSize];

  SpaceTile copyWith({
    int id,
    int spaceId,
    TileKey tileKey,
    TileSize tileSize,
    SpaceTileSettings tileSettings,
    int location,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (spaceId == null || identical(spaceId, this.spaceId)) &&
        (tileKey == null || identical(tileKey, this.tileKey)) &&
        (tileSize == null || identical(tileSize, this.tileSize)) &&
        (tileSettings == null || identical(tileSettings, this.tileSettings)) &&
        (location == null || identical(location, this.location))) {
      return this;
    }

    return new SpaceTile(
      id: id ?? this.id,
      spaceId: spaceId ?? this.spaceId,
      tileKey: tileKey ?? this.tileKey,
      tileSize: tileSize ?? this.tileSize,
      tileSettings: tileSettings ?? this.tileSettings,
      location: location ?? this.location,
    );
  }

  @override
  SpaceTile setId(int id) {
    return copyWith(id: id);
  }
}

class SpaceTileSettings {
  final Map<String, String> properties;

  SpaceTileSettings._(this.properties);

  factory SpaceTileSettings.empty() {
    return SpaceTileSettings._({});
  }

  factory SpaceTileSettings.create(Map<String, String> properties) {
    return SpaceTileSettings._(properties);
  }

  factory SpaceTileSettings.fromJson(String json) {
    if (json != null) {
      return SpaceTileSettings._(Map<String, String>.from(jsonDecode(json)));
    } else {
      throw Exception("Settings can't be null");
    }
  }

  String getOrValue(String property, String valueIfAbsent) {
    return properties.containsKey(property)
        ? properties[property]
        : valueIfAbsent;
  }

  T getOrFunctionValue<T>(String property, Function function, T valueIfAbsent) {
    return properties.containsKey(property)
        ? function(properties[property])
        : valueIfAbsent;
  }

  void put(String key, String value) {
    properties[key] = value;
  }

  String toJson() {
    return jsonEncode(properties);
  }
}
