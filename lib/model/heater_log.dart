import 'package:dac/model/base_record.dart';
import 'package:dac/model/printer_data.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class HeaterLogValue extends Equatable implements BaseRecord {
  final int id;
  final int printerId;
  final int heater;
  final DateTime dateTime;
  final double temperatureCurrent;
  final double temperatureActive;
  final double temperatureStandby;
  final HeaterState state;

  const HeaterLogValue({
    this.id,
    @required this.printerId,
    @required this.heater,
    @required this.dateTime,
    @required this.temperatureCurrent,
    @required this.temperatureActive,
    @required this.temperatureStandby,
    @required this.state,
  });

  @override
  List<Object> get props => [
        id,
        printerId,
        heater,
        dateTime,
        temperatureCurrent,
        temperatureActive,
        temperatureStandby,
        state
      ];

  HeaterLogValue copyWith({
    int id,
    int printerId,
    int heater,
    DateTime dateTime,
    double temperatureCurrent,
    double temperatureActive,
    double temperatureStandby,
    HeaterState state,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (printerId == null || identical(printerId, this.printerId)) &&
        (heater == null || identical(heater, this.heater)) &&
        (dateTime == null || identical(dateTime, this.dateTime)) &&
        (temperatureCurrent == null ||
            identical(temperatureCurrent, this.temperatureCurrent)) &&
        (temperatureActive == null ||
            identical(temperatureActive, this.temperatureActive)) &&
        (temperatureStandby == null ||
            identical(temperatureStandby, this.temperatureStandby)) &&
        (state == null || identical(state, this.state))) {
      return this;
    }

    return new HeaterLogValue(
      id: id ?? this.id,
      printerId: printerId ?? this.printerId,
      heater: heater ?? this.heater,
      dateTime: dateTime ?? this.dateTime,
      temperatureCurrent: temperatureCurrent ?? this.temperatureCurrent,
      temperatureActive: temperatureActive ?? this.temperatureActive,
      temperatureStandby: temperatureStandby ?? this.temperatureStandby,
      state: state ?? this.state,
    );
  }

  @override
  HeaterLogValue setId(int id) {
    return copyWith(id: id);
  }
}
