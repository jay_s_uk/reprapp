import 'package:dac/model/space.dart';
import 'package:dac/printer_icons_icons.dart';
import 'package:dac/scaffolds/logs_page.dart';
import 'package:dac/scaffolds/printers_page.dart';
import 'package:dac/scaffolds/settings_page.dart';
import 'package:dac/scaffolds/space_add_page.dart';
import 'package:dac/scaffolds/space_page.dart';
import 'package:dac/scaffolds/webcams_page.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:theme_provider/theme_provider.dart';
import 'package:url_launcher/url_launcher.dart';

class AppDrawer extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<Space> spaces = watch(spacesListProvider).records;
    return Container(
        width: 350,
        child: Drawer(
          child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _TilesViewsList(spaces),
                Expanded(
                    child: Container(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      _Header(),
                      _createDrawerItem(
                          theme: Theme.of(context),
                          icon: NozzleIcon(),
                          text: 'Printers',
                          onTap: () => Navigator.pushReplacementNamed(
                              context, PrintersScaffold.routeName)),
                      _createDrawerItem(
                          theme: Theme.of(context),
                          icon: Icon(Icons.videocam),
                          text: 'Webcams',
                          onTap: () => Navigator.pushReplacementNamed(
                              context, WebcamsPage.routeName)),
                      Divider(),
                      _createDrawerItem(
                        icon: Icon(Icons.collections_bookmark),
                        text: 'Logs',
                        theme: Theme.of(context),
                        onTap: () =>
                            Navigator.pushNamed(context, LogsPage.routeName),
                      ),
                      Divider(
                        color: Theme.of(context).accentColor,
                      ),
                      _createDrawerItem(
                        icon: Icon(Icons.color_lens),
                        text: 'Settings',
                        theme: Theme.of(context),
                        onTap: () => Navigator.pushNamed(
                            context, SettingsPage.routeName),
                        // onTap: () => showDialog(
                        //     context: context,
                        //     builder: (_) =>
                        //         ThemeConsumer(child: ThemeDialog())),
                      ),
                      Divider(
                        color: Theme.of(context).accentColor,
                      ),
                      _createDrawerItem(
                        icon: Icon(Icons.face),
                        text: 'Contact and Support',
                        theme: Theme.of(context),
                        onTap: () => _launchURLSupport(),
                      ),
                      _createDrawerItem(
                        icon: Icon(Icons.bug_report),
                        text: 'Report an issue',
                        theme: Theme.of(context),
                        onTap: () => _launchURL(),
                      ),
                      ListTile(
                        title: Text('0.0.1'),
                        onTap: () {},
                      ),
                    ],
                  ),
                )),
              ]),
        ));
  }

  Widget _createDrawerItem(
      {Widget icon, String text, GestureTapCallback onTap, ThemeData theme}) {
    return ListTile(
      title: Row(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: icon,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }

  _launchURL() async {
    const url = 'https://discord.gg/f5wACmFx3R';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLSupport() async {
    const url = 'https://discord.gg/tZqeXARwp4';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class _TilesViewsList extends StatelessWidget {
  final List<Space> spaces;
  _TilesViewsList(this.spaces);

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [];
    items.addAll(spaces.map((e) => _SpaceIconButton(e)).toList());
    Widget addIcon = IconButton(
      icon: Icon(Icons.add_circle),
      onPressed: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => SpaceAddPage(),
          fullscreenDialog: true,
        ));
      },
    );
    items.add(addIcon);
    return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        width: 90,
        padding: EdgeInsets.only(top: AppBar().preferredSize.height),
        child: ListView(
            scrollDirection: Axis.vertical,
            padding: EdgeInsets.all(5),
            children: items));
  }
}

class _SpaceIconButton extends ConsumerWidget {
  final Space space;

  _SpaceIconButton(this.space);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    bool selected = space == watch(spaceSelectedProvider).state;

    return InkWell(
      onTap: () {
        context.read(spaceSelectedProvider).setSpace(space);
        Navigator.pushReplacementNamed(context, SpacePage.routeName,
            arguments: space);
      },
      child: Container(
          margin: EdgeInsets.all(5),
          padding: EdgeInsets.all(3),
          decoration: BoxDecoration(
              color: selected
                  ? Theme.of(context).indicatorColor
                  : Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          child: Container(
              // width: 70,
              height: 60,
              decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.all(Radius.circular(18.0))),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.view_quilt_outlined,
                      size: 30,
                    ),
                    Text(
                      space.name ?? space.id.toString(),
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(fontSize: 8),
                    )
                  ]))),
    );
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String themeId = ThemeProvider.themeOf(context).id;
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 50,
          ),
          Image(
              image: AssetImage(themeId.contains("dark")
                  ? 'assets/logo_white_transparent.png'
                  : 'assets/logo_black_transparent.png'),
              height: 50,
              fit: BoxFit.fill),
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 40),
            child: Text(
              "duet app control",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500),
            ),
          ),
        ]);
  }
}
