import 'package:dac/model/printer.dart';
import 'package:dac/widgets/status/status_widget.dart';
import 'package:dac/widgets/status/tool_position_widget.dart';
import 'package:flutter/material.dart';

class StatusPanel extends StatelessWidget {
  final Printer printer;

  const StatusPanel({Key key, this.printer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: ExpansionTile(
            initiallyExpanded: true,
            title: Row(children: [
              Expanded(
                  child: ListTile(
                leading: Icon(Icons.info),
                title: Text('Status'),
              )),
              Expanded(child: StatusWidget(printer))
            ]),
            children: [
              Container(
                  margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Column(children: [ToolPositionWidget(printer)]))
            ]),
      ),
    );
  }
}
