import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class ToolPositionWidget extends ConsumerWidget {
  final Printer printer;

  ToolPositionWidget(this.printer);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Row(
      children: [
        Container(
            padding: EdgeInsets.all(10.0),
            child: Text(
              "Tool Position",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).textTheme.caption.color),
            )),
        Consumer(
            // ignore: missing_return
            builder: (_, watch, child) {
          Move move = watch(printerMoveProvider(printer));
          if (move != null && move.axes != null) {
            var axes = move?.axes;
            return Expanded(
                child: Row(children: [
              _Position("X", axes[0]?.machinePosition?.toString() ?? "-"),
              _Position("Y", axes[1]?.machinePosition?.toString() ?? "-"),
              _Position("Z", axes[2]?.machinePosition?.toString() ?? "-"),
            ]));
          } else {
            return Expanded(
                child: Row(children: [
              _Position("X", "-"),
              _Position("Y", "-"),
              _Position("Z", "-"),
            ]));
          }
        })
      ],
    );
  }
}

class _Position extends StatelessWidget {
  _Position(this.coordinate, this.value);

  final String value;
  final String coordinate;

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Center(
      child: Column(
        children: [
          Text(coordinate,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).textTheme.caption.color)),
          Text(value)
        ],
      ),
    ));
  }
}
