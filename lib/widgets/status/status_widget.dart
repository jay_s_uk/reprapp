import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class StatusWidget extends ConsumerWidget {
  final Printer printer;

  StatusWidget(this.printer);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    MachineStatus machineStatus = watch(printerMachineStatusProvider(printer));

    Color backgroundColor = Colors.limeAccent;
    Color textColor = Colors.black45;
    String title = "?";
    switch (machineStatus) {
      case MachineStatus.READING_CONFIGURATION:
        title = "Reading";
        break;
      case MachineStatus.FLASHING_FIRMWARE:
        title = "Flashing";
        backgroundColor = Colors.red;
        textColor = Colors.black45;
        break;
      case MachineStatus.HALTED:
        title = "Halted";
        backgroundColor = Colors.redAccent;
        textColor = Colors.black45;
        break;
      case MachineStatus.OFF:
        title = "Off";
        backgroundColor = Colors.black12;
        break;
      case MachineStatus.PAUSING_PRINT:
        title = "Pausing";
        backgroundColor = Colors.amber;
        textColor = Colors.black45;
        break;
      case MachineStatus.RESUMING_PRINT:
        title = "Resuming";
        backgroundColor = Colors.amber;
        textColor = Colors.black45;
        break;
      case MachineStatus.PRINT_PAUSED:
        title = "Paused";
        backgroundColor = Colors.deepOrangeAccent;
        textColor = Colors.black45;
        break;
      case MachineStatus.SIMULATING_PRINT:
        title = "Simulating";
        backgroundColor = Colors.lightGreen;
        textColor = Colors.black45;
        break;
      case MachineStatus.PRINTING:
        title = "Printing";
        backgroundColor = Colors.lightGreen;
        textColor = Colors.black45;
        break;
      case MachineStatus.CHANGING_TOOL:
        title = "Changing tool";
        backgroundColor = Colors.amber;
        textColor = Colors.black45;
        break;
      case MachineStatus.BUSY:
        title = "Busy";
        backgroundColor = Colors.deepOrangeAccent;
        textColor = Colors.black45;
        break;
      case MachineStatus.IDLE:
        title = "Idle";
        backgroundColor = Colors.limeAccent;
        textColor = Colors.black45;
        break;
      default:
        backgroundColor = Colors.greenAccent;
        textColor = Colors.black45;
    }

    return Chip(
        backgroundColor: backgroundColor,
        label: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: textColor,
          ),
        ));
  }
}
