import 'package:dac/model/printer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

import '../temperature_chart.dart';

class TemperaturesPanel extends StatelessWidget {
  final Printer printer;

  const TemperaturesPanel({Key key, this.printer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: ExpansionTile(
            initiallyExpanded: true,
            title: Row(children: [
              Expanded(
                  child: ListTile(
                leading: Icon(Icons.stacked_line_chart),
                title: Text('Temperatures'),
              )),
            ]),
            children: [
              Column(mainAxisSize: MainAxisSize.max, children: [
                Container(
                    height: 300,
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: SegmentsLineChart(printer))
              ])
            ]),
      ),
    );
  }
}
