import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class PrinterInfoPanel extends StatelessWidget {
  final Printer printer;

  const PrinterInfoPanel({Key key, this.printer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: ExpansionTile(
            initiallyExpanded: true,
            title: Row(children: [
              Expanded(
                  child: ListTile(
                leading: Icon(Icons.stacked_line_chart),
                title: Text('Machine'),
              )),
            ]),
            children: [
              _PrinterInfoWidget(
                printer: printer,
              ),
            ]),
      ),
    );
  }
}

class _PrinterInfoWidget extends ConsumerWidget {
  final Printer printer;

  const _PrinterInfoWidget({Key key, this.printer}) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    PrinterInfo printerInfo =
        watch(printerInfoNotifierProvider(printer)).printerInfo;
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(children: [
        _FieldText("Name", printerInfo?.name ?? "-"),
        _FieldText("Firmware", printerInfo?.firmwareName ?? "-"),
        _FieldText("Version", printerInfo?.firmwareVersion ?? "-"),
        _FieldText("Geometry", printerInfo?.geometry ?? "-"),
        _FieldText("Mode", printerInfo?.mode ?? "-"),
        _FieldText("Axes", printerInfo?.axes?.toString() ?? "-"),
      ]),
    );
  }
}

class _FieldText extends StatelessWidget {
  final String title;
  final String value;
  _FieldText(this.title, this.value);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.caption,
        ),
        Expanded(
          child: Text(
            value,
            style: Theme.of(context).textTheme.bodyText1,
            textAlign: TextAlign.right,
          ),
        )
      ],
    );
  }
}
