import 'dart:math';
import 'dart:ui';

import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/widgets/temperature_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:intl/intl.dart';
import 'package:spannable_grid/spannable_grid.dart';

class ToolsPanel extends StatelessWidget {
  final Printer printer;

  const ToolsPanel({Key key, @required this.printer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Card(
            child: ExpansionTile(
                initiallyExpanded: true,
                title: Row(children: [
                  Expanded(
                      child: ListTile(
                    leading: Icon(Icons.build),
                    title: Text('Tools'),
                  )),
                ]),
                children: [ToolsHeader(), ToolsGrid(printer)])));
  }
}

class ToolsHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<SpannableGridCellData> _toolsCells = [
      _cellHeader("Tool", 1, "Tool"),
      _cellHeader("Heater", 2, "Heater"),
      _cellHeader("Current", 3, "Current"),
      _cellHeader("Active", 4, "Active"),
      _cellHeader("Standby", 5, "Standby"),
    ];

    return SpannableGrid(
      columns: 5,
      rows: 1,
      cells: _toolsCells,
      spacing: 0,
      editingOnLongPress: false,
      rowHeight: 30,
    );
  }

  SpannableGridCellData _cellHeader(String id, int column, String title) {
    return SpannableGridCellData(
      row: 1,
      column: column,
      columnSpan: 1,
      rowSpan: 1,
      id: id,
      child: Container(
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}

class ToolsGrid extends ConsumerWidget {
  final Printer printer;

  ToolsGrid(this.printer);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    PrinterInfo printerInfo = watch(printerInfoProvider(printer)).state;
    List<Tool> tools = watch(printerToolsProvider(printer));
    Bed bed = watch(printerBedProvider(printer)).state;

    List<Color> backgroundColors = [
      Theme.of(context).backgroundColor.withAlpha(50),
      Theme.of(context).backgroundColor.withAlpha(10),
    ];
    List<SpannableGridCellData> _toolsCells = [];
    int countRows = 0;

    if (tools != null) {
      for (int i = 0; i < printerInfo.tools.length; i++) {
        ToolInfo t = printerInfo.tools[i];
        countRows++;

        _toolsCells.add(_cellName("Tool $i", countRows, t.heaters.length,
            t.name ?? "Tool " + t.number.toString(), backgroundColors[i % 2]));

        Tool tool =
            tools.firstWhere((element) => element.toolNumber == t.number);
        int heaterRow = countRows;
        t.heaters.forEach((heaterNumber) {
          Heater heater = tool.heaters
              .firstWhere((element) => element.number == heaterNumber);

          _toolsCells.add(_cellHeater("Tool $i Heater $heaterNumber", heaterRow,
              heater, backgroundColors[i % 2]));

          _toolsCells.add(_cellCurrent(
              "Tool $i Heater ${heater.number} Current",
              heaterRow,
              heater,
              backgroundColors[i % 2]));

          TemperatureSet temperatureSet = tool.temperaturesSets
              .firstWhere((element) => element.heaterNumber == heaterNumber);

          _toolsCells.add(_cellTemperatureSet(
              "Tool $i Heater ${heater.number} Active",
              heaterRow,
              4,
              temperatureSet.active,
              backgroundColors[i % 2]));
          _toolsCells.add(_cellTemperatureSet(
              "Tool $i Heater ${heater.number} Standby",
              heaterRow,
              5,
              temperatureSet.standby,
              backgroundColors[i % 2]));
          heaterRow++;
        });
        countRows += max(t.heaters.length - 1, 0);
      }
    }
    if (bed != null) {
      countRows++;

      var backgroundColor = backgroundColors[printerInfo.tools.length % 2];
      _toolsCells.add(_cellName("Bed", countRows, 1, "Bed", backgroundColor));

      Heater heater = bed.heater;
      _toolsCells.add(_cellHeater(
          "Bed Heater ${heater.number}", countRows, heater, backgroundColor));

      _toolsCells.add(_cellCurrent("Bed Heater ${heater.number} Current",
          countRows, heater, backgroundColor));

      _toolsCells.add(_cellTemperatureSet("Bed Heater ${heater.number} Active",
          countRows, 4, bed.temperatureSet.active, backgroundColor));
      _toolsCells.add(_cellTemperatureSet("Bed Heater ${heater.number} Standby",
          countRows, 5, bed.temperatureSet.standby, backgroundColor));
    }

    return countRows > 0
        ? SpannableGrid(
            columns: 5,
            rows: countRows,
            cells: _toolsCells,
            spacing: 0,
            editingOnLongPress: false,
            rowHeight: 60,
          )
        : Container(
            child: Text("No tools available"),
          );
  }

  SpannableGridCellData _cellName(String id, int rowNumber, int heatersCount,
      String title, Color backgroundColor) {
    return SpannableGridCellData(
      row: rowNumber,
      column: 1,
      columnSpan: 1,
      rowSpan: heatersCount,
      id: id,
      child: Container(
        color: backgroundColor,
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }

  SpannableGridCellData _cellHeater(
      String id, int rowNumber, Heater heater, Color backgroundColor) {
    return SpannableGridCellData(
      row: rowNumber,
      column: 2,
      columnSpan: 1,
      rowSpan: 1,
      id: id,
      child: Container(
        color: backgroundColor,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Heater ${heater.number}",
              style: TextStyle(color: heaterColor[heater.number]),
            ),
            HeaterStateWidget(heater.state),
          ],
        )),
      ),
    );
  }

  SpannableGridCellData _cellCurrent(
      String id, int rowNumber, Heater heater, Color backgroundColor) {
    return SpannableGridCellData(
      row: rowNumber,
      column: 3,
      columnSpan: 1,
      rowSpan: 1,
      id: id,
      child: Container(
        color: backgroundColor,
        child: Center(child: Text(temperatureFormat(heater.current))),
      ),
    );
  }

  SpannableGridCellData _cellTemperatureSet(String id, int rowNumber,
      int column, double temperature, Color backgroundColor) {
    return SpannableGridCellData(
      row: rowNumber,
      column: column,
      columnSpan: 1,
      rowSpan: 1,
      id: id,
      child: Container(
        color: backgroundColor,
        child: Center(child: Text(temperatureFormat(temperature))),
      ),
    );
  }
}

class HeaterStateWidget extends StatelessWidget {
  final HeaterState heaterState;

  HeaterStateWidget(this.heaterState);

  @override
  Widget build(BuildContext context) {
    String text = "";
    switch (heaterState) {
      case HeaterState.ACTIVE:
        text = "active";
        break;
      case HeaterState.OFF:
        text = "off";
        break;
      case HeaterState.STANDBY:
        text = "standby";
        break;
      case HeaterState.FAULT:
        text = "fault";
        break;
      case HeaterState.TUNING:
        text = "tuning";
        break;
      case HeaterState.OFFLINE:
        text = "offline";
        break;
    }
    return Text(
      text,
      style: Theme.of(context).textTheme.overline,
    );
  }
}

String temperatureFormat(double value, {int decimals = 1}) => value != null
    ? (NumberFormat("##0" + (decimals > 0 ? ".0" : "")).format(value) + "º")
    : "n/a";

class GridHeader extends StatelessWidget {
  final String text;

  GridHeader(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: Text(text,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).textTheme.caption.color)));
  }
}

class GridCell extends StatelessWidget {
  final Widget child;

  GridCell(this.child);

  @override
  Widget build(BuildContext context) {
    return Container(alignment: Alignment.center, height: 20, child: child);
  }
}

class GridCellText extends StatelessWidget {
  final String text;

  GridCellText(this.text);

  @override
  Widget build(BuildContext context) {
    return GridCell(Text(text, textAlign: TextAlign.center));
  }
}
