import 'package:dac/model/printer.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class PrinterSelector extends ConsumerWidget {
  final int selectedPrinterId;
  final Function onChanged;

  PrinterSelector(this.selectedPrinterId, this.onChanged);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<Printer> _records = watch(printersListProvider).records;

    return DropdownSearch<Printer>(
      enabled: _records.length > 1,
      showSelectedItem: true,
      compareFn: (Printer i, Printer s) => i.id == s.id,
      label: "Printer",
      onFind: (String filter) => _getData(_records, filter),
      onChanged: onChanged,
      dropdownBuilder: _customDropDown,
      dropDownButton: _records.length > 1 ? null : Container(),
      popupItemBuilder: _customPopupItemBuilder,
      selectedItem: _records?.firstWhere((p) => p.id == selectedPrinterId,
          orElse: () => _records.length == 1 ? _records.first : Printer()),
    );
  }

  Widget _customDropDown(
      BuildContext context, Printer item, String itemDesignation) {
    return Container(
      child: (item?.hostname == null)
          ? ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: CircleAvatar(),
              title: Text("No printer selected"),
            )
          : ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(item.name),
              subtitle: Text(
                item.hostname,
              ),
            ),
    );
  }

  Widget _customPopupItemBuilder(
      BuildContext context, Printer item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        selected: isSelected,
        title: Text(item.name),
        subtitle: Text(item.hostname),
      ),
    );
  }

  Future<List<Printer>> _getData(List<Printer> records, String filter) {
    return Future.microtask(() => records
        .where((p) =>
            p.hostname.startsWith(filter.toLowerCase()) ||
            p.name.toLowerCase().startsWith(filter.toLowerCase()))
        .toList());
  }
}
