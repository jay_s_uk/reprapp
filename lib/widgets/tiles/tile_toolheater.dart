import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/widgets/temperature_chart.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

import '../tiles_panel.dart';

class TileContentToolHeaterStatic extends StatelessWidget {
  final String title;
  final Widget icon;
  final Heater heater;
  final TemperatureSet temperatureSet;

  TileContentToolHeaterStatic(
    this.title,
    this.icon,
    this.heater,
    this.temperatureSet,
  );
  @override
  Widget build(BuildContext context) {
    return TileContentToolHeater(title, icon, heater, temperatureSet);
  }
}

class TileContentToolHeater extends StatelessWidget {
  final String title;
  final Widget icon;
  final Heater heater;
  final TemperatureSet temperatureSet;

  TileContentToolHeater(
    this.title,
    this.icon,
    this.heater,
    this.temperatureSet,
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(TILE_PADDING),
        child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _SubTile(
                child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        title,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                      ),
                      Padding(padding: EdgeInsets.only(top: 10), child: icon),
                    ]),
              ),
              _SubTile(
                child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Heater",
                        style:
                            TextStyle(color: heaterColor[heater?.number ?? 0]),
                      ),
                      Text(
                        heater?.number?.toString() ?? "-",
                        textScaleFactor: 1.4,
                        style:
                            TextStyle(color: heaterColor[heater?.number ?? 0]),
                      ),
                      HeaterStateWidget(heater?.state ?? HeaterState.OFFLINE)
                    ]),
              ),
              _SubTile(
                  child: _Temperature(
                      "Current",
                      heater?.current,
                      heater != null &&
                          (heater.state == HeaterState.ACTIVE ||
                              heater.state == HeaterState.STANDBY),
                      scaleFactor: 1.4)),
              _SubTile(
                  size: 50,
                  child: _Temperature("Active", temperatureSet?.active,
                      heater?.state == HeaterState.ACTIVE)),
              _SubTile(
                  size: 50,
                  child: _Temperature("Standby", temperatureSet?.standby,
                      heater?.state == HeaterState.STANDBY)),
            ]));
  }
}

class HeaterStateWidget extends StatelessWidget {
  final HeaterState heaterState;

  HeaterStateWidget(this.heaterState);

  @override
  Widget build(BuildContext context) {
    String text = "";
    switch (heaterState) {
      case HeaterState.ACTIVE:
        text = "active";
        break;
      case HeaterState.OFF:
        text = "off";
        break;
      case HeaterState.STANDBY:
        text = "standby";
        break;
      case HeaterState.FAULT:
        text = "fault";
        break;
      case HeaterState.TUNING:
        text = "tuning";
        break;
      case HeaterState.OFFLINE:
        text = "offline";
        break;
    }
    return Text(
      text,
      style: Theme.of(context).textTheme.overline,
    );
  }
}

String temperatureFormat(double value, {int decimals = 1}) =>
    NumberFormat("##0" + (decimals > 0 ? ".0" : "")).format(value) + "º";

class _Header extends StatelessWidget {
  final String text;
  _Header(this.text);
  @override
  Widget build(BuildContext context) {
    return AutoSizeText(
      text,
      textScaleFactor: 0.8,
      maxLines: 1,
      // style:
      //     TextStyle(color: Theme.of(context).primaryTextTheme.caption.color),
    );
  }
}

double _getTileSideForSize(double tileSize, int size) {
  double side = tileSize * size;
  return side;
}

class _SubTile extends ConsumerWidget {
  final Widget child;
  final double size;
  _SubTile({@required this.child, this.size});

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    double tileSize =
        watch(tileSizeProvider(TileMediaQuery.context(context))).state;
    double width =
        size ?? ((_getTileSideForSize(tileSize, 4) - TILE_PADDING * 4) / 5);
    return Container(width: width, child: child);
  }
}

class _Temperature extends StatelessWidget {
  final String title;
  final double value;
  final bool switchedOn;
  final double scaleFactor;
  _Temperature(this.title, this.value, this.switchedOn, {this.scaleFactor = 1});

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _Header(title),
          Expanded(
              child: Center(
            child: AutoSizeText(
              value != null ? temperatureFormat(value) : "-",
              textScaleFactor: scaleFactor,
              maxLines: 1,
            ),
          )),
          Icon(
            Icons.lens,
            color: switchedOn ? Colors.red : Color.fromRGBO(100, 100, 100, 0.5),
            size: 8,
          )
        ]);
  }
}
