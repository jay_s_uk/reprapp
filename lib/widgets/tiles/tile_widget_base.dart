import 'package:animated_widgets/animated_widgets.dart';
import 'package:dac/model/space.dart';
import 'package:dac/scaffolds/tile_settings_page.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

import '../tiles_panel.dart';

const int TILES_IDEAL_EXTENT_PORTRAIT = 6;
const int TILES_IDEAL_EXTENT_LANDSCAPE = 11;
const double TILE_PADDING = 4;
const double TILE_SPACING = 4;
const double TILE_SIZE_MIN = 74;
const double TILE_SIZE_MAX = 100;

class TileSize {
  final int x;
  final int y;

  TileSize(this.x, this.y);

  factory TileSize.fromString(String data) {
    var values = data.split(",");
    return TileSize(int.parse(values[0]), int.parse(values[1]));
  }

  String toString() {
    return x == null || y == null
        ? throw new Exception("TileSize with null values")
        : "$x,$y";
  }
}

abstract class TileSelect extends StatelessWidget {
  final TileKey tileKey;
  final TileSize tileSize;
  final Widget widget;
  final TileSettings tileSettings;

  const TileSelect({
    @required this.tileKey,
    @required this.tileSize,
    @required this.widget,
    @required this.tileSettings,
  });

  @override
  Widget build(BuildContext context) {
    return TileWidget(
        tileKey: tileKey,
        tileSize: tileSize,
        widget: widget,
        tileSettings: tileSettings);
  }
}

abstract class TileSpace extends StatelessWidget {
  final Space space;
  final SpaceTile spaceTile;
  final Widget widgetLive;
  final Widget widgetEdit;
  final bool isEdit;
  final TileSettings tileSettings;

  TileSpace({
    @required this.space,
    @required this.spaceTile,
    @required this.widgetLive,
    @required this.widgetEdit,
    @required this.isEdit,
    @required this.tileSettings,
  });

  @override
  Widget build(BuildContext context) {
    if (isEdit) {
      return ShakeAnimatedWidget(
          enabled: true,
          duration: Duration(milliseconds: 250),
          shakeAngle: Rotation.deg(z: 1),
          curve: Curves.linear,
          child: Stack(alignment: Alignment.center, children: [
            TileWidget(
              tileKey: spaceTile.tileKey,
              tileSize: spaceTile.tileSize,
              widget: widget,
              tileSettings: tileSettings,
            ),
            Positioned(
              top: 5,
              right: 5,
              child: Container(
                  width: 40,
                  height: 40,
                  decoration: new BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    shape: BoxShape.circle,
                  ),
                  child: PopupMenuButton<_Options>(
                    icon: Icon(Icons.more_vert),
                    onSelected: (value) {
                      switch (value) {
                        case _Options.DELETE:
                          _showDeleteDialog(context, tileSettings)
                              .then((value) {
                            if (value) {
                              context
                                  .read(spaceTilesListProvider(space))
                                  .remove(spaceTile);
                            }
                          });
                          break;
                        case _Options.EDIT:
                          Navigator.pushNamed(
                              context, TileSettingsPage.routeName,
                              arguments: SpaceTileSelected(
                                  space: space, tileSpace: this));
                          break;
                      }
                    },
                    itemBuilder: (BuildContext context) =>
                        <PopupMenuItem<_Options>>[
                      const PopupMenuItem<_Options>(
                          value: _Options.EDIT,
                          child: ListTile(
                            leading: Icon(Icons.edit),
                            title: Text('Settings'),
                          )),
                      const PopupMenuItem<_Options>(
                          value: _Options.DELETE,
                          child: ListTile(
                            tileColor: Colors.red,
                            leading: Icon(Icons.delete),
                            title: Text('Delete'),
                          )),
                    ],
                  )),
            )
          ]));
    }
    return TileWidget(
      tileKey: spaceTile.tileKey,
      tileSize: spaceTile.tileSize,
      widget: widget,
      tileSettings: tileSettings,
    );
  }

  Future<bool> _showDeleteDialog(
      BuildContext context, TileSettings tileSettings) async {
    Widget cancelButton = TextButton(
        child: Text("Cancel"),
        onPressed: () => Navigator.of(context).pop(false));

    Widget continueButton = TextButton(
      child: Text("Remove"),
      onPressed: () {
        Navigator.of(context).pop(true);
      },
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith((_) => Colors.white),
        foregroundColor: MaterialStateProperty.resolveWith((_) => Colors.red),
      ),
    );

    AlertDialog alert = AlertDialog(
      title: Text("Remove Tile"),
      content: Text("Do you confirm removing this tile?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    return await showDialog(
        context: context, builder: (BuildContext context) => alert);
  }

  Widget get widget => isEdit ? widgetEdit : widgetLive;
}

enum _Options { EDIT, DELETE }

abstract class TileSettings<T extends HasSpaceTileSettings> {
  final Widget settingsWidget;
  final String title;

  const TileSettings({
    @required this.settingsWidget,
    @required this.title,
  });
}

abstract class HasSpaceTileSettings {
  SpaceTileSettings spaceTileSettings();
}

double tilePaintSide(int tileSize, double tilePaintSize) {
  double innerSize = tileSize * tilePaintSize;
  double padding = 0;
  if (tileSize > 1) {
    padding = TILE_SPACING * (tileSize - 1);
  }
  return innerSize + padding;
}

class TileWidget extends ConsumerWidget {
  final TileKey tileKey;
  final TileSettings tileSettings;
  final TileSize tileSize;
  final Widget widget;

  TileWidget({
    @required this.tileKey,
    @required this.tileSize,
    @required this.widget,
    @required this.tileSettings,
  });

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    double tilePaintSize =
        watch(tileSizeProvider(TileMediaQuery.context(context))).state;
    double width = tilePaintSide(tileSize.x, tilePaintSize);
    double height = tilePaintSide(tileSize.y, tilePaintSize);
    return _TileWidgetBaseContainer(
      child: widget,
      width: width,
      height: height,
      padding: TILE_PADDING,
      color: Theme.of(context).canvasColor,
      borderColor: Theme.of(context).selectedRowColor,
    );
  }
}

class _TileWidgetBaseContainer extends StatelessWidget {
  final Widget child;
  final double width;
  final double height;
  final double padding;
  final Color color;
  final Color borderColor;

  _TileWidgetBaseContainer(
      {@required this.child,
      @required this.width,
      @required this.height,
      @required this.padding,
      @required this.color,
      @required this.borderColor,
      Key key})
      : super(key: key);

  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(padding),
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: color,
        border: Border.all(color: borderColor),
        borderRadius: const BorderRadius.all(const Radius.circular(8.0)),
      ),
      child: Material(color: color, child: child),
    );
  }
}
