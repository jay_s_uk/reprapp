import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:intl/intl.dart';

class TileCoordinatesSelect extends TileSelect {
  TileCoordinatesSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_COORDINATES,
          tileSize: tileSize,
          widget: _TileStatic(settings),
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileCoordinatesSelect(TileSize tileSize) {
    return TileCoordinatesSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileCoordinatesSpace extends TileSpace {
  TileCoordinatesSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings),
          widgetEdit: _TileStatic(settings),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileCoordinatesSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileCoordinatesSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;

  _TileStatic(this.settings);

  @override
  Widget build(BuildContext context) {
    return _Content("100.0", "0.0", "25.5");
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;

  _TileLive(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;

  _TileLivePrinter(this.printer);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Move move = watch(printerMoveProvider(printer));
    if (move != null && move.axes != null) {
      var axes = move?.axes;
      return _Content(_formatPosition(axes[0]), _formatPosition(axes[1]),
          _formatPosition(axes[2]));
    } else {
      return _Content("-", "-", "-");
    }
  }
}

String _formatPosition(var axis) {
  return coordinateFormat(axis?.machinePosition).toString() ?? "-";
}

class _Content extends StatelessWidget {
  final String x;
  final String y;
  final String z;

  _Content(this.x, this.y, this.z);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(TILE_PADDING),
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _Position("X", x),
              _Position("Y", y),
              _Position("Z", z),
            ]));
  }
}

class _Position extends StatelessWidget {
  _Position(this.coordinate, this.value);

  final String value;
  final String coordinate;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          coordinate,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 12,
            // color: Theme.of(context).textTheme.bodyText1.color,
          ),
        ),
        Expanded(
          child: Text(
            value,
            textAlign: TextAlign.right,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
              // color: Theme.of(context).textTheme.bodyText2.color,
            ),
          ),
        ),
      ],
    );
  }
}

String coordinateFormat(double value, {int decimals = 1}) =>
    NumberFormat("##0" + (decimals > 0 ? ".0" : "")).format(value);
