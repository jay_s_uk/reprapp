import 'package:dac/libraries/format_thousands.dart';
import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:riverpod/all.dart';

class TileJobFilamentSelect extends TileSelect {
  TileJobFilamentSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_JOB_FILAMENT,
          tileSize: tileSize,
          widget: _TileStatic(settings, tileSize),
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobFilamentSelect(TileSize tileSize) {
    return TileJobFilamentSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileJobFilamentSpace extends TileSpace {
  TileJobFilamentSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings, spaceTile.tileSize),
          widgetEdit: _TileStatic(settings, spaceTile.tileSize),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobFilamentSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileJobFilamentSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileStatic(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context) {
    return _ProgressTotal(
      Printer(),
      total: [1234, 345, 56],
      used: [321, 21, 43],
    );
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileLive(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer, tileSize);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;
  final TileSize tileSize;

  _TileLivePrinter(this.printer, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    JobFile jobFile = watch(printerJobFileNotifierProvider(printer)).jobFile;
    JobStatus jobStatus = watch(printerJobStatusProvider(printer).state);
    return _ProgressTotal(
      printer,
      total: jobFile?.filamentsLength,
      used: jobStatus?.progress?.filamentsLength,
    );
    // return _ProgressTotal(
    //   total: [10000],
    //   used: [000],
    // );
  }
}

class _ProgressTotal extends StatelessWidget {
  final Printer printer;
  final List<double> total;
  final List<double> used;

  _ProgressTotal(
    this.printer, {
    @required this.total,
    @required this.used,
  });

  @override
  Widget build(BuildContext context) {
    double totals = total?.reduce((a, b) => a + b) ?? 0;
    double useds = used?.reduce((a, b) => a + b) ?? 0;

    return Container(
      child: _SingleLine(
        printer,
        title: "Filament",
        value: useds,
        totalValue: totals,
        valueColor: Theme.of(context).textTheme.bodyText1.color,
        valueBackgroundColor: Theme.of(context).canvasColor,
        thick: 30,
      ),
    );
  }
}

List<Color> _colors = [
  Colors.brown,
  Colors.red,
  Colors.blue,
  Colors.green,
];

class _SingleLine extends StatelessWidget {
  const _SingleLine(
    this.printer, {
    Key key,
    @required this.value,
    @required this.totalValue,
    this.title,
    @required this.valueColor,
    @required this.valueBackgroundColor,
    this.thick = 10,
  })  : assert(value != null),
        assert(value >= 0),
        super(key: key);

  final Printer printer;
  final double value;
  final double totalValue;
  final String title;
  final Color valueColor;
  final Color valueBackgroundColor;
  final double thick;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Container(
        // color: Colors.red,
        child: Stack(
          // fit: StackFit.loose,
          children: [
            Container(
              margin: EdgeInsets.all(1),
            ),
            Positioned(
                bottom: -35,
                left: 0,
                child: _Filament(
                    printer, totalValue > 0 ? value / totalValue : 0)),
            Positioned(
              top: 0,
              right: 0,
              child:
                  Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                Text(
                  "Filament mm",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(fontSize: 10),
                ),
                Text(
                  formatThousands(totalValue, decimals: 1),
                  // textAlign: TextAlign.left,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(fontSize: 10),
                ),
              ]),
            ),
            Positioned(
              top: 30,
              right: 0,
              child: Text(
                "- " + formatThousands(value, decimals: 1),
                textAlign: TextAlign.start,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: 10,
                    color: valueColor,
                    backgroundColor: valueBackgroundColor),
              ),
            ),
            Positioned(
              top: 45,
              right: 0,
              child: Text(formatThousands(totalValue - value, decimals: 1),
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        fontSize: 10,
                        color: valueColor,
                        backgroundColor: valueBackgroundColor,
                      )),
            ),
          ],
        ),
      ),
    );
  }
}

class _Filament extends StatelessWidget {
  _Filament(this.printer, this.consumedPercent);

  final Printer printer;
  final double consumedPercent;

  @override
  Widget build(BuildContext context) {
    const double size = 36;
    const double inner = size / 3;
    double consumedWidth = (size - inner) * consumedPercent;
    double consumedPos = inner * consumedPercent;

    return Container(
        width: size * 2,
        height: size * 2,
        child: Stack(children: [
          Positioned(
            top: size - consumedPos,
            left: size / 2,
            child: Container(
              width: size * 2,
              height: 1,
              color: Colors.blueGrey,
            ),
          ),
          _CircleFill(
            maxSize: size,
            size: size - consumedWidth,
            color: Colors.blueGrey,
          ),
          _CircleFill(
            maxSize: size,
            size: inner,
            color: Theme.of(context).canvasColor,
          ),
          _Rotation(child: _InnerCircles(size), printer: printer),
        ]));
  }
}

class _InnerCircles extends StatelessWidget {
  final double size;

  _InnerCircles(this.size);

  @override
  Widget build(BuildContext context) {
    const double width = 1;
    var color = Colors.black45;
    return Container(
      width: size,
      height: size,
      // color: Colors.redAccent,
      child: Stack(
        children: [
          _CircleInterior(
            color: color,
            thick: 1,
            size: size / 3,
            posX: size / 3,
            posY: size / 3,
          ),
          _CircleInterior(
            color: color,
            thick: width,
            size: size / 3,
            posX: width / 2,
            posY: size / 3,
          ),
          _CircleInterior(
            color: color,
            thick: width,
            size: size / 3,
            posX: size - size / 3 - width / 2,
            posY: size / 3,
          ),
          _CircleInterior(
            color: color,
            thick: width,
            size: size / 3,
            posX: size / 3,
            posY: width / 2,
          ),
          _CircleInterior(
            color: color,
            thick: width,
            size: size / 3,
            posX: size / 3,
            posY: size - size / 3 - width / 2,
          ),
        ],
      ),
    );
  }
}

class _CircleFill extends StatelessWidget {
  _CircleFill({this.maxSize, this.size, this.color});
  final double maxSize;
  final double size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    double side = maxSize / 2 - size / 2;
    return Positioned(
      top: side > 0 ? side : 0,
      left: side > 0 ? side : 0,
      child: Container(
        constraints: BoxConstraints(minWidth: size, minHeight: size),
        width: size,
        height: size,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: color,
        ),
      ),
    );
  }
}

class _CircleInterior extends StatelessWidget {
  _CircleInterior({this.posX, this.posY, this.size, this.thick, this.color});
  final double posX;
  final double posY;
  final double size;
  final double thick;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: posX,
      top: posY,
      child: Container(
        width: size,
        height: size,
        decoration: new BoxDecoration(
          border: Border.all(color: color, width: thick),
          shape: BoxShape.circle,
          // color: color,
        ),
      ),
    );
  }
}

class _Rotation extends StatefulWidget {
  _Rotation({Key key, this.printer, this.child}) : super(key: key);

  final Widget child;
  final Printer printer;

  @override
  _RotationState createState() => _RotationState(printer, child);
}

class _RotationState extends State<_Rotation>
    with SingleTickerProviderStateMixin {
  _RotationState(this.printer, this.child);

  final Widget child;
  final Printer printer;

  AnimationController _controller;
  double consumed = 0;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 10000),
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        JobStatus value = watch(printerJobStatusProvider(printer).state);
        JobFile file = watch(printerJobFileProvider(printer));
        double totalSize = file?.filamentsLength?.reduce((a, b) => a + b) ?? 0;
        double newValue = value?.progress?.filamentsTotalLength ?? 0;
        if (totalSize > newValue) {
          _controller.repeat();
          consumed = newValue;
        } else {
          _controller.stop();
        }
        return child; // Hello world
      },
      child: RotationTransition(
        turns: Tween(begin: 0.0, end: -0.25).animate(_controller),
        child: child,
      ),
    );
  }
}
