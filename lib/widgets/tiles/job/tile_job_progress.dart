import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class TileJobProgressSelect extends TileSelect {
  TileJobProgressSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_JOB_PROGRESS,
          tileSize: tileSize,
          widget: _TileStatic(settings, tileSize),
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobProgressSelect(TileSize tileSize) {
    return TileJobProgressSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileJobProgressSpace extends TileSpace {
  TileJobProgressSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings, spaceTile.tileSize),
          widgetEdit: _TileStatic(settings, spaceTile.tileSize),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobProgressSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileJobProgressSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileStatic(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context) {
    return _BaseWidget(40);
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileLive(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer, tileSize);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;
  final TileSize tileSize;

  _TileLivePrinter(this.printer, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    JobStatus jobStatus = watch(printerJobStatusProvider(printer).state);

    double percent = jobStatus?.progress?.printedPercent ?? 0;
    return _BaseWidget(percent);
  }
}

class _BaseWidget extends StatelessWidget {
  final double percent;
  _BaseWidget(this.percent);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(bottom: 10),
              child: AspectRatio(
                aspectRatio: 1,
                child: PieChart(
                  PieChartData(
                      borderData: FlBorderData(show: false),
                      sectionsSpace: 0,
                      centerSpaceRadius: double.infinity,
                      startDegreeOffset: 270,
                      sections: showingSections(context, percent)),
                ),
              ),
            ),
          ),
          Text(
            "$percent%",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  List<PieChartSectionData> showingSections(
      BuildContext context, double percent) {
    return [
      PieChartSectionData(
        color: Colors.blue,
        value: percent,
        showTitle: false,
        radius: 20,
      ),
      PieChartSectionData(
        color: Colors.blue.withAlpha(30),
        value: 100 - percent,
        showTitle: false,
        radius: 18,
      )
    ];
  }
}
