import 'package:auto_size_text/auto_size_text.dart';
import 'package:dac/libraries/format_thousands.dart';
import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class TileJobFilamentsSelect extends TileSelect {
  TileJobFilamentsSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_JOB_FILAMENTS,
          tileSize: tileSize,
          widget: _TileStatic(settings, tileSize),
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobFilamentsSelect(TileSize tileSize) {
    return TileJobFilamentsSelect._(
        tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileJobFilamentsSpace extends TileSpace {
  TileJobFilamentsSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings, spaceTile.tileSize),
          widgetEdit: _TileStatic(settings, spaceTile.tileSize),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobFilamentsSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileJobFilamentsSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileStatic(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context) {
    return _Base(
      total: [1234, 345, 56],
      used: [321, 21, 43],
    );
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileLive(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer, tileSize);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;
  final TileSize tileSize;

  _TileLivePrinter(this.printer, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    JobFile jobFile = watch(printerJobFileNotifierProvider(printer)).jobFile;
    JobStatus jobStatus = watch(printerJobStatusProvider(printer).state);
    // return _Base(
    //   total: jobFile?.filamentsLength,
    //   used: jobStatus?.progress?.filamentsLength,
    // );
    return _Base(
      total: [1234, 345, 45],
      used: [234, 45, 5],
    );
  }
}

class _Base extends StatelessWidget {
  final List<double> total;
  final List<double> used;

  _Base({
    @required this.total,
    @required this.used,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _ProgressTotal(total: total, used: used),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Filament Used",
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(fontSize: 10),
                    ),
                    Text(
                      formatThousands(
                          used?.reduce((value, element) => value + element) ??
                              0,
                          unit: "mm",
                          decimals: 1),
                      textAlign: TextAlign.right,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(fontSize: 10),
                    ),
                  ]),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "Total",
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(fontSize: 10),
                  ),
                  Text(
                    formatThousands(
                        total?.reduce((value, element) => value + element) ?? 0,
                        unit: "mm",
                        decimals: 1),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 10),
                  ),
                ],
              ),
            ],
          )
        ]);
  }
}

class _ProgressTotal extends StatelessWidget {
  final List<double> total;
  final List<double> used;

  _ProgressTotal({
    @required this.total,
    @required this.used,
  });

  @override
  Widget build(BuildContext context) {
    double totals = total?.reduce((a, b) => a + b) ?? 1;
    double useds = used?.reduce((a, b) => a + b) ?? 0;

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _ChartLines(
            total: total,
            used: used,
          ),
        ],
      ),
    );
  }
}

List<String> _chars() => (List.generate(26, (int index) => index + 65))
    .map((e) => String.fromCharCode(e))
    .toList();

List<Color> _colors = [
  Colors.yellow,
  Colors.red,
  Colors.blue,
  Colors.green,
];

class _ChartLines extends StatelessWidget {
  final List<double> total;
  final List<double> used;

  _ChartLines({
    @required this.total,
    @required this.used,
  });

  @override
  Widget build(BuildContext context) {
    double totals = total?.reduce((a, b) => a + b) ?? 1;
    double useds = used?.reduce((a, b) => a + b) ?? 0;

    List<_StackedLine> charts = [];
    var names = _chars();
    for (int i = 0; i < total.length; i++) {
      charts.add(_StackedLine(
        title: names[i],
        value: used[i],
        maxValue: total[i],
        totalValue: totals,
        valueColor: _colors[i + 1],
        valueBackgroundColor: _colors[i + 1].withAlpha(20),
        thick: 25,
      ));
    }

    return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: charts);
  }
}

class _StackedLine extends StatelessWidget {
  const _StackedLine({
    Key key,
    @required this.value,
    @required this.maxValue,
    @required this.totalValue,
    @required this.title,
    @required this.valueColor,
    @required this.valueBackgroundColor,
    this.thick = 10,
  })  : assert(title != null),
        assert(value != null),
        assert(value >= 0),
        assert(value <= maxValue),
        assert(maxValue <= totalValue),
        super(key: key);

  final double value;
  final double maxValue;
  final double totalValue;
  final String title;
  final Color valueColor;
  final Color valueBackgroundColor;
  final double thick;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final valueWidth = constraints.maxWidth * (value / totalValue);
      final valueMaxWidth = constraints.maxWidth * (maxValue / totalValue);
      return Padding(
        padding: const EdgeInsets.only(bottom: 5.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              constraints: BoxConstraints(minWidth: valueMaxWidth),
              child: IntrinsicWidth(
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: Text(title,
                            style: Theme.of(context).textTheme.caption.copyWith(
                                fontSize: 10,
                                color: valueColor,
                                fontWeight: FontWeight.bold)),
                      ),
                      AutoSizeText(
                        formatThousands(maxValue, unit: "mm", decimals: 1),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(fontSize: 8),
                      ),
                    ]),
              ),
            ),
            Row(
              children: [
                Container(
                  color: valueColor,
                  height: thick,
                  width: valueWidth,
                ),
                Container(
                  color: valueBackgroundColor,
                  height: thick,
                  width: valueMaxWidth - valueWidth,
                ),
              ],
            ),
            Container(
              constraints: BoxConstraints(minWidth: valueWidth),
              child: IntrinsicWidth(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    AutoSizeText(
                      formatThousands(value, unit: "mm", decimals: 1),
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontSize: 8,
                          color: valueColor,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
