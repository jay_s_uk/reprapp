import 'package:auto_size_text/auto_size_text.dart';
import 'package:dac/libraries/format_length.dart';
import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/base_service.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:filesize/filesize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class TileJobFileSelect extends TileSelect {
  TileJobFileSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_JOB_FILE,
          tileSize: tileSize,
          widget: _TileStatic(settings, tileSize),
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobFileSelect(TileSize tileSize) {
    return TileJobFileSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileJobFileSpace extends TileSpace {
  TileJobFileSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings, spaceTile.tileSize),
          widgetEdit: _TileStatic(settings, spaceTile.tileSize),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileJobFileSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileJobFileSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileStatic(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context) {
    return _BaseWidget(
      "0:/gcodes/file.gcode",
      0,
      Duration(seconds: 0),
      0,
      0,
    );
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  final TileSize tileSize;

  _TileLive(this.settings, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer, tileSize);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;
  final TileSize tileSize;

  _TileLivePrinter(this.printer, this.tileSize);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    JobFile jobFile = watch(printerJobFileNotifierProvider(printer))?.jobFile;

    return _BaseWidget(
      jobFile?.fileName,
      jobFile?.fileSize,
      jobFile?.totaDuration,
      jobFile?.filamentsLength?.reduce((a, b) => a + b),
      jobFile?.totalHeight,
    );
  }
}

class _BaseWidget extends StatelessWidget {
  final String fileName;
  final int fileSize;
  final Duration time;
  final double filamentsLength;
  final double height;

  _BaseWidget(this.fileName, this.fileSize, this.time, this.filamentsLength,
      this.height);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "File name",
            style: Theme.of(context).textTheme.caption,
          ),
          AutoSizeText(
            fileName?.split('/')?.last ?? "-",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText1,
            minFontSize: 6,
          ),
          Container(height: 5),
          Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        "File size",
                        style: Theme.of(context).textTheme.caption,
                      ),
                      Text(filesize(fileSize ?? 0)),
                    ]),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "Filament",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(formatLength(filamentsLength ?? 0)),
                      ]),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "Height",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(formatLength(height ?? 0)),
                      ]),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Time",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(formatDuration(time ?? Duration(seconds: 0))),
                      ]),
                ),
              ]),
        ],
      ),
    );
  }
}
