import 'package:dac/model/printer.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

import '../temperature_chart.dart';

class TileChartSelect extends TileSelect {
  TileChartSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_CHART,
          tileSize: tileSize,
          widget: _TileStatic(settings),
          tileSettings: TilePrinterSettings(settings),
        );
  factory TileChartSelect(TileSize tileSize) {
    return TileChartSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileChartSpace extends TileSpace {
  TileChartSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
            space: space,
            spaceTile: spaceTile,
            widgetLive: _TileLive(settings),
            widgetEdit: _TileStatic(settings),
            isEdit: isEdit,
            tileSettings: TilePrinterSettings(settings));

  factory TileChartSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileChartSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  _TileLive(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return SegmentsLineChart(printer);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  _TileStatic(this.settings);

  @override
  Widget build(BuildContext context) {
    return SegmentsLineChartStatic();
  }
}
