import 'package:dac/model/space.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/widgets/printer_selector.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class TilePrinterSettings extends TileSettings {
  TilePrinterSettings(PrinterSpaceTileSettings settings)
      : super(settingsWidget: _TileSettingsWidget(settings), title: "Printer");
}

abstract class HasPrinterSettings {
  int get printerId;
}

class PrinterSpaceTileSettings
    implements HasPrinterSettings, HasSpaceTileSettings {
  final int printerId;
  final String label;

  PrinterSpaceTileSettings.settings({
    this.printerId,
    @required this.label,
  });

  factory PrinterSpaceTileSettings.static() {
    return PrinterSpaceTileSettings.settings(label: "Printer");
  }

  factory PrinterSpaceTileSettings(SpaceTileSettings settings) {
    return PrinterSpaceTileSettings.settings(
      printerId: settings.getOrFunctionValue("printerId", int.tryParse, null),
      label: settings.getOrValue("label", null),
    );
  }

  SpaceTileSettings spaceTileSettings() {
    return SpaceTileSettings.create({
      "printerId": printerId.toString(),
      "label": label,
    });
  }

  PrinterSpaceTileSettings copyWith({
    int printerId,
    String label,
  }) {
    if ((printerId == null || identical(printerId, this.printerId)) &&
        (label == null || identical(label, this.label))) {
      return this;
    }

    return PrinterSpaceTileSettings.settings(
      printerId: printerId ?? this.printerId,
      label: label ?? this.label,
    );
  }
}

class _TileSettingsWidget extends ConsumerWidget {
  final PrinterSpaceTileSettings initialSettings;

  _TileSettingsWidget(this.initialSettings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    PrinterSpaceTileSettings settings;
    if (initialSettings.printerId == null) {
      var records = watch(printersListProvider).records;
      if (records.length > 0) {
        settings = initialSettings.copyWith(
            printerId: records.first.id, label: records.first.name);
      } else {
        settings = initialSettings;
      }
    } else {
      settings = initialSettings;
    }
    context
        .read(spaceTileSettingsNotifierProvider)
        .load(settings.spaceTileSettings());
    return Padding(
      padding: EdgeInsets.all(20),
      child: Center(
        child: PrinterSelector(
            settings.printerId,
            (p) => context.read(spaceTileSettingsNotifierProvider).change(
                settings
                    .copyWith(printerId: p.id, label: p.name)
                    .spaceTileSettings())),
      ),
    );
  }
}
