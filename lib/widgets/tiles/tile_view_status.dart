import 'package:auto_size_text/auto_size_text.dart';
import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_communication_providers.dart' as conn;
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/connection_icon.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class TileStatusSelect extends TileSelect {
  TileStatusSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_STATUS,
          tileSize: tileSize,
          widget: _TileStatic(settings),
          tileSettings: TilePrinterSettings(settings),
        );
  factory TileStatusSelect(TileSize tileSize) {
    return TileStatusSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileStatusSpace extends TileSpace {
  TileStatusSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings),
          widgetEdit: _TileStatic(settings),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileStatusSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileStatusSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  _TileStatic(this.settings);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 2,
          right: 5,
          child: ConnectionIcon(
            connectionState: conn.ConnectionState.LOADED,
          ),
        ),
        Center(
            child: Text("Printing",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16
                    // color: Theme.of(context).colorScheme.error,
                    )))
      ],
    );
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  _TileLive(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;
  _TileLivePrinter(this.printer);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    MachineStatus machineStatus = watch(printerMachineStatusProvider(printer));
    Color backgroundColor = Colors.limeAccent;
    String title = "?";
    switch (machineStatus) {
      case MachineStatus.READING_CONFIGURATION:
        title = "Reading";
        break;
      case MachineStatus.FLASHING_FIRMWARE:
        title = "Flashing";
        backgroundColor = Colors.red;
        break;
      case MachineStatus.HALTED:
        title = "Halted";
        backgroundColor = Colors.redAccent;
        break;
      case MachineStatus.OFF:
        title = "Off";
        backgroundColor = Colors.black12;
        break;
      case MachineStatus.PAUSING_PRINT:
        title = "Pausing";
        backgroundColor = Colors.amber;
        break;
      case MachineStatus.RESUMING_PRINT:
        title = "Resuming";
        backgroundColor = Colors.amber;
        break;
      case MachineStatus.PRINT_PAUSED:
        title = "Paused";
        backgroundColor = Colors.deepOrangeAccent;
        break;
      case MachineStatus.SIMULATING_PRINT:
        title = "Simulating";
        backgroundColor = Colors.lightGreen;
        break;
      case MachineStatus.PRINTING:
        title = "Printing";
        backgroundColor = Colors.lightGreen;
        break;
      case MachineStatus.CHANGING_TOOL:
        title = "Changing tool";
        backgroundColor = Colors.amber;
        break;
      case MachineStatus.BUSY:
        title = "Busy";
        backgroundColor = Colors.deepOrangeAccent;
        break;
      case MachineStatus.IDLE:
        title = "Idle";
        backgroundColor = Colors.limeAccent;
        break;
      default:
        backgroundColor = Colors.greenAccent;
    }
    return Stack(
      children: [
        Positioned(
          top: 2,
          right: 5,
          child: PrinterConnectionIcon(printer: printer),
        ),
        Center(
            child: AutoSizeText(title,
                maxLines: 1,
                softWrap: true,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16
                    // color: Theme.of(context).colorScheme.error,
                    )))
      ],
    );
  }
}
