import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/space.dart';
import 'package:dac/printer_icons_icons.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_tool_settings.dart';
import 'package:dac/widgets/tiles/tile_toolheater.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

import '../temperature_chart.dart';

class TileToolSelect extends TileSelect {
  TileToolSelect._(TileSize tileSize, SpaceTilePrinterToolSettings settings)
      : super(
          tileKey: TileKey.PRINTER_TOOL,
          tileSize: tileSize,
          widget: _TileStatic(settings),
          tileSettings: TileToolSettings(settings),
        );
  factory TileToolSelect(TileSize tileSize) {
    return TileToolSelect._(tileSize, SpaceTilePrinterToolSettings.static());
  }
}

class TileToolSpace extends TileSpace {
  TileToolSpace._(Space space, SpaceTile spaceTile,
      SpaceTilePrinterToolSettings settings, bool isEdit)
      : super(
            space: space,
            spaceTile: spaceTile,
            widgetLive: _TileLive(settings),
            widgetEdit: _TileStatic(settings),
            isEdit: isEdit,
            tileSettings: TileToolSettings(settings));

  factory TileToolSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileToolSpace._(space, spaceTile,
        SpaceTilePrinterToolSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final SpaceTilePrinterToolSettings settings;

  _TileStatic(this.settings);

  @override
  Widget build(BuildContext context) {
    return TileContentToolHeaterStatic(
      "Tool " + (settings?.toolNumber ?? 0).toString(),
      NozzleIcon(),
      Heater(0, 100, HeaterState.OFF),
      TemperatureSet(0, 200, 0),
    );
  }
}

class _TileLive extends ConsumerWidget {
  final SpaceTilePrinterToolSettings settings;

  _TileLive(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLiveSettings(
        printer, settings.toolNumber, settings.heaterNumber);
  }
}

class _TileLiveSettings extends ConsumerWidget {
  final Printer printer;
  final int toolNumber;
  final int heaterNumber;

  _TileLiveSettings(this.printer, this.toolNumber, this.heaterNumber);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    if (printer != null && toolNumber != null) {
      Tool tool =
          watch(printerToolProvider(PrinterTool(printer, toolNumber))).state;
      ToolInfo toolInfo = watch(printerToolInfoProvider(
              PrinterToolHeater(printer, toolNumber, heaterNumber)))
          .state;
      if (tool != null && toolInfo != null && tool.heaters != null) {
        Heater heater = tool.heaters.firstWhere(
            (element) => element.number == heaterNumber,
            orElse: () => null);
        TemperatureSet temperatureSet = getTemperatureSetFromHeaterNumber(
            tool.temperaturesSets, heaterNumber);
        return TileContentToolHeater(
          toolInfo?.name ?? ("Tool " + (toolNumber?.toString() ?? "-")),
          NozzleIcon(color: heaterColor[heaterNumber]),
          heater,
          temperatureSet,
        );
      } else {
        if (toolNumber != null) {
          return Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                    printer.name + " " + (printer.enabled ? "" : "is offline")),
                Text("Tool " + toolNumber.toString() + " not available"),
              ]);
        } else {
          return Center(child: Text("Tool not set"));
        }
      }
    } else {
      return Center(
          child: Text("Tool " + (toolNumber?.toString() ?? "not set")));
    }
  }

  TemperatureSet getTemperatureSetFromHeaterNumber(
      List<TemperatureSet> temperaturesSet, int heaterNumber) {
    return temperaturesSet.firstWhere(
        (element) => element.heaterNumber == heaterNumber,
        orElse: () => null);
  }
}
