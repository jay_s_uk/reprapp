import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/space.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/temperature_chart.dart';
import 'package:dac/widgets/tiles/tile_printer_settings.dart';
import 'package:dac/widgets/tiles/tile_toolheater.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttericon/rpg_awesome_icons.dart';
import 'package:hooks_riverpod/all.dart';

class TileBedSelect extends TileSelect {
  TileBedSelect._(TileSize tileSize, PrinterSpaceTileSettings settings)
      : super(
          tileKey: TileKey.PRINTER_BED,
          tileSize: tileSize,
          widget: _TileStatic(settings),
          tileSettings: TilePrinterSettings(settings),
        );

  factory TileBedSelect(TileSize tileSize) {
    return TileBedSelect._(tileSize, PrinterSpaceTileSettings.static());
  }
}

class TileBedSpace extends TileSpace {
  TileBedSpace._(Space space, SpaceTile spaceTile,
      PrinterSpaceTileSettings settings, bool isEdit)
      : super(
          space: space,
          spaceTile: spaceTile,
          widgetLive: _TileLive(settings),
          widgetEdit: _TileStatic(settings),
          isEdit: isEdit,
          tileSettings: TilePrinterSettings(settings),
        );
  factory TileBedSpace(Space space, SpaceTile spaceTile, bool isEdit) {
    return TileBedSpace._(space, spaceTile,
        PrinterSpaceTileSettings(spaceTile.tileSettings), isEdit);
  }
}

class _TileStatic extends StatelessWidget {
  final PrinterSpaceTileSettings settings;
  _TileStatic(this.settings);
  @override
  Widget build(BuildContext context) {
    return TileContentToolHeaterStatic(
      "Bed",
      Icon(
        RpgAwesome.hot_surface,
      ),
      Heater(0, 50, HeaterState.STANDBY),
      TemperatureSet(0, 70, 50),
    );
  }
}

class _TileLive extends ConsumerWidget {
  final PrinterSpaceTileSettings settings;
  _TileLive(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printer = watch(printerProviderFromId(settings.printerId));
    return _TileLivePrinter(printer);
  }
}

class _TileLivePrinter extends ConsumerWidget {
  final Printer printer;

  _TileLivePrinter(this.printer);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    if (printer != null) {
      Bed bed = watch(printerBedProvider(printer)).state;
      if (bed?.heater != null) {
        return TileContentToolHeater(
          "Bed",
          Icon(RpgAwesome.hot_surface,
              color: heaterColor[bed?.heater?.number ?? 0]),
          bed?.heater,
          bed?.temperatureSet,
        );
      } else {
        return Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(printer.name + " " + (printer.enabled ? "" : "is offline")),
              Text("Bed not available"),
            ]);
      }
    } else {
      return Center(child: Text("Printer not set"));
    }
  }
}
