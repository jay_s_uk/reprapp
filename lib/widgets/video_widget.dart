import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';

class VideoWidget extends StatefulWidget {
  final VlcPlayerController videoViewController;
  final String streamUrl;

  VideoWidget(this.videoViewController, this.streamUrl);

  @override
  _VideoState createState() => _VideoState(videoViewController, streamUrl);
}

class _VideoState extends State<VideoWidget> with WidgetsBindingObserver {
  final VlcPlayerController videoViewController;
  final String streamUrl;

  _VideoState(this.videoViewController, this.streamUrl);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('state = $state');
    switch (state) {
      case AppLifecycleState.resumed:
        videoViewController.play();
        break;
      case AppLifecycleState.paused:
        videoViewController.pause();
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.detached:
        videoViewController.stop();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new VlcPlayer(
      // options: ["transform-type=vflip"],
      aspectRatio: 16 / 9,
      autoplay: true,
      url: streamUrl ??
          "https://www.duet3d.com/image/catalog/logo/120_D3d_words_single_line_inv_nb.png",
      controller: videoViewController,
      placeholder: Center(child: CircularProgressIndicator()),
    );
  }
}
