import 'package:dac/model/space.dart';
import 'package:dac/scaffolds/tile_settings_page.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:dac/widgets/tiles_select_panel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class TilesSelectPage extends StatefulWidget {
  final Space space;
  TilesSelectPage(this.space);

  @override
  _TilesSelectState createState() => _TilesSelectState(space);
}

class _TilesSelectState extends State<TilesSelectPage> {
  final Space space;
  _TilesSelectState(this.space);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [
      _TileGroupStatus(space),
      _TileGroupCoordinates(space),
      _TileGroupExtruders(space),
      _TileGroupBed(),
      _TileGroupCharts(space),
      _TileGroupJob(space),
      _TileGroupWebcam(),
    ];

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop()),
        centerTitle: true,
        title: Text("Select Tile"),
        actions: [_TileAddButton(space)],
      ),
      extendBodyBehindAppBar: false,
      body: ListView.builder(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(vertical: 20),
          itemCount: items.length,
          itemBuilder: (_, index) => Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: items[index])),
    );
  }
}

class _TileAddButton extends ConsumerWidget {
  final Space space;
  _TileAddButton(this.space);
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    var tileSelected = watch(tileSelectedProvider.state);
    return TextButton(
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.resolveWith((states) {
          return states.contains(MaterialState.disabled)
              ? null
              : Theme.of(context).iconTheme.color;
        })),
        child: Text("Add"),
        onPressed: tileSelected.isSelected
            ? () => Navigator.pushNamed(context, TileSettingsPage.routeName,
                    arguments: SpaceTileSelected(
                        space: space, tileSelect: tileSelected.selected))
                .then((value) =>
                    value != null && value ? {Navigator.pop(context)} : {})
            : null);
  }
}

class _TileGroup extends StatelessWidget {
  final String title;
  final List<TileSelect> items;

  _TileGroup(this.title, this.items);

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline4,
          )),
      TilesPanelSelect(tiles: items)
    ]);
  }
}

class _TileGroupCoordinates extends StatelessWidget {
  final Space space;
  _TileGroupCoordinates(this.space);
  @override
  Widget build(BuildContext context) {
    return _TileGroup("Coordinates", [
      tileFactorySelect(TileKey.PRINTER_COORDINATES, TileSize(1, 1)),
    ]);
  }
}

class _TileGroupCharts extends StatelessWidget {
  final Space space;
  _TileGroupCharts(this.space);
  @override
  Widget build(BuildContext context) {
    return _TileGroup("Temperature Charts", [
      tileFactorySelect(TileKey.PRINTER_CHART, TileSize(2, 1)),
      tileFactorySelect(TileKey.PRINTER_CHART, TileSize(2, 2)),
      tileFactorySelect(TileKey.PRINTER_CHART, TileSize(3, 2)),
      tileFactorySelect(TileKey.PRINTER_CHART, TileSize(4, 2)),
      tileFactorySelect(TileKey.PRINTER_CHART, TileSize(4, 3)),
    ]);
  }
}

class _TileGroupExtruders extends StatelessWidget {
  final Space space;
  _TileGroupExtruders(this.space);
  @override
  Widget build(BuildContext context) {
    return _TileGroup("Tools", [
      tileFactorySelect(TileKey.PRINTER_TOOL, TileSize(4, 1)),
    ]);
  }
}

class _TileGroupBed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _TileGroup("Bed", [
      tileFactorySelect(TileKey.PRINTER_BED, TileSize(4, 1)),
    ]);
  }
}

class _TileGroupStatus extends StatelessWidget {
  final Space space;
  _TileGroupStatus(this.space);
  @override
  Widget build(BuildContext context) {
    return _TileGroup("Status", [
      tileFactorySelect(TileKey.PRINTER_STATUS, TileSize(1, 1)),
      tileFactorySelect(TileKey.PRINTER_STATUS, TileSize(2, 1)),
    ]);
  }
}

class _TileGroupJob extends StatelessWidget {
  final Space space;
  _TileGroupJob(this.space);
  @override
  Widget build(BuildContext context) {
    return _TileGroup("Job", [
      tileFactorySelect(TileKey.PRINTER_JOB_PROGRESS, TileSize(1, 1)),
      tileFactorySelect(TileKey.PRINTER_JOB_LAYERS, TileSize(1, 1)),
      tileFactorySelect(TileKey.PRINTER_JOB_FILAMENT, TileSize(1, 1)),
      // tileFactorySelect(TileKey.PRINTER_JOB_FILAMENTS, TileSize(2, 2)),
      tileFactorySelect(TileKey.PRINTER_JOB_FILE, TileSize(4, 1)),
    ]);
  }
}

class _TileGroupWebcam extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _TileGroup("Webcam", [
      tileFactorySelect(TileKey.CAM, TileSize(1, 1)),
      tileFactorySelect(TileKey.CAM, TileSize(2, 1)),
      tileFactorySelect(TileKey.CAM, TileSize(2, 2)),
      tileFactorySelect(TileKey.CAM, TileSize(3, 2)),
      tileFactorySelect(TileKey.CAM, TileSize(4, 3)),
    ]);
  }
}
