import 'package:dac/model/printer.dart';
import 'package:dac/scaffolds/printer_page.dart';
import 'package:dac/services/printer_communication_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/widgets/app_drawer.dart';
import 'package:dac/widgets/connection_icon.dart';
import 'package:flutter/material.dart' hide ConnectionState;
import 'package:flutter/rendering.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:theme_provider/theme_provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'printer_add_page.dart';

class PrintersScaffold extends ConsumerWidget {
  static const String routeName = '/printers';

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<Printer> printers = watch(printersListProvider).records;
    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          title: Text("Printers"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => PrinterAddPage())),
          child: Icon(Icons.add),
        ),
        body:
            printers.length > 0 ? _PrintersListWidget(printers) : _EmptyList());
  }
}

class _EmptyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String color = "blue";
    String image = "assets/thinker.png";
    switch (ThemeProvider.controllerOf(context).currentThemeId) {
      case "very_dark":
        color = "gray";
    }
    double height = MediaQuery.of(context).size.height;

    return Container(
      padding: EdgeInsets.all(30),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                      icon: Icon(Icons.info_outline),
                      onPressed: () => _openCopyright(context)),
                ]),
            SizedBox(height: height / 3, child: Image.asset(image)),
            Padding(
              padding: EdgeInsets.only(top: 40),
              child: Center(
                child: Text(
                  "\"Hmmm... that $color buttom must be important...\"",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

_openCopyright(BuildContext context) {
  showDialog(
      context: context,
      builder: (_) => AlertDialog(
            title: Text('Copyright'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Linkify(
                    onOpen: (link) => launch(link.url),
                    text: "Made by https://www.thingiverse.com/thing:2559833"),
              ],
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: const Text('Close'),
              ),
            ],
          ));
}

class _PrintersListWidget extends StatelessWidget {
  final List<Printer> printers;
  _PrintersListWidget(this.printers);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.all(5),
      physics: NeverScrollableScrollPhysics(),
      itemCount: printers.length,
      itemBuilder: (context, index) {
        final item = printers[index];

        return Dismissible(
          key: Key(item.hostname),
          confirmDismiss: (direction) => showDeleteDialog(context, item),
          onDismissed: (direction) {
            PrintersService().remove(item.id);
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text(item.hostname + " removed")));
          },
          background: Container(
            color: Colors.red,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Icon(Icons.delete, color: Colors.white),
                  Text('Remove', style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          ),
          child: _PrinterListItem(
            onChanged: (enabled) {
              PrintersService().save(item.copyWith(enabled: enabled));
            },
            printer: item,
          ),
        );
      },
    );
  }

  Future<bool> showDeleteDialog(BuildContext context, Printer printer) async {
    Widget cancelButton = FlatButton(
        child: Text("Cancel"),
        onPressed: () => Navigator.of(context).pop(false));

    Widget continueButton = FlatButton(
      child: Text("Remove"),
      onPressed: () {
        Navigator.of(context).pop(true);
      },
      textColor: Colors.white,
      color: Colors.red,
    );

    AlertDialog alert = AlertDialog(
      title: Text("Remove Printer"),
      content: Text("Do you confirm removing this printer?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    return await showDialog(
        context: context, builder: (BuildContext context) => alert);
  }
}

class _PrinterListItem extends StatelessWidget {
  const _PrinterListItem({
    this.printer,
    this.groupValue,
    this.onChanged,
  });

  final Printer printer;
  final bool groupValue;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, PrinterPage.routeName,
              arguments: printer);
        },
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                      Text(
                        printer.name,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Text(printer.hostname,
                          style: Theme.of(context).textTheme.subtitle2)
                    ])),
                Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Switch(
                          value: printer.enabled,
                          onChanged: (bool newValue) {
                            onChanged(newValue);
                          },
                        ),
                        PrinterConnectionIcon(printer: printer)
                      ]),
                  ConnectionStatusWidget(printer)
                ]),
              ],
            ),
          ),
        ));
  }
}

class ConnectionStatusWidget extends ConsumerWidget {
  final Printer printer;

  ConnectionStatusWidget(this.printer);

  Widget build(BuildContext context, ScopedReader watch) {
    ConnectionState connectionState =
        watch(connectionStateProvider(printer).state);

    String title = "?";
    var color = Theme.of(context).textTheme.caption.color;
    switch (connectionState) {
      case ConnectionState.LOADING:
      case ConnectionState.LOADED:
        title = "Connected";
        break;
      case ConnectionState.ERROR:
        title = "Error";
        color = Theme.of(context).colorScheme.error;
        break;
      case ConnectionState.CLOSED:
        title = "Not connected";
        break;
    }

    return Text(
      title,
      style: TextStyle(
        color: color,
      ),
    );
  }
}

class AnimatedIcon extends StatefulWidget {
  AnimatedIcon({Key key, this.icon}) : super(key: key);

  final Icon icon;

  @override
  _AnimatedIconState createState() => _AnimatedIconState();
}

class _AnimatedIconState extends State<AnimatedIcon>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 15000),
      vsync: this,
    )..repeat();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
      child: widget.icon,
    );
  }
}
