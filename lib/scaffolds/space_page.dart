import 'package:dac/model/space.dart';
import 'package:dac/scaffolds/space_add_page.dart';
import 'package:dac/scaffolds/space_edit_page.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/widgets/app_drawer.dart';
import 'package:dac/widgets/tiles_panel.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class SpacePage extends ConsumerWidget {
  static const String routeName = '/space';

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Space spaceArgument = ModalRoute.of(context).settings.arguments;
    List<Space> records = watch(spacesListProvider).records;
    List<Widget> tabs = records
        .where((element) => element.id != null)
        .map((space) => SpaceTilesLive(space: space))
        .toList();

    SpaceSelected spaceSelected = context.read(spaceSelectedProvider.state);
    int initialIndex = spaceSelected?.index ??
        (spaceArgument != null ? records.indexOf(spaceArgument) : 0);

    if (records.length > 0) {
      return _PageView(tabs, initialIndex, (int index) {
        context.read(spaceSelectedProvider).setSpaceIndex(index);
      });
    } else {
      return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(),
        body: Container(
          padding: EdgeInsets.all(50),
          child: Center(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Add Space"),
                  Container(
                    height: 30,
                  ),
                  FloatingActionButton(
                      child: Icon(Icons.add),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => SpaceAddPage(),
                            fullscreenDialog: true));
                      })
                ]),
          ),
        ),
      );
    }
  }
}

class _PageView extends StatefulWidget {
  final int initialIndex;
  final List<Widget> tabs;
  final Function onChange;

  _PageView(this.tabs, this.initialIndex, this.onChange);

  @override
  _TabBarViewState createState() => _TabBarViewState();
}

class _TabBarViewState extends State<_PageView> with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = _getTabController();
    super.initState();
  }

  TabController _getTabController() {
    TabController tabController = TabController(
      vsync: this,
      length: widget.tabs.length,
      initialIndex: widget.initialIndex > -1 ? widget.initialIndex : 0,
    );
    tabController.addListener(() {
      if (mounted) {
        widget.onChange(_tabController.index);
      }
    });
    return tabController;
  }

  @override
  void didUpdateWidget(_PageView oldWidget) {
    super.didUpdateWidget(oldWidget);
    _tabController.dispose();
    _tabController = _getTabController();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: AppDrawer(),
      appBar: _AppBar(),
      body: Stack(children: [
        TabBarView(
          controller: _tabController,
          children: widget.tabs,
        ),
        Positioned(
          bottom: 30,
          right: 0.0,
          left: 0.0,
          child: _PageIndicator(),
        ),
      ]),
    );
  }
}

class _PageIndicator extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    SpaceSelected spaceSelected = watch(spaceSelectedProvider.state);
    double index = spaceSelected.index.toDouble();

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(),
        index >= 0
            ? DotsIndicator(
                dotsCount: spaceSelected.total,
                position: index,
                decorator: DotsDecorator(
                  color: Theme.of(context)
                      .indicatorColor
                      .withAlpha(50), // Inactive color
                  activeColor: Theme.of(context).indicatorColor,
                ),
              )
            : Container(),
        Container(),
      ],
    );
  }
}

class _AppBar extends ConsumerWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  _AppBar({Key key})
      : preferredSize = Size.fromHeight(60.0),
        super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    SpaceSelected spaceSelected = watch(spaceSelectedProvider.state);
    return AppBar(
      backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
      centerTitle: true,
      title: Row(
        children: [
          Expanded(child: Center(
            child: Consumer(builder: (_, watch, child) {
              if (spaceSelected != null) {
                String text =
                    watch(spaceProvider(spaceSelected.space).state)?.name;
                return Text(
                    (text ?? spaceSelected?.space?.id?.toString()) ?? "Space");
              } else {
                return Text("Space");
              }
            }),
          )),
        ],
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.settings),
          onPressed: () => SpaceEditPage.open(context, spaceSelected.space),
        )
      ],
    );
  }
}
