import 'package:dac/model/space.dart';
import 'package:dac/scaffolds/space_page.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class SpaceAddPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          leading: new IconButton(
            icon: new Icon(Icons.close),
            onPressed: () => Navigator.pop(context),
          ),
          title: Text("Add Space"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _AddForm(),
        ));
  }
}

class _AddForm extends StatefulWidget {
  @override
  _AddFormState createState() => _AddFormState();
}

class _AddFormState extends State<_AddForm> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String startName = "";
    // "Space #" + (context.read(spacesListProvider).count() + 1).toString();
    return FocusTraversalGroup(
        descendantsAreFocusable: true,
        child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.always,
            child: new ListView(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: TextFormField(
                      autofocus: true,
                      controller: _nameController..text = startName,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter a name for the Space';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        icon: const Icon(Icons.book),
                        labelText: 'Name',
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        context
                            .read(spacesListProvider)
                            .save(Space(name: _nameController.text))
                            .then((space) {
                          context.read(spaceSelectedProvider).setSpace(space);
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Space created")));
                          Navigator.of(context).popAndPushNamed(
                              SpacePage.routeName,
                              arguments: space);
                        });
                      }
                    },
                    child: Text('Add'),
                  ),
                ),
              ],
            )));
  }
}
