import 'package:dac/model/space.dart';
import 'package:dac/scaffolds/space_page.dart';
import 'package:dac/scaffolds/tiles_select_page.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/widgets/tiles_panel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class SpaceEditPage extends ConsumerWidget {
  final Space space;
  SpaceEditPage._(this.space);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          leading: IconButton(
            icon: Icon(Icons.done),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: Consumer(builder: (_, watch, child) {
            String text = watch(spaceProvider(space).state)?.name;
            return Text((text ?? space?.id?.toString()) ?? "Space");
          }),
          actions: [
            PopupMenuButton<int>(
              color: Theme.of(context).canvasColor,
              icon: Icon(Icons.more_vert),
              onSelected: (value) {
                switch (value) {
                  case 1:
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => TilesSelectPage(space),
                        fullscreenDialog: true));
                    break;
                  case 2:
                    _showRenameDialog(context, space);
                    break;
                  case 3:
                    _showDeleteDialog(context, space).then((value) {
                      if (value) {
                        context.read(spacesListProvider).delete(space);
                        Navigator.pushReplacementNamed(
                            context, SpacePage.routeName);
                      }
                    });
                    break;
                }
              },
              itemBuilder: (BuildContext context) => <PopupMenuItem<int>>[
                const PopupMenuItem<int>(
                    value: 1,
                    child: ListTile(
                      leading: Icon(Icons.add_box),
                      title: Text('Add Tile'),
                    )),
                const PopupMenuItem<int>(
                    value: 2,
                    child: ListTile(
                      leading: Icon(Icons.edit),
                      title: Text('Rename Space'),
                    )),
                const PopupMenuItem<int>(
                  value: 3,
                  child: ListTile(
                    // tileColor: Colors.red,
                    leading: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                    title: Text(
                      'Delete Space',
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                ),
              ],
            ),
          ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (_) => TilesSelectPage(space), fullscreenDialog: true));
        },
        child: Icon(Icons.add),
      ),
      body: space != null
          ? SpaceTilesEdit(space: space)
          : Container(child: Text("No space selected")),
    );
  }

  Future<bool> _showRenameDialog(BuildContext context, Space space) async {
    var name = space.name;

    Widget cancelButton = FlatButton(
        child: Text("Cancel"), onPressed: () => Navigator.of(context).pop());

    Widget continueButton = FlatButton(
      child: Text("Save"),
      onPressed: () {
        context.read(spaceProvider(space)).rename(name);
        Navigator.of(context).pop();
      },
      textColor: Colors.white,
      color: Colors.red,
    );

    AlertDialog alert = AlertDialog(
      title: Text("Space Name"),
      content: _RenameForm(space, (v) {
        name = v;
      }),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    return await showDialog(
        context: context, builder: (BuildContext context) => alert);
  }

  Future<bool> _showDeleteDialog(BuildContext context, Space space) async {
    Widget cancelButton = FlatButton(
        child: Text("No, Cancel"),
        onPressed: () => Navigator.of(context).pop(false));

    Widget continueButton = FlatButton(
      child: Text("Yes, Delete Space"),
      onPressed: () => Navigator.of(context).pop(true),
      textColor: Colors.white,
      color: Colors.red,
    );

    AlertDialog alert = AlertDialog(
      title: Text("Delete Space"),
      content: Text("Do you confirm deleting this space and tiles?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    return await showDialog(
        context: context, builder: (BuildContext context) => alert);
  }

  static open(BuildContext context, Space space) {
    Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) =>
            SpaceEditPage._(space),
        fullscreenDialog: true,
        transitionDuration: Duration(seconds: 0),
      ),
    );
  }
}

class _RenameForm extends StatelessWidget {
  final Space space;
  final Function onChanged;
  _RenameForm(this.space, this.onChanged);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: true,
      initialValue: space.name,
      onChanged: onChanged,
    );
  }
}
