import 'package:dac/model/printer.dart';
import 'package:dac/scaffolds/printer_edit_page.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/widgets/status/status_panel.dart';
import 'package:dac/widgets/tools/printer_info_panel.dart';
import 'package:dac/widgets/tools/temperatures_panel.dart';
import 'package:dac/widgets/tools/tools_panel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class PrinterPage extends ConsumerWidget {
  static const String routeName = '/printer';

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Printer printerArgument = ModalRoute.of(context).settings.arguments;
    Printer _printer = watch(printerProvider(printerArgument).state);

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context),
          ),
          centerTitle: true,
          title: Row(
            children: [
              Expanded(
                  child: Center(
                child:
                    Text(_printer?.name ?? _printer?.hostname ?? "no printer"),
              )),
              IconButton(
                  icon: Icon(Icons.settings),
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => PrinterEditPage(_printer),
                      fullscreenDialog: true)))
            ],
          )),
      body: homePanel(_printer),
    );
  }

  Widget homePanel(Printer _printer) {
    var items = [
      PrinterInfoPanel(printer: _printer),
      StatusPanel(printer: _printer),
      ToolsPanel(printer: _printer),
      TemperaturesPanel(printer: _printer),
    ];
    return ListView.builder(
      shrinkWrap: true,
      itemCount: items.length,
      itemBuilder: (_, index) => items[index],
    );
  }
}
