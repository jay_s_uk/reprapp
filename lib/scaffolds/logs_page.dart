import 'package:clipboard/clipboard.dart';
import 'package:dac/services/debuglog_service.dart';
import 'package:dac/widgets/app_drawer.dart';
import 'package:flutter/material.dart' hide ConnectionState;
import 'package:hooks_riverpod/all.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';

class LogsPage extends StatelessWidget {
  static const String routeName = '/logs';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          title: Text("Log"),
          actions: [
            PopupMenuButton<int>(
              onSelected: (int result) {
                switch (result) {
                  case 1:
                    break;
                  case 2:
                    _copyClipboard(context);
                    break;
                }
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
                // const PopupMenuItem<int>(
                //   value: 1,
                //   child: Text('Send log by email'),
                // ),
                const PopupMenuItem<int>(
                  value: 2,
                  child: Text('Copy top 50 to clipboard'),
                ),
              ],
            ),
          ],
        ),
        body: _DebugPanel());
  }

  void _copyClipboard(BuildContext context) async {
    var entries = await DebugLogService().load(limit: 50);
    String text = "";
    entries.forEach((element) {
      text += element.toString() + "\n";
    });

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;

    FlutterClipboard.copy('RepRapp: $version build: $buildNumber \n' + text)
        .then((value) => ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("Copied to clipboard"))));
  }
}

class _DebugPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.black87,
        padding: const EdgeInsets.all(10.0),
        child: _DebugLogList());
  }
}

class _DebugLogList extends ConsumerWidget {
  _DebugLogList();

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<DebugLog> _records = watch(debugLogProvider).records;

    return ListView.builder(
        shrinkWrap: true,
        itemCount: _records.length,
        itemBuilder: (context, index) {
          final item = _records[index];
          return Padding(
              padding: EdgeInsets.only(bottom: 5),
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Container(
                  width: 50,
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(right: 10),
                  child: Text(DateFormat.Hms().format(item.time),
                      style: TextStyle(
                          fontSize: 9,
                          color: item.level == DebugLevel.ERROR
                              ? Colors.deepOrange
                              : Colors.white60)),
                ),
                Expanded(
                  child: Text(item.log,
                      style: TextStyle(
                          fontSize: 8,
                          color: item.level == DebugLevel.ERROR
                              ? Colors.deepOrange
                              : Colors.white60)),
                )
              ]));
        });
  }
}
