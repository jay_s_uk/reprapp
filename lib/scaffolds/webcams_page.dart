import 'package:dac/model/webcam.dart';
import 'package:dac/scaffolds/webcam_edit_page.dart';
import 'package:dac/scaffolds/webcam_page.dart';
import 'package:dac/services/webcams_service.dart';
import 'package:dac/widgets/app_drawer.dart';
import 'package:dac/widgets/video_widget.dart';
import 'package:flutter/material.dart' hide ConnectionState;
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:hooks_riverpod/all.dart';

class WebcamsPage extends StatelessWidget {
  static const String routeName = '/webcams';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          title: Text("Webcams"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => WebcamEditPage())),
          child: Icon(Icons.add),
        ),
        body: _WebcamsListWidget());
  }
}

class _WebcamsListWidget extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    List<Webcam> records = watch(webcamsListProvider).records;

    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: records.length,
      itemBuilder: (context, index) {
        final item = records[index];
        return Dismissible(
          key: Key(item.id.toString()),
          confirmDismiss: (direction) => _showDeleteDialog(context, item),
          onDismissed: (direction) =>
              context.read(webcamsListProvider).remove(item.id).then((value) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(item.name + " removed")));
          }),
          background: Container(
            color: Colors.red,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Icon(Icons.delete, color: Colors.white),
                  Text('Remove', style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          ),
          child: _ListItem(record: item),
        );
      },
    );
  }

  Future<bool> _showDeleteDialog(BuildContext context, Webcam record) async {
    Widget cancelButton = FlatButton(
        child: Text("Cancel"),
        onPressed: () => Navigator.of(context).pop(false));

    Widget continueButton = FlatButton(
      child: Text("Remove"),
      onPressed: () {
        Navigator.of(context).pop(true);
      },
      textColor: Colors.white,
      color: Colors.red,
    );

    AlertDialog alert = AlertDialog(
      title: Text("Remove Webcam"),
      content: Text("Do you confirm removing this webcam?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    return await showDialog(
        context: context, builder: (BuildContext context) => alert);
  }
}

class _ListItem extends StatefulWidget {
  const _ListItem({
    this.record,
    this.groupValue,
  });

  final Webcam record;
  final bool groupValue;

  @override
  _WebcamVideoState createState() => _WebcamVideoState(record);
}

class _WebcamVideoState extends State<_ListItem> {
  _WebcamVideoState(this.webcam);

  final Webcam webcam;

  VlcPlayerController _videoViewController;
  String _url;

  @override
  void initState() {
    _videoViewController = new VlcPlayerController(onInit: () {
      _videoViewController.play();
    });
    _url = webcam.url;
    super.initState();
  }

  @override
  void dispose() {
    _videoViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _videoViewController.stop();
        Navigator.pushNamed(context, WebcamPage.routeName, arguments: webcam)
            .then((value) {
          _videoViewController.play();
        });
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  webcam?.name ?? "",
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        width: 200,
                        child: VideoWidget(_videoViewController, _url))
                  ]),
            ],
          ),
        ),
      ),
    );
  }
}
