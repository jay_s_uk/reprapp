import 'package:dac/model/printer.dart';
import 'package:dac/services/printers_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class PrinterEditPage extends StatefulWidget {
  final Printer printer;
  PrinterEditPage(this.printer);

  @override
  _FormState createState() => _FormState(printer);
}

class _FormState extends State<PrinterEditPage> {
  final Printer printer;
  final _formKey = GlobalKey<FormState>();
  final _addressController = TextEditingController();
  final _nameController = TextEditingController();

  _FormState(this.printer);

  @override
  void dispose() {
    _addressController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _addressController.text = printer.hostname;
    _nameController.text = printer.name;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        leading: new IconButton(
          icon: new Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(printer?.name ?? "Printer"),
        actions: [
          FlatButton(
            child: Text("Save"),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                PrintersService().save(printer.copyWith(
                    hostname: _addressController.text,
                    name: _nameController.text));
                ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text("Printer saved")));
                Navigator.of(context).pop();
              }
            },
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          child: FocusTraversalGroup(
            descendantsAreFocusable: true,
            child: Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.always,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextFormField(
                        keyboardType: TextInputType.url,
                        controller: _addressController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter the printer address';
                          } else {
                            if (value.startsWith("http")) {
                              return "Enter only the hostname. Don't start with http.\nExample: 192.168.1.10";
                            }
                            Uri _uri = Uri.parse("http://" + value);
                            if (!_uri.isAbsolute) {
                              return "Invalid address";
                            }
                            if (context.read(printersListProvider).records?.any(
                                    (element) =>
                                        element.id != printer.id &&
                                        element.hostname ==
                                            _addressController.text) ??
                                false) {
                              return "Printer already exists with this address";
                            }
                          }
                          return null;
                        },
                        autofocus: true,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.wifi),
                          labelText: 'Address',
                          // border: InputBorder(borderSide: BorderSide(width: 1)),
                        ),
                      )),
                  Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextFormField(
                        controller: _nameController,
                        decoration: InputDecoration(
                          icon: const Icon(Icons.book),
                          labelText: 'Name',
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
