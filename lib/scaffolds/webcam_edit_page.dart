import 'package:dac/model/webcam.dart';
import 'package:dac/services/webcams_service.dart';
import 'package:dac/widgets/video_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:hooks_riverpod/all.dart';

class WebcamEditPage extends StatefulWidget {
  static const String routeName = '/webcams/edit';

  WebcamEditPage();

  @override
  _WebcamVideoEditState createState() => _WebcamVideoEditState();
}

class _WebcamVideoEditState extends State<WebcamEditPage> {
  Webcam _webcam;
  VlcPlayerController _videoViewController;
  TextEditingController _inputControllerUrl;
  TextEditingController _inputControllerName;

  _WebcamVideoEditState();

  @override
  void initState() {
    _videoViewController = new VlcPlayerController(onInit: () {
      _videoViewController.play();
    });
    _inputControllerUrl = TextEditingController(text: _webcam?.url ?? "");
    _inputControllerName =
        TextEditingController(text: _webcam?.name ?? "Webcam #");
    super.initState();
  }

  @override
  void dispose() {
    _videoViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String _defaultName =
        "Webcam #" + (context.read(webcamsListProvider).count + 1).toString();
    _webcam = ModalRoute.of(context).settings.arguments ??
        Webcam(name: _defaultName, enabled: true);

    _inputControllerUrl.value = TextEditingValue(text: _webcam?.url ?? "");
    _inputControllerName.value = TextEditingValue(text: _webcam.name);

    final _formKey = GlobalKey<FormState>();

    var items = [
      Text(
        "Enter the full URL of the webcam.",
        style: Theme.of(context).textTheme.bodyText1,
      ),
      Text(
        "You can use any IP streaming from these protocols: http, https, rtsp.",
        style: Theme.of(context).textTheme.bodyText1,
      ),
      Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: TextFormField(
                    keyboardType: TextInputType.url,
                    minLines: 1,
                    maxLines: 10,
                    controller: _inputControllerUrl,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter the URL of the webcam';
                      } else {
                        Uri _uri = Uri.parse(value);
                      }
                      return null;
                    },
                    autofocus: true,
                    decoration: InputDecoration(
                      labelText: 'Webcam URL',
                      // border: InputBorder(borderSide: BorderSide(width: 1)),
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    controller: _inputControllerName,
                    decoration: InputDecoration(
                      labelText: 'Name',
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      RaisedButton(
                        onPressed: () {
                          _videoViewController
                              .setStreamUrl(_inputControllerUrl.text);
                          _videoViewController.play();
                        },
                        child: Text('Preview'),
                      ),
                      Expanded(child: Container()),
                      ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            context
                                .read(webcamsListProvider)
                                .save(_webcam.copyWith(
                                    name: _inputControllerName.text,
                                    url: _inputControllerUrl.text))
                                .then((value) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text("Webcam saved")));
                              Navigator.of(context).pop();
                            });
                          }
                        },
                        child: Text('Save'),
                      ),
                    ],
                  )),
              Center(child: VideoWidget(_videoViewController, _webcam?.url)),
            ],
          ))
    ];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop()),
        centerTitle: true,
        title: Text("Edit Webcam"),
      ),
      extendBodyBehindAppBar: false,
      body: ListView.builder(
          shrinkWrap: true,
          padding: EdgeInsets.all(20),
          itemCount: items.length,
          itemBuilder: (_, index) => Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: items[index])),
    );
  }
}
