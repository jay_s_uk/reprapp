import 'package:dac/model/settings.dart';
import 'package:dac/services/settings_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:spinner_input/spinner_input.dart';
import 'package:theme_provider/theme_provider.dart';

class SettingsPage extends ConsumerWidget {
  static const String routeName = '/settings';

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    AsyncValue<Settings> settings = watch(settingsProvider);

    return settings.when(
      data: (value) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          leading: new IconButton(
            icon: new Icon(Icons.done),
            onPressed: () {
              context.read(settingsEditProvider(value)).save();
              Navigator.pop(context);
            },
          ),
          title: Text("Settings"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _SettingsForm(value),
        ),
      ),
      loading: () => const CircularProgressIndicator(),
      error: (err, stack) => Text('Error: $err'),
    );
  }
}

class _SettingsForm extends ConsumerWidget {
  final Settings settings;
  _SettingsForm(this.settings);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Settings newSettings = watch(settingsEditProvider(settings).state);
    var controller = ThemeProvider.controllerOf(context);

    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      children: [
        _SettingsField(
          "Theme",
          RaisedButton(
            child: ThemeConsumer(child: Text(controller.theme.description)),
            elevation: 0,
            onPressed: () => showDialog(
              context: context,
              builder: (_) => ThemeConsumer(child: ThemeDialog()),
            ),
          ),
        ),
        _SettingsField(
          "Network update interval (seconds)",
          SpinnerInput(
            minValue: 1,
            maxValue: 60,
            step: 1,
            plusButton: SpinnerButtonStyle(
                elevation: 0,
                color: Theme.of(context).buttonColor,
                borderRadius: BorderRadius.circular(0)),
            minusButton: SpinnerButtonStyle(
                elevation: 0,
                color: Theme.of(context).buttonColor,
                borderRadius: BorderRadius.circular(0)),
            middleNumberWidth: 70,
            middleNumberStyle: TextStyle(fontSize: 21),
            // middleNumberBackground: Colors.yellowAccent.withOpacity(0.5),
            spinnerValue: newSettings.printerUpdateIntervalSeconds.toDouble(),
            onChange: (newValue) {
              context.read(settingsEditProvider(settings)).update(newSettings
                  .copyWith(printerUpdateIntervalSeconds: newValue.toInt()));
            },
          ),
        ),
        _SettingsField(
          "Send data to Watch",
          Switch(
            value: newSettings.watchEnabled,
            onChanged: (bool newValue) {
              context
                  .read(settingsEditProvider(settings))
                  .update(newSettings.copyWith(watchEnabled: newValue));
            },
          ),
        ),
      ],
    );
  }
}

class _SettingsField extends StatelessWidget {
  final Widget child;
  final String title;
  _SettingsField(this.title, this.child);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 20),
      child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(child: Text(title)),
            child,
          ]),
    );
  }
}
