import 'package:dac/model/webcam.dart';
import 'package:dac/scaffolds/webcam_edit_page.dart';
import 'package:dac/services/webcams_service.dart';
import 'package:dac/widgets/webcam_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/all.dart';

class WebcamPage extends StatelessWidget {
  static const String routeName = '/webcams/view';

  @override
  Widget build(BuildContext context) {
    Webcam _record = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop()),
        centerTitle: true,
        title: _Title(_record),
        actions: [_EditButton(_record)],
      ),
      body: WebcamViewWidget(_record),
    );
  }
}

class _Title extends ConsumerWidget {
  final Webcam record;

  _Title(this.record);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Webcam _record = watch(webcamProvider(record).state);
    return Text(_record?.name ?? _record?.id ?? "no webcam");
  }
}

class _EditButton extends ConsumerWidget {
  final Webcam record;

  _EditButton(this.record);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Webcam _record = watch(webcamProvider(record).state);
    return IconButton(
      icon: Icon(Icons.settings),
      onPressed: () {
        Navigator.of(context)
            .pushNamed(WebcamEditPage.routeName, arguments: _record);
      },
    );
  }
}
