import 'package:dac/model/space.dart';
import 'package:dac/services/spaces_service.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

class SpaceTileSelected {
  final Space space;
  final TileSelect tileSelect;
  final TileSpace tileSpace;

  const SpaceTileSelected({
    @required this.space,
    this.tileSelect,
    this.tileSpace,
  });
}

class TileSettingsPage extends StatelessWidget {
  static const String routeName = '/tile/settings';

  TileSettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SpaceTileSelected _spaceTileSelected =
        ModalRoute.of(context).settings.arguments;

    TileSettings _tileSettings = _spaceTileSelected.tileSpace?.tileSettings ??
        _spaceTileSelected.tileSelect?.tileSettings;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context, false)),
          centerTitle: true,
          title: Text(_tileSettings?.title ?? "Tile"),
          actions: [
            FlatButton(
              child: Text("Save"),
              onPressed: () {
                SpaceTileSettings settings =
                    context.read(spaceTileSettingsNotifierProvider).selected;
                if (_spaceTileSelected.tileSelect != null) {
                  context
                      .read(spaceTilesListProvider(_spaceTileSelected.space))
                      .add(_spaceTileSelected.tileSelect, settings)
                      .then((value) {
                    Navigator.pop(context, true);
                  });
                } else {
                  context
                      .read(spaceTilesListProvider(_spaceTileSelected.space))
                      .save(_spaceTileSelected.tileSpace, settings)
                      .then((value) {
                    Navigator.pop(context, true);
                  });
                }
              },
            ),
          ],
        ),
        body: Column(
          children: [
            _tileSettings?.settingsWidget ?? Container(),
          ],
        ));
  }
}
