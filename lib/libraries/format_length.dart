/// A method returns a human readable string representing a length
String formatLength(dynamic length, [int round = 2]) {
  /**
   * [length] can be passed as number or as string
   *
   * the optional parameter [round] specifies the number
   * of digits after comma/point (default is 2)
   */
  int divider = 1000;
  double _length;
  try {
    _length = double.parse(length.toString());
  } catch (e) {
    throw ArgumentError("Can not parse the length parameter: $e");
  }

  if (_length < divider) {
    return "$_length mm";
  }

  if (_length < divider * divider && _length % divider == 0) {
    return "${(_length / divider).toStringAsFixed(0)} cm";
  }

  if (_length < divider * divider) {
    return "${(_length / divider).toStringAsFixed(round)} m";
  }

  if (_length < divider * divider * divider && _length % divider == 0) {
    return "${(_length / (divider * divider)).toStringAsFixed(0)} km";
  }

  if (_length < divider * divider * divider) {
    return "${(_length / divider / divider).toStringAsFixed(round)} Mm";
  }

  if (_length < divider * divider * divider * divider &&
      _length % divider == 0) {
    return "${(_length / (divider * divider * divider)).toStringAsFixed(0)} Gm";
  }

  if (_length < divider * divider * divider * divider) {
    return "${(_length / divider / divider / divider).toStringAsFixed(round)} Gm";
  }

  if (_length < divider * divider * divider * divider * divider &&
      _length % divider == 0) {
    num r = _length / divider / divider / divider / divider;
    return "${r.toStringAsFixed(0)} Tm";
  }

  if (_length < divider * divider * divider * divider * divider) {
    num r = _length / divider / divider / divider / divider;
    return "${r.toStringAsFixed(round)} Tm";
  }

  if (_length < divider * divider * divider * divider * divider * divider &&
      _length % divider == 0) {
    num r = _length / divider / divider / divider / divider / divider;
    return "${r.toStringAsFixed(0)} PB";
  } else {
    num r = _length / divider / divider / divider / divider / divider;
    return "${r.toStringAsFixed(round)} PB";
  }
}
