import 'package:intl/intl.dart';

/// A method returns a human readable string representing a number
String formatThousands(dynamic number, {String unit, int decimals}) {
  /**
   * [number] can be passed as number or as string
   *
   */
  double _number;
  try {
    _number = double.parse(number.toString());
  } catch (e) {
    throw ArgumentError("Can not parse the number parameter: $e");
  }

  var formatter = NumberFormat('#,##0' + (decimals > 0 ? '.0' : ''));
  return formatter.format(_number) + (unit != null ? " " + unit : "");
}
