import 'dart:async';
import 'dart:developer';

import 'package:dac/model/printer.dart';
import 'package:dac/model/rr_fileinfo.dart';
import 'package:dac/model/status_type1.dart';
import 'package:dac/model/status_type2.dart';
import 'package:dac/model/status_type3.dart';
import 'package:dio/dio.dart';

class HttpService {
  Dio dio = new Dio(BaseOptions(
    connectTimeout: 5000,
    receiveTimeout: 3000,
  ));

  HttpService() {
    // PrinterMock().interceptMocks(dio);
  }

  Uri _getUri(Printer printer, String path, Map<String, String> parameters) {
    Uri uri = Uri.http(printer.hostname, path, parameters);
    return uri;
  }

  Future<StatusType1> fetchStatusType1(Printer printer) async {
    Uri uri = _getUri(printer, "rr_status", {"type": "1"});
    log('loading from: ' + uri.toString());
    try {
      Response response = await dio.get(uri.toString());

      log('Response code: ' + response.statusCode.toString());

      if (response.statusCode == 200) {
        StatusType1 status = statusType1FromJsonMap(response.data);
        return status;
      } else {
        throw Exception('Failed to load status 1');
      }
    } on DioError catch (e) {
      throw NetworkException(e, 'Failed to load status 1');
    }
  }

  Future<StatusType2> fetchStatusType2(Printer printer) async {
    Uri uri = _getUri(printer, "rr_status", {"type": "2"});
    log('loading from: ' + uri.toString());
    final response = await dio.get(uri.toString());

    log('Response code: ' + response.statusCode.toString());

    if (response.statusCode == 200) {
      StatusType2 status = statusType2FromJsonMap(response.data);
      return status;
    } else {
      throw Exception('Failed to load status 2');
    }
  }

  Future<StatusType3> fetchStatusType3(Printer printer) async {
    Uri uri = _getUri(printer, "rr_status", {"type": "3"});
    log('loading from: ' + uri.toString());
    try {
      Response response = await dio.get(uri.toString());

      log('Response code: ' + response.statusCode.toString());

      if (response.statusCode == 200) {
        StatusType3 status = statusType3FromJsonMap(response.data);
        return status;
      } else {
        throw Exception('Failed to load status 3');
      }
    } on DioError catch (e) {
      throw NetworkException(e, 'Failed to load status 3');
    }
  }

  Future<FileInfo> fetchFileInfo(Printer printer) async {
    Uri uri = _getUri(printer, "rr_fileinfo", {});
    log('loading from: ' + uri.toString());
    try {
      Response response = await dio.get(uri.toString());

      log('Response code: ' + response.statusCode.toString());

      if (response.statusCode == 200) {
        FileInfo result = fileInfoFromJsonMap(response.data);
        return result;
      } else {
        throw Exception('Failed to load FileInfo');
      }
    } on DioError catch (e) {
      throw NetworkException(e, 'Failed to load FileInfo');
    }
  }
}

class NetworkException {
  final DioError error;
  final String message;

  NetworkException(this.error, this.message);

  @override
  String toString() => message + " " + error.toString();
}
