import 'dart:async';
import 'dart:developer';

import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/status_type2.dart' as status2;
import 'package:dac/services/http_service.dart';
import 'package:dac/services/printers_service.dart';
import 'package:stash/stash_api.dart';
import 'package:stash/stash_memory.dart';

import 'debuglog_service.dart';

class PrinterInfoError {
  final Printer printer;
  final Exception error;
  PrinterInfoError(this.printer, this.error);
}

class PrintersInfoService {
  static final PrintersInfoService _instance = PrintersInfoService._();

  factory PrintersInfoService() => _instance;

  final _controller = StreamController<PrinterInfo>.broadcast();

  Stream<PrinterInfo> get stream => _controller.stream;

  final Cache records = newMemoryCache(
    expiryPolicy: CreatedExpiryPolicy(Duration(minutes: 5)),
    cacheLoader: (printerId) async {
      Printer printer = await PrintersService().read(int.tryParse(printerId));
      if (printer.enabled) {
        try {
          PrinterInfo printerInfo = await _getPrinterInfo(printer);
          log("Loaded new PrinterInfo: " + printerInfo.toString());
          PrintersInfoService()._controller.sink.add(printerInfo);
          return printerInfo;
        } catch (e) {
          debug.e("Error loading info from: " +
              printer.toString() +
              "\n" +
              e.toString());
          PrintersInfoService()
              ._controller
              .sink
              .addError(PrinterInfoError(printer, Exception(e.toString())));
        }
      } else {
        return PrinterInfo(printer);
      }
    },
  );

  PrintersInfoService._() {
    PrintersService().stream.listen((printers) {
      printers.where((printer) => printer.enabled).forEach((printer) {
        _getPrinterInfo(printer).then((printerInfo) {
          records.put(printer.id.toString(), printerInfo);
          _controller.sink.add(printerInfo);
        }, onError: (e) {
          _controller.sink
              .addError(PrinterInfoError(printer, Exception(e.toString())));
        });
      });
    });
    log("Loaded PrintersInfoService");
  }

  void close() {
    _controller.close();
  }
}

Future<PrinterInfo> _getPrinterInfo(Printer printer) async {
  status2.StatusType2 status = await HttpService().fetchStatusType2(printer);
  return _processFromStatusType2(printer, status);
}

PrinterInfo _processFromStatusType2(
    Printer printer, status2.StatusType2 status) {
  return PrinterInfo(
    printer,
    controllableFans: status.controllableFans,
    temperatureLimit: status.tempLimit,
    endstops: status.endstops,
    firmwareName: status.firmwareName,
    firmwareVersion: status.firmwareVersion,
    geometry: status.geometry,
    axes: status.axes,
    totalAxes: status.totalAxes,
    axisNames: status.axisNames,
    volumes: status.volumes,
    mountedVolumes: status.mountedVolumes,
    mode: status.mode,
    name: status.name,
    tools: _toolsInfo(status.tools),
  );
}

List<ToolInfo> _toolsInfo(List<status2.Tool> tools) {
  return tools
      .map((t) => ToolInfo(
            number: t.number,
            name: t.name,
            heaters: t.heaters,
            drives: t.drives,
            fans: t.fans,
            filament: t.filament,
            offsets: t.offsets,
          ))
      .toList();
}
