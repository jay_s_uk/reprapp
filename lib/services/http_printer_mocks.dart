import 'package:dio/dio.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

class PrinterMock {
  final dioInterceptor = DioInterceptor();

  PrinterMock() {
    MockPrinterType2 mock1 = MockPrinterType2();
    MockPrinterType2 mock2 = MockPrinterType2();

    dioInterceptor
        .onGet(mock1.path)
        .reply(200, mock1.response)
        .onGet(mock2.path)
        .reply(200, mock2.response);
  }

  void interceptMocks(Dio dio) {
    dio.interceptors.add(dioInterceptor);
  }
}

abstract class _MockRequest {
  final String path;
  _MockRequest(this.path);
  dynamic get response;
}

class MockPrinterType1 extends _MockRequest {
  MockPrinterType1() : super("/rr_status?type=1");
  @override
  get response => {'status': 'I', 'time': 21304.0};
}

class MockPrinterType2 extends _MockRequest {
  MockPrinterType2() : super("/rr_status?type=2");
  @override
  get response => {
        "status": "I",
        "coords": {
          "axesHomed": [0, 0, 0],
          "wpl": 1,
          "xyz": [0.000, 0.000, 0.000],
          "machine": [0.000, 0.000, 0.000],
          "extr": [0.0]
        },
        "speeds": {"requested": 0.0, "top": 0.0},
        "currentTool": -1,
        "params": {
          "atxPower": 1,
          "fanPercent": [0, 100, 30, 0, 0, 0, 0, 0, 0],
          "fanNames": ["", "", "", "", "", "", "", "", ""],
          "speedFactor": 100.0,
          "extrFactors": [100.0],
          "babystep": 0.000
        },
        "seq": 104,
        "sensors": {"probeValue": 0, "fanRPM": 0},
        "temps": {
          "bed": {
            "current": 13.2,
            "active": 0.0,
            "standby": 0.0,
            "state": 0,
            "heater": 0
          },
          "current": [
            13.2,
            12.7,
            2000.0,
            2000.0,
            2000.0,
            2000.0,
            2000.0,
            2000.0
          ],
          "state": [0, 0, 0, 0, 0, 0, 0, 0],
          "names": ["", "", "", "", "", "", "", ""],
          "tools": {
            "active": [
              [0.0]
            ],
            "standby": [
              [0.0]
            ]
          },
          "extra": [
            {"name": "*MCU", "temp": 22.1}
          ]
        },
        "time": 21324.0,
        "coldExtrudeTemp": 160.0,
        "coldRetractTemp": 90.0,
        "compensation": "None",
        "controllableFans": 1,
        "tempLimit": 290.0,
        "endstops": 3096,
        "firmwareName": "RepRapFirmware for Duet 2 WiFi/Ethernet",
        "geometry": "coreXY",
        "axes": 3,
        "totalAxes": 3,
        "axisNames": "XYZ",
        "volumes": 2,
        "mountedVolumes": 1,
        "name": "VIVEDINO",
        "probe": {"threshold": 50, "height": 1.10, "type": 5},
        "tools": [
          {
            "number": 0,
            "heaters": [1],
            "drives": [0],
            "axisMap": [
              [0],
              [1]
            ],
            "fans": 1,
            "filament": "PETG",
            "offsets": [0.00, 0.00, 0.00]
          }
        ],
        "mcutemp": {"min": 21.7, "cur": 22.1, "max": 24.4},
        "vin": {"min": 23.9, "cur": 24.1, "max": 24.4}
      };
}
