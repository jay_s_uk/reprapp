import 'package:dac/model/webcam.dart';
import 'package:dac/repositories/webcams_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:sorted_list/sorted_list.dart';

import 'base_service.dart';

/// Providers

final webcamsListProvider =
    ChangeNotifierProvider((ref) => _WebcamsListNotifier());

final webcamProvider =
    StateNotifierProvider.family((ref, webcam) => _WebcamNotifier(webcam));

final webcamProviderFromId =
    FutureProvider.family<Webcam, int>((ref, id) async {
  Webcam record = id != null ? await WebcamsService().read(id) : Webcam();
  return record;
});

/// Notifiers

class _WebcamNotifier extends StateNotifier<Webcam> {
  _WebcamNotifier(Webcam record) : super(record) {
    WebcamsService().stream.listen((records) {
      records.where((element) => element.id == record.id).forEach((element) {
        state = element;
      });
    });
  }
}

class _WebcamsListNotifier extends ChangeNotifier {
  SortedList<Webcam> records =
      SortedList<Webcam>((a, b) => a.name.compareTo(b.name));

  _WebcamsListNotifier() {
    _setAll(WebcamsService().records);
    WebcamsService().stream.listen((value) => _setAll(value));
  }

  _setAll(List<Webcam> values) {
    records.clear();
    records.addAll(values);
    notifyListeners();
  }

  Future<Webcam> save(Webcam record) {
    return WebcamsService().save(record);
  }

  Future<void> remove(int id) {
    return WebcamsService().remove(id);
  }

  int get count => records.length;
}

/// Services

class WebcamsService extends BaseService<Webcam, WebcamsRepository> {
  static final WebcamsService _instance = WebcamsService._();

  WebcamsService._() : super(WebcamsRepository.db);

  factory WebcamsService() {
    return _instance;
  }
}
