import 'dart:developer';

import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/services/printers_data_service.dart';
import 'package:dac/services/printers_info_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:hooks_riverpod/all.dart';

/// Providers

final printersDataProvider =
    StateNotifierProvider((ref) => _PrintersDataNotifier());

final printerDataNotifierProvider = StateNotifierProvider.family(
    (ref, printer) => _PrinterDataNotifier(ref, printer));

final printerDataProvider = Provider.family((ref, printer) {
  PrinterData printerData =
      ref.watch(printerDataNotifierProvider(printer).state);
  return printerData;
});

final printerMoveProvider = Provider.family<Move, Printer>((ref, printer) {
  return ref.watch(printerDataProvider(printer))?.move;
});

final printerMachineStatusProvider = Provider.family((ref, printer) {
  return ref.watch(printerDataProvider(printer))?.status;
});

final printerToolsProvider =
    Provider.family<List<Tool>, Printer>((ref, printer) {
  PrinterData printerData = ref.watch(printerDataProvider(printer));
  return printerData?.tools;
});

final printerBedProvider = StateProvider.family<Bed, Printer>((ref, printer) {
  PrinterData printerData = ref.watch(printerDataProvider(printer));
  return printerData?.bed;
});

final printerToolProvider =
    StateProvider.family<Tool, PrinterTool>((ref, printerTool) {
  PrinterData printerData = ref.watch(printerDataProvider(printerTool.printer));
  return printerTool.toolNumber != null
      ? printerData?.tools
          ?.firstWhere((t) => t.toolNumber == printerTool.toolNumber)
      : null;
});

final printerInfoNotifierProvider = ChangeNotifierProvider.family(
    (ref, printer) => _PrinterInfoNotifier(ref, printer));

final printerInfoProvider = StateProvider.family((ref, printer) {
  PrinterInfo printerInfo =
      ref.watch(printerInfoNotifierProvider(printer)).printerInfo;
  return printerInfo;
});

final printerToolInfoProvider =
    StateProvider.family<ToolInfo, PrinterTool>((ref, printerTool) {
  PrinterInfo printerInfo =
      ref.watch(printerInfoProvider(printerTool.printer)).state;
  ToolInfo toolInfo = printerInfo?.tools
      ?.firstWhere((element) => element.number == printerTool.toolNumber);
  return toolInfo;
});

final printerJobStatusProvider = StateNotifierProvider.family(
    (ref, printer) => _PrinterJobStatusNotifier(ref, printer));

final printerJobFileNotifierProvider = ChangeNotifierProvider.family(
    (ref, printer) => _PrinterJobNotifier(ref, printer));

final printerJobFileProvider = Provider.family((ref, printer) {
  if (printer != null) {
    JobFile jobFile =
        ref.watch(printerJobFileNotifierProvider(printer)).jobFile;
    return jobFile;
  } else {
    return null;
  }
});

/// Models
class PrinterTool extends Equatable {
  final Printer printer;
  final int toolNumber;

  PrinterTool(this.printer, this.toolNumber);

  @override
  List<Object> get props => [printer, toolNumber];
}

class PrinterToolHeater extends PrinterTool {
  final int heaterNumber;

  PrinterToolHeater(Printer printer, int toolNumber, this.heaterNumber)
      : super(printer, toolNumber);

  @override
  List<Object> get props => List.of(super.props)..addAll([heaterNumber]);
}

/// Notifiers

class _PrinterInfoNotifier extends ChangeNotifier {
  final Printer printer;
  PrinterInfo printerInfo;
  PrinterInfoError printerInfoError;

  _PrinterInfoNotifier(ProviderReference providerReference, this.printer) {
    if (printer != null) {
      PrintersInfoService().stream.listen((printerInfo) {
        if (printerInfo.printer == printer) {
          _setValue(printerInfo);
        }
      }, onError: (e) => _setError(e));
      PrintersInfoService().records.get(printer.id.toString()).then(
          (printerInfo) => _setValue(printerInfo),
          onError: (e) => _setError(e));
    }
  }

  _setValue(value) {
    printerInfo = value;
    printerInfoError = null;
    notifyListeners();
  }

  _setError(e) {
    printerInfoError = PrinterInfoError(printer, Exception(e));
    notifyListeners();
  }
}

class _PrintersDataNotifier extends StateNotifier<PrinterData> {
  _PrintersDataNotifier() : super(null) {
    PrintersDataService()
        .stream
        .handleError((_) => log("Error"))
        .listen((printerData) => state = printerData);
  }
}

class _PrinterDataNotifier extends StateNotifier<PrinterData> {
  final Printer printer;
  final ProviderReference providerReference;

  _PrinterDataNotifier(this.providerReference, this.printer) : super(null) {
    PrintersDataService()
        .stream
        .where((p) => p.printer == printer)
        .handleError((_) => log("Error"))
        .listen((printerData) => state = printerData);
  }
}

class _PrinterJobNotifier extends ChangeNotifier {
  final Printer printer;
  final ProviderReference providerReference;

  bool isPrinting = false;

  JobFile jobFile;

  _PrinterJobNotifier(this.providerReference, this.printer) {
    MachineStatus status =
        providerReference.watch(printerMachineStatusProvider(printer));
    bool newStatus = PrintersDataService.isPrinting(status);
    if (jobFile == null || (newStatus != isPrinting && newStatus)) {
      isPrinting = newStatus;
      PrintersDataService().getPrinterFileInfo(printer).then((value) {
        jobFile = value;
        notifyListeners();
      });
    }
  }
}

class _PrinterJobStatusNotifier extends StateNotifier<JobStatus> {
  final Printer printer;
  final ProviderReference providerReference;

  _PrinterJobStatusNotifier(this.providerReference, this.printer)
      : super(null) {
    PrinterData printerData =
        providerReference.watch(printerDataProvider(printer));
    if (printerData?.jobStatus != null && state != printerData?.jobStatus) {
      state = printerData.jobStatus;
    }
  }
}
