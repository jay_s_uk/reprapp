import 'dart:async';
import 'dart:developer';

import 'package:dac/model/settings.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final settingsNotifierProvider =
    StateNotifierProvider((ref) => _SettingsNotifier());

final settingsProvider = FutureProvider<Settings>((ref) {
  return SettingsService().getAll();
});

class _SettingsNotifier extends StateNotifier<Settings> {
  _SettingsNotifier() : super(Settings.empty()) {
    SettingsService()
        .stream
        .handleError((_) => log("Error"))
        .listen((printerData) => state = printerData);
    SettingsService().getAll().then((value) => state = value);
  }
}

final settingsEditProvider = StateNotifierProvider.family(
    (ref, settings) => _SettingsEditNotifier(settings));

class _SettingsEditNotifier extends StateNotifier<Settings> {
  _SettingsEditNotifier(Settings settings) : super(settings);

  void update(Settings settings) {
    state = state.copyWithSettings(settings);
  }

  void save() {
    SettingsService().save(state);
  }
}

class SettingsService {
  static final SettingsService _instance = SettingsService._();

  factory SettingsService() {
    return _instance;
  }

  final _controller = StreamController<Settings>.broadcast();

  Stream<Settings> get stream => _controller.stream;

  Future<SharedPreferences> _prefs;

  SettingsService._() {
    _prefs = SharedPreferences.getInstance();
    getAll().then((value) => _controller.sink.add(value));
  }

  Future<Settings> getAll() async {
    SharedPreferences prefs = await _prefs;
    Settings settings = Settings.fromSharedPreferences(prefs);
    return settings;
  }

  void save(Settings settings) async {
    SharedPreferences prefs = await _prefs;
    settings.saveSharedPreferences(prefs);
    _controller.sink.add(settings);
  }

  void close() {
    _controller.close();
  }

  void update({int lastSpace}) {
    getAll().then((value) => save(value.copyWith(lastSpace: lastSpace)));
  }
}
