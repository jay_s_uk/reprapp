import 'package:dac/model/printer.dart';
import 'package:dac/repositories/printers_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:sorted_list/sorted_list.dart';

import 'base_service.dart';

/// Providers

final printersListProvider =
    ChangeNotifierProvider((ref) => _PrintersListNotifier());

final printerProvider =
    StateNotifierProvider.family((ref, printer) => _PrinterNotifier(printer));

final printerProviderFromId = Provider.family((ref, id) {
  List<Printer> records = ref.watch(printersListProvider).records;
  Printer printer = records.firstWhere((p) => p.id == id, orElse: () => null);
  return printer;
});

/// Services

class PrintersService extends BaseService<Printer, PrintersRepository> {
  static final PrintersService _instance = PrintersService._();

  PrintersService._() : super(PrintersRepository.db);

  factory PrintersService() {
    return _instance;
  }

  void addNew({String hostname, String name}) {
    Printer printer =
        Printer(hostname: hostname.toLowerCase(), name: name, enabled: true);
    super.save(printer);
  }

  Future<List<Printer>> filter(String filter) async {
    return repository.getAllWithFilter(filter: filter);
  }
}

/// Notifiers

class _PrinterNotifier extends StateNotifier<Printer> {
  _PrinterNotifier(Printer printer) : super(printer) {
    PrintersService().stream.listen((printers) {
      printers.where((element) => element == printer).forEach((element) {
        state = element;
      });
    });
  }
}

class _PrintersListNotifier extends ChangeNotifier {
  SortedList<Printer> records =
      SortedList<Printer>((a, b) => a.name.compareTo(b.name));

  _PrintersListNotifier() {
    _setAll(PrintersService().records);
    PrintersService().stream.listen((values) => _setAll(values));
  }
  _setAll(List<Printer> values) {
    records.clear();
    records.addAll(values);
    notifyListeners();
  }

  int count() => records.length;

  bool exists({String hostname}) {
    return records.any(
        (element) => element.hostname.toLowerCase() == hostname.toLowerCase());
  }
}
