import 'dart:developer';

import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/services/printer_data_providers.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/settings_service.dart';
import 'package:dac/services/watch/watch_models.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/all.dart';

import '../debuglog_service.dart';
import '../printers_data_service.dart';

enum DataType {
  PRINTERS_LIST,
  PRINTER,
  PRINTER_DATA,
  JOB_STATUS,
}

const _watchMessageTypes = {
  DataType.PRINTERS_LIST: "printersList",
  DataType.PRINTER: "printer",
  DataType.PRINTER_DATA: "printerData",
  DataType.JOB_STATUS: "printerJobStatus",
};

final _printerJobStatusProvider =
    Provider.family<String, PrinterData>((ref, printerData) {
  JobFile jobFile = ref.watch(printerJobFileProvider(printerData.printer));

  String data = WatchPrinterJobStatus.fromJobStatus(
    printerData.printer.id.toString(),
    jobFile,
    printerData.jobStatus,
  ).toJson();

  return data;
});

final _printerDataProvider =
    Provider.family<String, PrinterData>((ref, printerData) {
  String data = WatchPrinterData.fromPrinterData(printerData).toJson();
  return data;
});

class WatchChannels {
  static const channel = const MethodChannel('dac1010Channel');

  static final WatchChannels _instance = WatchChannels._();

  factory WatchChannels() {
    return _instance;
  }

  final container = ProviderContainer();
  Map<Printer, Map<DataType, dynamic>> _printers = {};

  bool _watchEnabled = false;

  _setWatchEnabled(bool value) {
    _watchEnabled = value ?? false;
    debug.i("Watch data enabled: $_watchEnabled");

    if (_watchEnabled) {
      PrintersService()
          .getAll()
          .then((printers) => _sendPrintersList(printers));
    }
  }

  WatchChannels._() {
    PrintersService().stream.listen((printers) {
      printers.forEach((printer) {
        _printers.putIfAbsent(printer, () => {});
      });
      if (_watchEnabled) {
        _sendPrintersList(printers);
      }
    });

    channel.setMethodCallHandler(_receivedMessage);

    container
        .read(settingsNotifierProvider)
        .addListener((value) => _setWatchEnabled(value.watchEnabled));

    container
        .read(settingsProvider)
        .whenData((value) => _setWatchEnabled(value.watchEnabled));

    PrintersDataService()
        .stream
        .handleError((_) => log("Error"))
        .listen((printerData) {
      if (_watchEnabled) {
        if (printerData?.printer != null &&
            _printers.containsKey(printerData.printer)) {
          String printerDataJson =
              container.read<String>(_printerDataProvider(printerData));
          _sendPrinterData(printerData.printer, printerDataJson);

          String jobStatusJson =
              container.read<String>(_printerJobStatusProvider(printerData));
          _sendPrinterJobStatus(printerData.printer, jobStatusJson);
        }
      }
    });
  }

  Future<String> _receivedMessage(MethodCall call) async {
    if (call.method == _watchMessageTypes[DataType.PRINTERS_LIST]) {
      return watchPrintersToJson(
          _printers.keys.map((e) => WatchPrinter.fromPrinter(e)));
    }
    return null;
  }

  void _sendPrintersList(List<Printer> printers) {
    String data = watchPrintersToJson(
        printers.map((e) => WatchPrinter.fromPrinter(e)).toList());
    print("Sending list of printers: " + data);
    channel.invokeMethod(_watchMessageTypes[DataType.PRINTERS_LIST], data);
  }

  void _sendPrinterData(Printer printer, String data) {
    if (_hasChanged(printer, DataType.PRINTER_DATA, data)) {
      print("Sending message: " + data);
      channel.invokeMethod(_watchMessageTypes[DataType.PRINTER_DATA], data);
    }
  }

  void _sendPrinterJobStatus(Printer printer, String data) {
    if (_hasChanged(printer, DataType.JOB_STATUS, data)) {
      print("Sending message: " + data);
      channel.invokeMethod(_watchMessageTypes[DataType.JOB_STATUS], data);
    }
  }

  bool _hasChanged(Printer printer, DataType dataType, dynamic data) {
    if (_printers[printer].containsKey(dataType)) {
      dynamic existingData = _printers[printer][dataType];
      if (existingData != data) {
        _printers[printer][dataType] = data;
        return true;
      }
    } else {
      _printers[printer][dataType] = data;
      return true;
    }
    return false;
  }
}
