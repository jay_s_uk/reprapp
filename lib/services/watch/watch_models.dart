import 'dart:convert';

import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/printer_job.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';

String watchPrintersToJson(List<WatchPrinter> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class WatchPrinter {
  final String id;
  final String name;
  final bool enabled;
  final String hostname;

  WatchPrinter({
    @required this.id,
    @required this.name,
    @required this.enabled,
    @required this.hostname,
  });

  factory WatchPrinter.fromPrinter(Printer printer) {
    return WatchPrinter(
      id: printer.id.toString(),
      name: printer.name,
      enabled: printer.enabled,
      hostname: printer.hostname,
    );
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'id': this.id,
      'name': this.name,
      'enabled': this.enabled,
      'hostname': this.hostname,
    } as Map<String, dynamic>;
  }

  String toJson() => json.encode(this.toMap());
}

class WatchPrinterJobStatus {
  final String printerId;
  final double progress;
  final int currentLayer;
  final double currentHeight;
  final String filename;
  final int timeLeftSeconds;

  const WatchPrinterJobStatus({
    @required this.printerId,
    @required this.progress,
    @required this.currentLayer,
    @required this.currentHeight,
    @required this.filename,
    @required this.timeLeftSeconds,
  });

  factory WatchPrinterJobStatus.fromJobStatus(
      String printerId, JobFile jobFile, JobStatus jobStatus) {
    return WatchPrinterJobStatus(
      printerId: printerId,
      progress: jobStatus?.progress?.printedPercent,
      currentLayer: jobStatus?.currentLayer?.layer,
      currentHeight: jobStatus?.currentLayer?.height,
      filename: jobFile?.fileName,
      timeLeftSeconds: jobStatus?.jobLeft?.durationFile?.inSeconds,
    );
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'printerId': this.printerId,
      'progress': this.progress,
      'currentLayer': this.currentLayer,
      'currentHeight': this.currentHeight,
      'filename': this.filename,
      'timeLeftSeconds': this.timeLeftSeconds,
    } as Map<String, dynamic>;
  }

  String toJson() => json.encode(this.toMap());
}

class WatchPrinterData {
  final String printerId;
  final MachineStatus status;
  final List<WatchTool> tools;
  final WatchBed bed;
  final List<Fan> fans;

  const WatchPrinterData({
    @required this.printerId,
    @required this.status,
    @required this.tools,
    @required this.bed,
    @required this.fans,
  });

  factory WatchPrinterData.fromPrinterData(PrinterData data) {
    return WatchPrinterData(
        printerId: data.printer.id.toString(),
        status: data.status,
        tools: List<WatchTool>.of(data.tools.map((e) => WatchTool.fromTool(e))),
        bed: WatchBed.fromBed(data.bed),
        fans: data.fans);
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'printerId': this.printerId,
      'status': describeEnum(this.status),
      'tools': this.tools != null
          ? List<dynamic>.from(this.tools.map((x) => x.toMap()))
          : [],
      'bed': this.bed.toMap(),
      'fans': this.fans != null
          ? List<dynamic>.from(this.fans.map((x) => x.toMap()))
          : [],
    } as Map<String, dynamic>;
  }

  String toJson() => json.encode(this.toMap());
}

class WatchTool {
  final double current;
  final double set;
  final int toolNumber;

  const WatchTool({
    @required this.current,
    @required this.set,
    @required this.toolNumber,
  });

  factory WatchTool.fromTool(Tool tool) {
    double setValue;
    Heater heater = tool.heaters.first;
    TemperatureSet temperatureSet = tool.temperaturesSets
        .firstWhere((element) => element.heaterNumber == heater.number);
    switch (heater.state) {
      case HeaterState.ACTIVE:
        setValue = temperatureSet.active;
        break;
      case HeaterState.STANDBY:
        setValue = temperatureSet.standby;
        break;
    }
    return WatchTool(
        current: heater.current, set: setValue, toolNumber: tool.toolNumber);
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'current': this.current,
      'set': this.set,
      'toolNumber': this.toolNumber,
    } as Map<String, dynamic>;
  }
}

class WatchBed {
  final double current;
  final double set;

  const WatchBed({
    @required this.current,
    @required this.set,
  });

  factory WatchBed.fromBed(Bed bed) {
    double setValue;
    Heater heater = bed.heater;
    TemperatureSet temperatureSet = bed.temperatureSet;

    switch (heater.state) {
      case HeaterState.ACTIVE:
        setValue = temperatureSet.active;
        break;
      case HeaterState.STANDBY:
        setValue = temperatureSet.standby;
        break;
    }
    return WatchBed(current: heater.current, set: setValue);
  }

  Map<String, dynamic> toMap() {
    // ignore: unnecessary_cast
    return {
      'current': this.current,
      'set': this.set,
    } as Map<String, dynamic>;
  }
}
