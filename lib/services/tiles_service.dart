import 'package:dac/model/space.dart';
import 'package:dac/widgets/tiles/job/tile_job_filament.dart';
import 'package:dac/widgets/tiles/job/tile_job_filaments.dart';
import 'package:dac/widgets/tiles/job/tile_job_file.dart';
import 'package:dac/widgets/tiles/job/tile_job_layers.dart';
import 'package:dac/widgets/tiles/job/tile_job_progress.dart';
import 'package:dac/widgets/tiles/tile_bed.dart';
import 'package:dac/widgets/tiles/tile_tool.dart';
import 'package:dac/widgets/tiles/tile_view_chart.dart';
import 'package:dac/widgets/tiles/tile_view_coordinates.dart';
import 'package:dac/widgets/tiles/tile_view_status.dart';
import 'package:dac/widgets/tiles/tile_webcam.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:equatable/equatable.dart';
import 'package:hooks_riverpod/all.dart';

/// Providers

final tileSelectedProvider =
    StateNotifierProvider.autoDispose((ref) => _TileSelectedNotifier());

/// Notifiers

class _TileSelectedNotifier extends StateNotifier<SelectedTileEvent> {
  _TileSelectedNotifier() : super(SelectedTileEvent());

  void select(TileSelect tile) {
    state = state.copyWith(previous: state.selected, selected: tile);
  }

  void unselect() {
    state = state.copyWith(previous: state.selected, selected: null);
  }
}

/// Models

class SelectedTileEvent extends Equatable {
  final TileSelect previous;
  final TileSelect selected;

  const SelectedTileEvent({this.previous, this.selected});

  @override
  List<Object> get props => [previous, selected];

  bool get isSelected => selected != null;

  SelectedTileEvent copyWith({
    TileSelect previous,
    TileSelect selected,
  }) {
    return new SelectedTileEvent(
      previous: previous,
      selected: selected,
    );
  }
}

/// Factories

TileSelect tileFactorySelect(TileKey tileKey, TileSize tileSize) {
  switch (tileKey) {
    case TileKey.PRINTER_STATUS:
      return TileStatusSelect(tileSize);
    case TileKey.PRINTER_COORDINATES:
      return TileCoordinatesSelect(tileSize);
    case TileKey.PRINTER_TOOL:
      return TileToolSelect(tileSize);
    case TileKey.PRINTER_BED:
      return TileBedSelect(tileSize);
    case TileKey.PRINTER_CHART:
      return TileChartSelect(tileSize);
    case TileKey.PRINTER_JOB_PROGRESS:
      return TileJobProgressSelect(tileSize);
    case TileKey.PRINTER_JOB_LAYERS:
      return TileJobLayersSelect(tileSize);
    case TileKey.PRINTER_JOB_FILE:
      return TileJobFileSelect(tileSize);
    case TileKey.PRINTER_JOB_FILAMENT:
      return TileJobFilamentSelect(tileSize);
    case TileKey.PRINTER_JOB_FILAMENTS:
      return TileJobFilamentsSelect(tileSize);
    case TileKey.CAM:
      return TileWebcamSelect(tileSize);
    case TileKey.PRINTER_JOB_TIME:
      // TODO: Handle this case.
      break;
    case TileKey.WEB:
      // TODO: Handle this case.
      break;
  }
  throw Exception("TileSelect without factory: " + tileKey.toString());
}

TileSpace tileFactorySpace({Space space, SpaceTile spaceTile, bool isEdit}) {
  switch (spaceTile.tileKey) {
    case TileKey.PRINTER_STATUS:
      return TileStatusSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_COORDINATES:
      return TileCoordinatesSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_TOOL:
      return TileToolSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_BED:
      return TileBedSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_CHART:
      return TileChartSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_JOB_PROGRESS:
      return TileJobProgressSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_JOB_LAYERS:
      return TileJobLayersSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_JOB_FILE:
      return TileJobFileSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_JOB_FILAMENT:
      return TileJobFilamentSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_JOB_FILAMENTS:
      return TileJobFilamentsSpace(space, spaceTile, isEdit);
    case TileKey.CAM:
      return TileWebcamSpace(space, spaceTile, isEdit);
    case TileKey.PRINTER_JOB_TIME:
      // TODO: Handle this case.
      break;
    case TileKey.WEB:
      // TODO: Handle this case.
      break;
  }
  throw Exception("TileSpace without factory: " + spaceTile.tileKey.toString());
}

enum TileKey {
  PRINTER_STATUS,
  PRINTER_COORDINATES,
  PRINTER_TOOL,
  PRINTER_BED,
  PRINTER_CHART,
  PRINTER_JOB_FILE,
  PRINTER_JOB_PROGRESS,
  PRINTER_JOB_LAYERS,
  PRINTER_JOB_TIME,
  PRINTER_JOB_FILAMENT,
  PRINTER_JOB_FILAMENTS,
  CAM,
  WEB,
}
