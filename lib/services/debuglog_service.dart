import 'dart:async';
import 'dart:developer';

import 'package:dac/model/base_record.dart';
import 'package:dac/repositories/debuglog_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:intl/intl.dart';
import 'package:sorted_list/sorted_list.dart';

class DebugLog extends Equatable implements BaseRecord {
  final int id;
  final String log;
  final DateTime time;
  final DebugLevel level;

  const DebugLog({
    @required this.id,
    @required this.log,
    @required this.time,
    @required this.level,
  });

  @override
  List<Object> get props => [id, time, log, level];

  @override
  String toString() {
    return DateFormat.Hms().format(time) + " " + level.toString() + " " + log;
  }

  DebugLog copyWith({
    int id,
    String log,
    DateTime time,
    DebugLevel level,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (log == null || identical(log, this.log)) &&
        (time == null || identical(time, this.time)) &&
        (level == null || identical(level, this.level))) {
      return this;
    }

    return new DebugLog(
      id: id ?? this.id,
      log: log ?? this.log,
      time: time ?? this.time,
      level: level ?? this.level,
    );
  }

  @override
  DebugLog setId(int id) {
    return copyWith(id: id);
  }
}

enum DebugLevel { INFO, ERROR }

final DebugLogService debug = DebugLogService();

class DebugLogService {
  final _controller = StreamController<DebugLog>.broadcast();
  final _repository = DebugLogRepository.db;

  Stream<DebugLog> get stream => _controller.stream;

  DebugLogService._();

  static final DebugLogService _instance = DebugLogService._();

  factory DebugLogService() {
    return _instance;
  }

  void close() {
    _controller.close();
  }

  Future<List<DebugLog>> load({int limit = 500}) async {
    return _repository.getAll(limit: limit);
  }

  void _log(String text, DebugLevel level) {
    var debugLog = DebugLog(log: text, time: DateTime.now(), level: level);
    log(text);
    _repository.addToDatabase(debugLog);
    _controller.sink.add(debugLog);
  }

  void i(String log) {
    _log(log, DebugLevel.INFO);
  }

  void e(String log) {
    _log(log, DebugLevel.ERROR);
  }
}

final debugLogProvider = ChangeNotifierProvider((ref) => DebugLogNotifier());

class DebugLogNotifier extends ChangeNotifier {
  var records = SortedList<DebugLog>((a, b) => b.time.compareTo(a.time));

  DebugLogNotifier() {
    DebugLogService().load().then((value) {
      records.addAll(value);
      notifyListeners();
    });
    DebugLogService().stream.listen((value) {
      records.add(value);
      notifyListeners();
    });
  }
}
