import 'package:dac/model/settings.dart';
import 'package:dac/model/space.dart';
import 'package:dac/repositories/spaces_repository.dart';
import 'package:dac/services/base_service.dart';
import 'package:dac/services/settings_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:flutter/foundation.dart';
import 'package:hooks_riverpod/all.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// Providers

final spacesListProvider =
    ChangeNotifierProvider((ref) => _SpacesListNotifier(ref));

final spaceProvider =
    StateNotifierProvider.family((ref, space) => _SpaceNotifier(space));

final spaceStoredProvider = FutureProvider<Space>((ref) async {
  Settings settings = await SettingsService().getAll();
  if (settings.lastSpace != null) {
    List<Space> records = await _SpacesService().getAll();
    Space space = records.firstWhere(
        (element) => element.id == settings.lastSpace,
        orElse: null);
    ref.read(spaceSelectedProvider).setSpaceSelected(
        SpaceSelected(space, records.indexOf(space), records.length));
    return space;
  } else {
    return null;
  }
});

final spaceSelectedProvider =
    StateNotifierProvider((ref) => _SpaceSelectedNotifier(ref));

final spaceTilesListProvider =
    ChangeNotifierProvider.family<_SpaceTilesListNotifier, Space>(
        (ref, space) => _SpaceTilesListNotifier(space));

final spaceTileProvider = StateNotifierProvider.family(
    (ref, spaceTile) => _SpaceTileNotifier(spaceTile));

final spaceTileSettingsNotifierProvider = ChangeNotifierProvider(
    (ref) => _SpaceTileSettingsSelectNotifier<SpaceTileSettings>());

/// Notifiers

class _SpaceTileSettingsSelectNotifier<T> extends ChangeNotifier {
  T selected;

  void load(T record) {
    selected = record;
  }

  void change(T record) {
    selected = record;
    notifyListeners();
  }
}

class _SpacesListNotifier extends ChangeNotifier {
  final ProviderReference reference;

  List<Space> records = [];

  _SpacesListNotifier(this.reference) {
    _SpacesService().getAll().then((value) {
      records.clear();
      records.addAll(value);
      notifyListeners();
    });
    _SpacesService().stream.listen((value) {
      records.clear();
      records.addAll(value);
      notifyListeners();
    });
  }

  int get count => records.length;

  Future<Space> save(Space space) async {
    Space record = await _SpacesService().save(space);
    if (!records.contains(records) && record.id != null) {
      records.add(record);
    }
    return record;
  }

  void delete(Space space) async {
    int index = records.indexOf(space);
    records.remove(space);
    Space previousAvailable = index > 0
        ? records[index - 1]
        : records.length > 0
            ? records[0]
            : null;
    reference.read(spaceSelectedProvider).setSpace(previousAvailable);
    _SpaceTilesService().removeAllFromSpace(space.id);
    _SpacesService().remove(space.id);
  }

  Space getBySpaceId(int spaceId) {
    return records.firstWhere((element) => element.id == spaceId, orElse: null);
  }

  int getSpaceIndex(Space space) {
    return records.indexOf(space);
  }
}

class SpaceSelected {
  final Space space;
  final int index;
  final int total;
  SpaceSelected(this.space, this.index, this.total);
}

class _SpaceSelectedNotifier extends StateNotifier<SpaceSelected> {
  final ProviderReference reference;
  _SpaceSelectedNotifier(this.reference) : super(null);

  void setSpace(Space space) {
    SettingsService().update(lastSpace: space.id);
    int index = reference.read(spacesListProvider).getSpaceIndex(space);
    int total = reference.read(spacesListProvider).records.length;
    setSpaceSelected(SpaceSelected(space, index, total));
  }

  void setSpaceId(int lastSpace) {
    Space space = reference.read(spacesListProvider).getBySpaceId(lastSpace);
    if (space != null) {
      setSpace(space);
    }
  }

  void setSpaceIndex(int index) {
    Space space = reference.read(spacesListProvider).records[index];
    setSpace(space);
  }

  void setSpaceSelected(SpaceSelected spaceSelected) {
    state = spaceSelected;
  }
}

class _SpaceNotifier extends StateNotifier<Space> {
  _SpaceNotifier(Space space) : super(space) {
    _SpacesService().stream.listen((records) {
      records.where((element) => element == space).forEach((element) {
        state = element;
      });
    });
  }

  void rename(String name) async {
    await _SpacesService()
        .save(state.copyWith(name: name))
        .then((value) => state = value);
  }
}

class _SpaceTilesListNotifier extends ChangeNotifier {
  final Space space;
  List<SpaceTile> records = [];

  _SpaceTilesListNotifier(this.space) {
    _SpaceTilesService().getAllWith(space).then((value) {
      records.clear();
      records.addAll(value);
      records.sort((a, b) => a.location.compareTo(b.location));
      notifyListeners();
    });
    _SpaceTilesService().stream.listen((value) {
      records.clear();
      records.addAll(
          value.where((element) => element.spaceId == space.id).toList());
      records.sort((a, b) => a.location.compareTo(b.location));
      notifyListeners();
    });
  }

  int get count => records.length;

  Future<SpaceTile> add(TileSelect tile, SpaceTileSettings settings) {
    SpaceTile spaceTile = SpaceTile(
        location: count,
        spaceId: space.id,
        tileKey: tile.tileKey,
        tileSize: tile.tileSize,
        tileSettings: settings);
    return _SpaceTilesService().save(spaceTile);
  }

  Future<SpaceTile> save(TileSpace tile, SpaceTileSettings settings) {
    SpaceTile spaceTile = tile.spaceTile.copyWith(tileSettings: settings);
    return _SpaceTilesService().save(spaceTile);
  }

  Future<void> remove(SpaceTile spaceTile) {
    return _SpaceTilesService().remove(spaceTile.id);
  }

  saveLocations(List<SpaceTile> spaceTiles) {
    _SpaceTilesService().saveLocations(spaceTiles);
  }
}

class _SpaceTileNotifier extends StateNotifier<SpaceTile> {
  _SpaceTileNotifier(SpaceTile record) : super(record) {
    _SpaceTilesService().stream.listen((records) {
      records.where((element) => element == record).forEach((element) {
        state = element;
      });
    });
  }
}

/// Services

class _SpacesService extends BaseService<Space, SpacesRepository> {
  static final _SpacesService _instance = _SpacesService._();

  _SpacesService._() : super(SpacesRepository.db);

  factory _SpacesService() {
    return _instance;
  }
}

class _SpaceTilesService extends BaseService<SpaceTile, SpaceTilesRepository> {
  static final _SpaceTilesService _instance = _SpaceTilesService._();

  _SpaceTilesService._() : super(SpaceTilesRepository.db);

  factory _SpaceTilesService() {
    return _instance;
  }

  Future<List<SpaceTile>> getAllWith(Space space) async {
    List<SpaceTile> spaceTiles = await repository.getAllWhere(
        column: SpaceTilesRepository.COLUMN_SPACE_ID, value: space.id);

    List<SpaceTile> result = [];
    spaceTiles.forEach((element) {
      TileKey.values.contains(element.tileKey)
          ? result.add(element)
          : remove(element.id);
    });
    return result;
  }

  void saveAll(List<SpaceTile> spaceTiles) {
    spaceTiles.forEach((element) {
      repository.update(element);
    });
    super.notifyAll();
  }

  void removeAllFromSpace(int spaceId) async {
    repository.removeAllWithSpaceId(spaceId: spaceId);
    super.notifyAll();
  }

  void saveLocations(List<SpaceTile> spaceTiles) {
    List<SpaceTile> spaceTilesNew = [];
    for (int i = 0; i < spaceTiles.length; i++) {
      spaceTilesNew.add(spaceTiles[i].copyWith(location: i));
    }
    saveAll(spaceTilesNew);
  }
}
