import 'dart:async';

import 'package:dac/model/heater_log.dart';
import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/repositories/temperatures_repository.dart';
import 'package:dac/services/printers_data_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:hooks_riverpod/all.dart';

const Duration MAX_DATETIME_RANGE = Duration(minutes: 120);

class PrinterHeaterLog extends Equatable {
  final Printer printer;
  final List<HeaterLogValue> heatersLog;

  PrinterHeaterLog(this.printer, this.heatersLog);

  @override
  List<Object> get props => [printer, heatersLog];
}

class HeatersLogService {
  final _controller = StreamController<PrinterHeaterLog>.broadcast();

  Stream<PrinterHeaterLog> get stream => _controller.stream;

  HeatersLogService._() {
    PrintersDataService().stream.listen((pd) {
      processToolsData(pd.printer, pd.tools, pd.bed);
    });
  }

  static final HeatersLogService _instance = HeatersLogService._();

  factory HeatersLogService() {
    return _instance;
  }

  void close() {
    _controller.close();
  }

  Future<List<HeaterLogValue>> loadLatest(Printer printer) async {
    DateTime from = DateTime.now().subtract(MAX_DATETIME_RANGE);
    return printer?.id != null
        ? HeaterLogRepository.db.getFromDateTime(printer.id, from)
        : [];
  }

  void processToolsData(Printer printer, List<Tool> tools, Bed bed) {
    var now = DateTime.now();
    Map<int, _HeaterSet> heaters = {};
    List<HeaterLogValue> records = [];
    tools.forEach((tool) {
      tool.heaters.forEach((heater) {
        TemperatureSet temperatureSet = tool.temperaturesSets
            .firstWhere((element) => element.heaterNumber == heater.number);
        heaters.putIfAbsent(
            heater.number, () => _HeaterSet(heater, temperatureSet));
      });
    });

    heaters.putIfAbsent(
        bed.heater.number, () => _HeaterSet(bed.heater, bed.temperatureSet));

    heaters.forEach((key, value) {
      records.add(_saveHeaterLogValue(printer, now, value));
    });
    _controller.sink.add(PrinterHeaterLog(printer, records));
  }

  HeaterLogValue _saveHeaterLogValue(
      Printer printer, DateTime dateTime, _HeaterSet heaterSet) {
    var record = HeaterLogValue(
      printerId: printer.id,
      heater: heaterSet.heater.number,
      dateTime: dateTime,
      temperatureCurrent: heaterSet.heater.current,
      temperatureActive: heaterSet.temperatureSet.active,
      temperatureStandby: heaterSet.temperatureSet.standby,
      state: heaterSet.heater.state,
    );
    HeaterLogRepository.db.addToDatabase(record);
    return record;
  }
}

class _HeaterSet {
  final Heater heater;
  final TemperatureSet temperatureSet;

  _HeaterSet(this.heater, this.temperatureSet);
}

final heatersLogHistoryProvider = ChangeNotifierProvider.family(
    (ref, printer) => _HeatersLogHistoryNotifier(ref, printer));

class _HeatersLogHistoryNotifier extends ChangeNotifier {
  Map<int, List<HeaterLogValue>> heatersLog = {};

  _HeatersLogHistoryNotifier(ProviderReference ref, Printer printer) {
    HeatersLogService().loadLatest(printer).then((value) => _updateData(value));
    HeatersLogService().stream.listen((value) {
      if (value.printer == printer) {
        _updateData(value.heatersLog);
      }
    });
  }

  void _updateData(List<HeaterLogValue> heatersLogs) {
    heatersLogs.forEach((heaterLog) {
      if (!heatersLog.containsKey(heaterLog.heater)) {
        heatersLog[heaterLog.heater] = [heaterLog];
      } else {
        heatersLog[heaterLog.heater].add(heaterLog);
      }
    });
    notifyListeners();
  }
}
