import 'dart:async';
import 'dart:collection';

import 'package:dac/model/printer.dart';
import 'package:dac/model/printer_data.dart';
import 'package:dac/model/printer_job.dart';
import 'package:dac/model/rr_fileinfo.dart';
import 'package:dac/model/status_type1.dart' as status1;
import 'package:dac/model/status_type3.dart' as status3;
import 'package:dac/services/debuglog_service.dart';
import 'package:dac/services/http_service.dart';
import 'package:dac/services/printers_info_service.dart';
import 'package:dac/services/printers_service.dart';
import 'package:dac/services/settings_service.dart';

const PERIOD = const Duration(seconds: 5);

class PrintersDataService {
  static final PrintersDataService _instance = PrintersDataService._();

  factory PrintersDataService() {
    return _instance;
  }

  final _controller = StreamController<PrinterData>.broadcast();

  Stream<PrinterData> get stream => _controller.stream;

  HashSet<Printer> _loading = HashSet();
  HashSet<Printer> _notConnected = HashSet();
  HashSet<Printer> _error = HashSet();
  List<Printer> _printers = [];
  Map<Printer, int> _updateType = {};
  Timer _timer;

  PrintersDataService._() {
    PrintersService().getAll().then((printers) {
      this._printers = printers;
      _notConnected.addAll(_printers);
      _printers.forEach((printer) => _updateType.putIfAbsent(printer, () => 1));
      _updateType.removeWhere((key, value) => !printers.contains(key));
    });
    PrintersService().stream.listen((printers) {
      this._printers = printers;
      _notConnected.addAll(_printers);
      _updateType.removeWhere((key, value) => !printers.contains(key));
      _printers.forEach((printer) => _updateType.putIfAbsent(printer, () => 1));
      _error.clear();
    });

    SettingsService()
        .stream
        .listen((settings) => _setTimer(settings.printerUpdateIntervalSeconds));

    SettingsService()
        .getAll()
        .then((settings) => _setTimer(settings.printerUpdateIntervalSeconds));
  }

  void _setTimer(int seconds) {
    if (_timer != null) _timer.cancel();
    _timer = Timer.periodic(Duration(seconds: seconds), (_) => _tick());
    debug.i("Started timer. Update interval: " + seconds.toString());
  }

  void close() {
    _timer.cancel();
    _controller.close();
  }

  void _tick() {
    _printers.forEach((printer) {
      if (printer.enabled) {
        if (!_loading.contains(printer)) {
          _loading.add(printer);
          _getPrinterData(printer)
              .then((printerData) => _controller.sink.add(printerData))
              .then((value) => _loading.remove(printer))
              .then((value) {
            if (_notConnected.contains(printer)) {
              _notConnected.remove(printer);
              debug.i(printer.hostname + " connected");
            }
            _error.remove(printer);
          }).catchError((e) {
            _loading.remove(printer);
            _notConnected.add(printer);
            if (!_error.contains(printer)) {
              _error.add(printer);
              debug.e("Error loading data from: " +
                  printer.toString() +
                  "\n" +
                  e.toString());
              _controller.sink.addError(PrinterData(printer));
            }
          });
        }
      } else {
        _loading.remove(printer);
      }
    });
  }

  Future<PrinterData> _getPrinterData(Printer printer) async {
    if (_updateType.containsKey(printer)) {
      PrinterInfo printerInfo =
          await PrintersInfoService().records.get(printer.id.toString());

      PrinterData printerData;
      if (_updateType[printer] == 1) {
        status1.StatusType1 status =
            await HttpService().fetchStatusType1(printer);
        printerData = _processFromStatusType1(printer, printerInfo, status);
      } else {
        status3.StatusType3 status =
            await HttpService().fetchStatusType3(printer);
        printerData = _processFromStatusType3(printer, printerInfo, status);
      }
      if (printerData.status != null) {
        switch (printerData.status) {
          case MachineStatus.READING_CONFIGURATION:
          case MachineStatus.FLASHING_FIRMWARE:
          case MachineStatus.HALTED:
          case MachineStatus.OFF:
          case MachineStatus.IDLE:
            _updateType.update(printer, (value) => 1);
            break;
          case MachineStatus.PAUSING_PRINT:
          case MachineStatus.RESUMING_PRINT:
          case MachineStatus.PRINT_PAUSED:
          case MachineStatus.SIMULATING_PRINT:
          case MachineStatus.PRINTING:
          case MachineStatus.CHANGING_TOOL:
          case MachineStatus.BUSY:
            _updateType.update(printer, (value) => 3);
            break;
        }
      }
      return printerData;
    } else {
      throw Exception("Error loading printer data. UpdateType not present.");
    }
  }

  Future<JobFile> getPrinterFileInfo(Printer printer) async {
    if (printer?.hostname?.isEmpty ?? true) {
      return null;
    } else {
      FileInfo result = await HttpService().fetchFileInfo(printer);
      return _processJobFile(printer, result);
    }
  }

  JobFile _processJobFile(Printer printer, FileInfo fileInfo) {
    return JobFile(
      fileName: fileInfo.fileName,
      generatedBy: fileInfo.generatedBy,
      fileSize: fileInfo.size,
      lastModified: fileInfo.lastModified,
      totalHeight: fileInfo.height,
      totalLayers: fileInfo.height != null && fileInfo.layerHeight != null
          ? fileInfo.height ~/ fileInfo.layerHeight
          : null,
      totaDuration: fileInfo.printTime != null
          ? Duration(milliseconds: (fileInfo.printTime * 1000).toInt())
          : null,
      filamentsLength: fileInfo.filament,
    );
  }

  PrinterData _processFromStatusType1(
      Printer printer, PrinterInfo printerInfo, status1.StatusType1 status) {
    return PrinterData(
      printer,
      status: _machineStatus(status.status),
      move: _machineMove(status.coords, status.speeds, status.params),
      bed: _bed(status.temps),
      tools: _tools(printerInfo, status.temps),
    );
  }

  PrinterData _processFromStatusType3(
      Printer printer, PrinterInfo printerInfo, status3.StatusType3 status) {
    return PrinterData(
      printer,
      status: _machineStatus(status.status),
      move: _machineMove(status.coords, status.speeds, status.params),
      bed: _bed(status.temps),
      tools: _tools(printerInfo, status.temps),
      jobStatus: _jobStatus(status),
    );
  }

  JobStatus _jobStatus(status3.StatusType3 status) {
    return JobStatus(
      warmUpDuration:
          Duration(milliseconds: (status.warmUpDuration * 1000).toInt()),
      firstLayer: JobLayer(
          layer: 1,
          height: status.firstLayerHeight,
          duration: Duration(
              milliseconds: (status.firstLayerDuration * 1000).toInt())),
      currentLayer: JobLayer(
          layer: status.currentLayer,
          height: status.currentLayer > 0 ? status.coords.xyz[2] : 0,
          duration:
              Duration(milliseconds: (status.currentLayerTime * 1000).toInt())),
      progress: JobProgress(
          printedPercent: status.fractionPrinted,
          duration:
              Duration(milliseconds: (status.printDuration * 1000).toInt()),
          filamentsLength: status.extrRaw),
      jobLeft: JobLeft(
          durationFile: Duration(
              milliseconds: ((status.timesLeft?.file ?? 0) * 1000).toInt()),
          durationFilament: Duration(
              milliseconds: ((status.timesLeft?.filament ?? 0) * 1000).toInt()),
          durationLayer: Duration(
              milliseconds: ((status.timesLeft?.layer ?? 0) * 1000).toInt())),
    );
  }

  List<Tool> _tools(PrinterInfo printerInfo, status1.Temps temps) {
    List<Tool> tools = [];
    for (int toolIndex = 0; toolIndex < printerInfo.tools.length; toolIndex++) {
      var toolInfo = printerInfo.tools[toolIndex];
      try {
        List<Heater> heaters = [];
        List<TemperatureSet> temperaturesSets = [];
        for (int heaterIndex = 0;
            heaterIndex < toolInfo.heaters.length;
            heaterIndex++) {
          int heaterNumber = toolInfo.heaters[heaterIndex];

          bool heaterExists = temps.state.length > heaterNumber;

          Heater heater = Heater(
            heaterNumber,
            heaterExists ? temps.current[heaterNumber] : null,
            heaterExists ? _heaterState(temps.state[heaterNumber]) : null,
          );
          heaters.add(heater);

          TemperatureSet temperatureSet = TemperatureSet(
            heaterNumber,
            heaterExists
                ? temps.tempsTools.active[toolIndex][heaterIndex]
                : null,
            heaterExists
                ? temps.tempsTools.standby[toolIndex][heaterIndex]
                : null,
          );
          temperaturesSets.add(temperatureSet);
        }
        Tool tool = new Tool(toolInfo.number, heaters, temperaturesSets);
        tools.add(tool);
      } catch (e) {
        throw Exception(
            "Error loading tool from temperatures. " + e.toString());
      }
    }
    return tools;
  }

  Bed _bed(status1.Temps temps) {
    int heaterNumber = temps.tempsBed.heater;

    Heater heater = Heater(
      heaterNumber,
      temps.current[heaterNumber],
      _heaterState(temps.state[heaterNumber]),
    );

    TemperatureSet temperatureSet = TemperatureSet(
      heaterNumber,
      temps.tempsBed.active,
      temps.tempsBed.standby,
    );

    Bed bed = new Bed(heater, temperatureSet);
    return bed;
  }

  Move _machineMove(
      status1.Coords coords, status1.Speeds speeds, status1.Params params) {
    List<Axis> axes = [];
    for (int drive = 0; drive < coords.xyz.length; drive++) {
      double userPosition = coords.xyz[drive];
      double machinePosition = coords.machine[drive];
      Axis axis = new Axis(
          drive, coords.axesHomed[drive] == 1, userPosition, machinePosition);
      axes.add(axis);
    }

    MoveSpeed currentMove = new MoveSpeed(speeds.requested, speeds.top);

    List<Extruder> extruders = [];
    for (int drive = 0; drive < coords.extr.length; drive++) {
      Extruder extruder = new Extruder(drive + coords.xyz.length,
          params.extrFactors[drive] / 100, coords.extr[drive]);
      extruders.add(extruder);
    }

    Move move =
        new Move(axes, currentMove, extruders, params.speedFactor / 100);

    return move;
  }

  MachineStatus _machineStatus(String status) {
    switch (status.substring(0, 1).toUpperCase()) {
      case "C":
        return MachineStatus.READING_CONFIGURATION;
      case "F":
        return MachineStatus.FLASHING_FIRMWARE;
      case "H":
        return MachineStatus.HALTED;
      case "O":
        return MachineStatus.OFF;
      case "D":
        return MachineStatus.PAUSING_PRINT;
      case "R":
        return MachineStatus.RESUMING_PRINT;
      case "S":
        return MachineStatus.PRINT_PAUSED;
      case "M":
        return MachineStatus.SIMULATING_PRINT;
      case "P":
        return MachineStatus.PRINTING;
      case "T":
        return MachineStatus.CHANGING_TOOL;
      case "B":
        return MachineStatus.BUSY;
      case "I":
        return MachineStatus.IDLE;
      default:
        throw Exception("Malformed status data. Wrong status: " + status);
    }
  }

  HeaterState _heaterState(int state) {
    switch (state) {
      case 0:
        return HeaterState.OFF;
      case 1:
        return HeaterState.STANDBY;
      case 2:
        return HeaterState.ACTIVE;
      case 3:
        return HeaterState.FAULT;
      case 4:
        return HeaterState.TUNING;
      case 5:
        return HeaterState.OFFLINE;
      default:
        throw Exception(
            "Malformed status data. Wrong heater state: " + state.toString());
    }
  }

  static bool isPrinting(MachineStatus status) {
    switch (status) {
      case MachineStatus.READING_CONFIGURATION:
      case MachineStatus.FLASHING_FIRMWARE:
      case MachineStatus.HALTED:
      case MachineStatus.OFF:
      case MachineStatus.IDLE:
        return false;
      case MachineStatus.PAUSING_PRINT:
      case MachineStatus.RESUMING_PRINT:
      case MachineStatus.PRINT_PAUSED:
      case MachineStatus.SIMULATING_PRINT:
      case MachineStatus.PRINTING:
      case MachineStatus.CHANGING_TOOL:
      case MachineStatus.BUSY:
        return true;
    }
    return false;
  }
}
