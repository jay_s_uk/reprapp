import 'dart:async';
import 'dart:developer';

import 'package:dac/services/debuglog_service.dart';

import 'base_repository.dart';
import 'database.dart';

class DebugLogRepository extends BaseRepository<DebugLog> {
  static const String DB_TABLE = "DebugLog";
  static const String COLUMN_DATETIME = 'datetime';
  static const String COLUMN_LOG = 'log';
  static const String COLUMN_LEVEL = 'level';

  DebugLogRepository._() : super(DB_TABLE) {
    deleteOld();
  }

  static final DebugLogRepository db = DebugLogRepository._();

  Future<List<DebugLog>> getAll({int limit = 500}) async {
    final db = await MainDatabase.db.database;
    var response = await db.query(DB_TABLE,
        limit: limit, orderBy: "$COLUMN_DATETIME desc");
    List<DebugLog> list =
        response.isNotEmpty ? response.map((e) => fromMap(e)).toList() : [];
    log("Database $DB_TABLE load all " + list.length.toString());
    return list;
  }

  void deleteOld({int days = 7}) async {
    final db = await MainDatabase.db.database;
    db.delete(DB_TABLE, where: "$COLUMN_DATETIME < ?", whereArgs: [
      DateTime.now().subtract(Duration(days: days)).millisecondsSinceEpoch
    ]);
    debug.i("Database $DB_TABLE deleted older than $days days");
  }

  DebugLog fromMap(Map<String, dynamic> map) {
    return new DebugLog(
        id: map[BaseRepository.COLUMN_ID] as int,
        time: DateTime.fromMillisecondsSinceEpoch(map[COLUMN_DATETIME] as int),
        log: map[COLUMN_LOG] as String,
        level: DebugLevel.values[map[COLUMN_LEVEL] as int]);
  }

  Map<String, dynamic> toMap(DebugLog record) {
    var map = <String, dynamic>{
      COLUMN_DATETIME: record.time.millisecondsSinceEpoch,
      COLUMN_LOG: record.log,
      COLUMN_LEVEL: record.level.index
    };
    if (record.id != null) {
      map[BaseRepository.COLUMN_ID] = record.id;
    }
    return map;
  }
}
