import 'dart:developer';

import 'package:dac/model/heater_log.dart';
import 'package:dac/model/printer_data.dart';

import 'base_repository.dart';
import 'database.dart';

class HeaterLogRepository extends BaseRepository<HeaterLogValue> {
  static const String DB_TABLE = "Heater";
  static const String COLUMN_PRINTER_ID = 'printerId';
  static const String COLUMN_HEATER = 'heater';
  static const String COLUMN_DATETIME = 'datetime';
  static const String COLUMN_TEMPERATURE_CURRENT = 'temperatureCurrent';
  static const String COLUMN_TEMPERATURE_ACTIVE = 'temperatureActive';
  static const String COLUMN_TEMPERATURE_STANDBY = 'temperatureStandby';
  static const String COLUMN_STATE = 'state';

  HeaterLogRepository._() : super(DB_TABLE);

  static final HeaterLogRepository db = HeaterLogRepository._();

  Future<List<HeaterLogValue>> getFromDateTime(
      int printerId, DateTime from) async {
    final db = await MainDatabase.db.database;
    var response = await db.query(DB_TABLE,
        where: "$COLUMN_PRINTER_ID = ? AND $COLUMN_DATETIME >= ?",
        whereArgs: [
          printerId,
          from.millisecondsSinceEpoch,
        ],
        orderBy: "$COLUMN_DATETIME");
    List<HeaterLogValue> list =
        response.isNotEmpty ? response.map((e) => fromMap(e)).toList() : [];
    log("Database load date range from: $DB_TABLE " + list.length.toString());
    return list;
  }

  HeaterLogValue fromMap(Map<String, dynamic> map) {
    return new HeaterLogValue(
      id: map[BaseRepository.COLUMN_ID] as int,
      printerId: map[COLUMN_PRINTER_ID] as int,
      heater: map[COLUMN_HEATER] as int,
      dateTime:
          DateTime.fromMillisecondsSinceEpoch(map[COLUMN_DATETIME] as int),
      temperatureCurrent: map[COLUMN_TEMPERATURE_CURRENT] as double,
      temperatureActive: map[COLUMN_TEMPERATURE_ACTIVE] as double,
      temperatureStandby: map[COLUMN_TEMPERATURE_STANDBY] as double,
      state: map[COLUMN_STATE] != null
          ? HeaterState.values[map[COLUMN_STATE] as int]
          : null,
    );
  }

  Map<String, dynamic> toMap(HeaterLogValue record) {
    var map = <String, dynamic>{
      COLUMN_PRINTER_ID: record.printerId,
      COLUMN_HEATER: record.heater,
      COLUMN_DATETIME: record.dateTime.millisecondsSinceEpoch,
      COLUMN_TEMPERATURE_CURRENT: record.temperatureCurrent,
      COLUMN_TEMPERATURE_ACTIVE: record.temperatureActive,
      COLUMN_TEMPERATURE_STANDBY: record.temperatureStandby,
      COLUMN_STATE: record.state?.index
    };
    if (record.id != null) {
      map[BaseRepository.COLUMN_ID] = record.id;
    }
    return map;
  }
}
