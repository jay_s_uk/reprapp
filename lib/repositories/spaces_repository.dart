import 'package:dac/model/space.dart';
import 'package:dac/services/base_service.dart';
import 'package:dac/services/debuglog_service.dart';
import 'package:dac/services/tiles_service.dart';
import 'package:dac/widgets/tiles/tile_widget_base.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/foundation.dart';

import 'base_repository.dart';
import 'database.dart';

class SpacesRepository extends BaseRepository<Space> {
  static const String DB_TABLE = "Spaces";
  static const String COLUMN_NAME = 'name';

  SpacesRepository._() : super(DB_TABLE);

  static final SpacesRepository db = SpacesRepository._();

  @override
  Space fromMap(Map<String, dynamic> map) {
    return new Space(
      id: cast<int>(map[BaseRepository.COLUMN_ID]),
      name: map[COLUMN_NAME]?.toString(),
    );
  }

  @override
  Map<String, dynamic> toMap(Space record) {
    var map = <String, dynamic>{
      COLUMN_NAME: record.name,
    };
    if (record.id != null) {
      map[BaseRepository.COLUMN_ID] = record.id;
    }
    return map;
  }
}

class SpaceTilesRepository extends BaseRepository<SpaceTile> {
  static const String DB_TABLE = "SpaceTiles";
  static const String COLUMN_SPACE_ID = 'spaceId';
  static const String COLUMN_TILE_KEY = 'tileKey';
  static const String COLUMN_TILE_SIZE = 'tileSize';
  static const String COLUMN_TILE_SETTINGS = 'tileSettings';
  static const String COLUMN_LOCATION = 'location';

  SpaceTilesRepository._() : super(DB_TABLE);

  static final SpaceTilesRepository db = SpaceTilesRepository._();

  @override
  SpaceTile fromMap(Map<String, dynamic> map) {
    return new SpaceTile(
      id: cast<int>(map[BaseRepository.COLUMN_ID]),
      spaceId: cast<int>(map[COLUMN_SPACE_ID]),
      tileKey: EnumToString.fromString(TileKey.values, map[COLUMN_TILE_KEY]),
      tileSize: TileSize.fromString(map[COLUMN_TILE_SIZE]),
      tileSettings: SpaceTileSettings.fromJson(map[COLUMN_TILE_SETTINGS]),
      location: cast<int>(map[COLUMN_LOCATION]),
    );
  }

  @override
  Map<String, dynamic> toMap(SpaceTile record) {
    var map = <String, dynamic>{
      COLUMN_SPACE_ID: record.spaceId,
      COLUMN_TILE_KEY: EnumToString.convertToString(record.tileKey),
      COLUMN_TILE_SIZE: record.tileSize.toString(),
      COLUMN_TILE_SETTINGS: record.tileSettings?.toJson(),
      COLUMN_LOCATION: record.location,
    };
    if (record.id != null) {
      map[BaseRepository.COLUMN_ID] = record.id;
    }
    return map;
  }

  Future<void> removeAllWithSpaceId({@required int spaceId}) async {
    final db = await MainDatabase.db.database;
    if (spaceId != null) {
      await db
          .query(dbTable, where: "$COLUMN_SPACE_ID = ?", whereArgs: [spaceId]);
      debug.i("Database remove from: $dbTable where spaceId = " +
          spaceId.toString());
    }
  }
}
