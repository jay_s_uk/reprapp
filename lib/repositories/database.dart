import 'dart:io';

import 'package:dac/repositories/printers_repository.dart';
import 'package:dac/repositories/spaces_repository.dart';
import 'package:dac/repositories/temperatures_repository.dart';
import 'package:dac/repositories/webcams_repository.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_migration/sqflite_migration.dart';

import 'base_repository.dart';
import 'debuglog_repository.dart';

const String DB_NAME = "reprapp.db";

const List<String> initialScript = [
  """
      CREATE TABLE ${PrintersRepository.DB_TABLE}(
          ${BaseRepository.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT,
          ${PrintersRepository.COLUMN_HOSTNAME} TEXT,
          ${PrintersRepository.COLUMN_NAME} TEXT,
          ${PrintersRepository.COLUMN_VERSION} INTEGER,
          ${PrintersRepository.COLUMN_ENABLED} INTEGER
          ) """,
  """
      CREATE TABLE ${HeaterLogRepository.DB_TABLE}(
          ${BaseRepository.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT,
          ${HeaterLogRepository.COLUMN_PRINTER_ID} INTEGER,
          ${HeaterLogRepository.COLUMN_HEATER} INTEGER,
          ${HeaterLogRepository.COLUMN_DATETIME} INTEGER,
          ${HeaterLogRepository.COLUMN_TEMPERATURE_CURRENT} REAL,
          ${HeaterLogRepository.COLUMN_TEMPERATURE_ACTIVE} REAL,
          ${HeaterLogRepository.COLUMN_TEMPERATURE_STANDBY} REAL,
          ${HeaterLogRepository.COLUMN_STATE} INTEGER
          ) """,
  """
      CREATE TABLE ${DebugLogRepository.DB_TABLE}(
          ${BaseRepository.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT,
          ${DebugLogRepository.COLUMN_DATETIME} INTEGER,
          ${DebugLogRepository.COLUMN_LOG} STRING,
          ${DebugLogRepository.COLUMN_LEVEL} INTEGER
          ) """,
  """
      CREATE TABLE ${WebcamsRepository.DB_TABLE}(
          ${BaseRepository.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT,
          ${WebcamsRepository.COLUMN_URL} STRING,
          ${WebcamsRepository.COLUMN_NAME} STRING,
          ${WebcamsRepository.COLUMN_ENABLED} INTEGER,
          ${WebcamsRepository.COLUMN_FLIP} INTEGER,
          ${WebcamsRepository.COLUMN_ROTATE} INTEGER
          ) """,
  """
      CREATE TABLE ${SpacesRepository.DB_TABLE}(
          ${BaseRepository.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT,
          ${SpacesRepository.COLUMN_NAME} STRING
          ) """,
  """
      CREATE TABLE ${SpaceTilesRepository.DB_TABLE}(
          ${BaseRepository.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT,
          ${SpaceTilesRepository.COLUMN_SPACE_ID} INTEGER,
          ${SpaceTilesRepository.COLUMN_TILE_KEY} STRING,
          ${SpaceTilesRepository.COLUMN_TILE_SIZE} STRING,
          ${SpaceTilesRepository.COLUMN_TILE_SETTINGS} STRING,
          ${SpaceTilesRepository.COLUMN_LOCATION} INTEGER
          ) """,
];
const List<String> migrations = [];

class MainDatabase {
  MainDatabase._();

  static final MainDatabase db = MainDatabase._();
  Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await getDatabaseInstance();
    return _database;
  }

  final config = MigrationConfig(
      initializationScript: initialScript, migrationScripts: migrations);

  Future<Database> getDatabaseInstance() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, DB_NAME);
    return await openDatabaseWithMigration(path, config);
  }
}
