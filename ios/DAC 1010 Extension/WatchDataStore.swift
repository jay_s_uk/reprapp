//
//  WatchDataStore.swift
//  DAC 1010 Extension
//
//  Created by João Martins on 27/12/2020.
//

import WatchConnectivity
import WatchKit

protocol HasDataStore {
    func updatedDataStore()
    func updatedPrinterList()
    func updatedPrinter(printerId: String)
}

class WatchDataStore: WKInterfaceController, WCSessionDelegate, HasDataStore {
    static var storePrinters: StorePrinters?

    private var session = WCSession.default
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        processData(data: applicationContext)
    }
     
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        processData(data: message)
    }

    func processData(data: [String : Any]) {
        if let storeData = data[WatchMessagesConstants.store] as? Data {
            let initialLoad = WatchDataStore.storePrinters == nil
            WatchDataStore.storePrinters = factoryStoreFromData(data: storeData)!
                .sorted(by: { ($0.printer?.name ?? "") < ($1.printer?.name ?? "") })
            updatedDataStore()
            if initialLoad {
                updatedPrinterList()
            } else {
                if let method = data[WatchMessagesConstants.method] as? String {
                    if method == WatchMessagesConstants.printersList {
                        updatedPrinterList()
                    }
                }
                if let printerId = data[WatchMessagesConstants.printerId] as? String {
                    updatedPrinter(printerId: printerId)
                }
            }
        } else {
            print("Error loading context store")
        }
    }

    func updatedPrinter(printerId: String) {
    }

    func updatedPrinterList() {
    }

    func updatedDataStore() {
    }

    override func willActivate() {
        super.willActivate()
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
    }

}
