//
//  PrintersRowController.swift
//  DAC 1010 Extension
//
//  Created by João Martins on 23/12/2020.
//

import Foundation
import WatchKit

class PrintersRowController: NSObject {
    @IBOutlet weak var printerName: WKInterfaceLabel!
    
    @IBOutlet weak var printerHostname: WKInterfaceLabel!
}
