//
//  PrinterController.swift
//  DAC 1010 Extension
//
//  Created by João Martins on 24/12/2020.
//

import WatchKit
import Foundation
import WatchConnectivity

class PrinterController: WatchDataStore {
    
    var printerId: String?
    
    @IBOutlet weak var table: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        table.setRowTypes(["PrinterStatusController",
                           "PrinterTemperatureController",
                           "PrinterTemperatureController",
                           "PrinterJobController"])
        if let printer = context as? StorePrinter {
            printerId = printer.printerId
            updateDisplay(printer: printer)
        }
    }
    
    func updateDisplay(printer: StorePrinter){
        if let row = table.rowController(at: 0) as? PrinterStatusController {
            row.setProgress(value: Int(printer.jobStatus?.progress ?? 0))
            row.setStatus(value: printer.printerData?.status?.description)
        }
        if let row = table.rowController(at: 1) as? PrinterTemperatureController {
            row.setData(data: printer.printerData, toolIndex: 0)
        }
        if let row = table.rowController(at: 2) as? PrinterTemperatureController {
            row.setData(data: printer.printerData)
        }
        if let row = table.rowController(at: 3) as? PrinterJobController {
            row.setData(data: printer.jobStatus)
        }
        self.setTitle(printer.printer?.name ?? "Printer")
    }
    
    override func updatedPrinterList() {
        if let printer = (WatchDataStore.storePrinters?.first(where: { $0.printerId == printerId })) {
            updateDisplay(printer: printer)
        }
    }
    
    override func updatedPrinter(printerId: String) {
        if(self.printerId == printerId){
            if let printer = (WatchDataStore.storePrinters?.first(where: { $0.printerId == printerId })) {
                updateDisplay(printer: printer)
            }
        }
    }
}
