//
//  PrinterBedController.swift
//  DAC 1010 Extension
//
//  Created by João Martins on 25/12/2020.
//

import WatchKit
import Foundation

class PrinterTemperatureController: NSObject {
    
    @IBOutlet weak var TemperatureGroup: WKInterfaceGroup!
    @IBOutlet weak var TemperatureCurrent: WKInterfaceLabel!
    @IBOutlet weak var Icon: WKInterfaceImage!
    @IBOutlet weak var ToolName: WKInterfaceLabel!
    @IBOutlet weak var TemperatureSet: WKInterfaceLabel!
    
    func setData(data: PrinterData?, toolIndex: Int? = nil) {
        if let index = toolIndex {
            setValues(current: data?.tools?[index].current, set: data?.tools?[index].set, name: "Tool \(index)")
            Icon.setImageNamed("nozzle_white")
        } else {
            setValues(current: data?.bed?.current, set: data?.bed?.set, name: "Bed")
            Icon.setImageNamed("bed_white")
        }
    }
    
    private func setValues(current: Double?, set: Double?, name: String) {
        TemperatureCurrent.setText(formatTemperature(value: current))
        ToolName.setText(name)
        
        if current != nil {
            if set != nil {
                if set! == 0 {
                    TemperatureGroup.setBackgroundColor(UIColor(named: "colorTempBackSet0")!)
                }
                if set! < current! {
                    TemperatureGroup.setBackgroundColor(UIColor(named: "colorTempBackSetReached")!)
                } else {
                    TemperatureGroup.setBackgroundColor(UIColor(named: "colorTempBackSetNotReached")!)
                }
                TemperatureSet.setText("Set to "+formatTemperature(value: set))
            } else {
                TemperatureGroup.setBackgroundColor(UIColor(named: "colorTempBackNotSet")!)
                TemperatureSet.setText("Off")
            }
        } else {
            TemperatureGroup.setBackgroundColor(UIColor(named: "colorTempBackUnknown")!)
            TemperatureSet.setText("Set to "+formatTemperature(value: set))
        }
    }
    
    private func formatTemperature(value: Double?) -> String {
        if let v = value {
            return String(format: "%.0f", v)+"°"
        } else {
            return "-°"
        }
    }
}
