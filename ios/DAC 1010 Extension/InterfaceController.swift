//
//  InterfaceController.swift
//  DAC 1010 Extension
//
//  Created by João Martins on 21/12/2020.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WatchDataStore {
    @IBOutlet weak var printersList: WKInterfaceTable!
    @IBOutlet weak var emptyGroup: WKInterfaceGroup!
    @IBOutlet weak var loadingGroup: WKInterfaceGroup!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        loadingGroup.setHidden(false)
        emptyGroup.setHidden(true)
        printersList.setHidden(true)
    }
    
    override func willActivate() {
        updatedPrinterList()
        super.willActivate()
    }

    override func contextForSegue(withIdentifier segueIdentifier: String,
                                  in table: WKInterfaceTable,
                                  rowIndex: Int) -> Any? {
         if segueIdentifier == "printerSelected" {
            return WatchDataStore.storePrinters?[rowIndex]
         }
         return nil
     }
    
    override func updatedPrinterList() {
        if let storePrinters = WatchDataStore.storePrinters {
            printersList.setNumberOfRows(storePrinters.count, withRowType: "PrintersRowController")
            for (i, item) in storePrinters.enumerated() {
                if let row = printersList.rowController(at: i) as? PrintersRowController {
                    row.printerName.setText(item.printer?.name ?? "Printer \(item.printer?.id ?? "")")
                    row.printerHostname.setText(item.printer?.hostname ?? "")
                }
            }
            if storePrinters.count > 0 {
                loadingGroup.setHidden(true)
                emptyGroup.setHidden(true)
                printersList.setHidden(false)
            } else {
                loadingGroup.setHidden(true)
                emptyGroup.setHidden(false)
                printersList.setHidden(true)
            }
        }
    }
    
}
