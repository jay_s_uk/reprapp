//
//  WatchChannel.swift
//  Runner
//
//  Created by João Martins on 22/12/2020.
//

import Foundation
import WatchConnectivity
import Flutter

class WatchChannel: NSObject, WCSessionDelegate {
    static let channelName = "dac1010Channel"
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    private let session = WCSession.default
    private let controller: FlutterViewController
    private let channel: FlutterMethodChannel
    private var store: [String:StorePrinter] = [:]
    
    init(controller: FlutterViewController) {
        self.controller = controller
        channel = FlutterMethodChannel(name: WatchChannel.channelName,
                                       binaryMessenger: controller.binaryMessenger)
        
        super.init()
        
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
        
        channel.setMethodCallHandler(flutterCallback)
        
        channel.invokeMethod(WatchMessagesConstants.printersList, arguments: nil) { result in
            if let data = result as? String {
                self.processData(method: WatchMessagesConstants.printersList, data: data)
            } else {
                print("Error receiving printers list")
            }
        }
    }
    
    private func flutterCallback(call: FlutterMethodCall, result: @escaping FlutterResult){
        if WatchMessagesConstants.flutterMessages.contains(call.method) {
            self.processData(method: call.method, data: call.arguments as! String)
        }  else {
            print("FlutterMessage not registered for Watch: "+call.method)
        }
    }
    
    func processData(method: String, data: String) {
        switch method {
        case WatchMessagesConstants.printersList:
            let printers = factoryPrintersFromJson(json: data)!
            printers.forEach { printer in
                let keyExists = store[printer.id]
                if keyExists != nil {
                    store[printer.id]?.printer = printer
                } else {
                    store[printer.id] = StorePrinter(printerId: printer.id, printer: printer)
                }
            }
            updateContext(method: method)
        case WatchMessagesConstants.printerJobStatus:
            let dataJobStatus = factoryJobStatusFromJson(json: data)!
            let keyExists = store[dataJobStatus.printerId]
            if keyExists != nil {
                store[dataJobStatus.printerId]?.jobStatus = dataJobStatus
            } else {
                store[dataJobStatus.printerId] = StorePrinter(printerId: dataJobStatus.printerId,
                                                              jobStatus: dataJobStatus)
            }
            updateContext(method: method, printerId: dataJobStatus.printerId)
        case WatchMessagesConstants.printerData:
            let dataPrinterData = factoryPrinterDataFromJson(json: data)!
            let keyExists = store[dataPrinterData.printerId]
            if keyExists != nil {
                store[dataPrinterData.printerId]?.printerData = dataPrinterData
            } else {
                store[dataPrinterData.printerId] = StorePrinter(printerId: dataPrinterData.printerId,
                                                                printerData: dataPrinterData)
            }
            updateContext(method: method, printerId: dataPrinterData.printerId)
        default:
            print("Unknown method: \(method)")
        }
    }
    
    private func updateContext(method: String, printerId: String? = nil){
        if(isActivated()){
            do {
                let storeValues: StorePrinters = Array(self.store.values)
                var context: [String:Any] = [:]
                context[WatchMessagesConstants.method] = method
                context[WatchMessagesConstants.store] = factoryStoreToData(data: storeValues)
                if let id = printerId {
                    context[WatchMessagesConstants.printerId] = id
                }
                try session.updateApplicationContext(context)
//                session.sendMessage(context, replyHandler: nil)
            } catch {
                print(error)
            }
        }else{
            print("Session not active!")
        }
    }
    
    private func isActivated() -> Bool {
        return session.isReachable &&
            session.activationState == WCSessionActivationState.activated
    }
    
}
