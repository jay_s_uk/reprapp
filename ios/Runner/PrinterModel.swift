//
//  PrinterModel.swift
//  Runner
//
//  Created by João Martins on 23/12/2020.
//

import Foundation

typealias Printers = [Printer]

struct Printer: Decodable {
    var id: Int
    var name: String
    var enabled: Bool
    var hostname: String
}

struct JobStatus: Decodable {
    var printerId: Int
    var progress: Double?
    var progressLayer: Int?
    var progressHeight: Double?
    var filename: String?
}

struct PrinterData: Decodable {
    var printerId: Int
    var machineStatus: String
    var tools: [Tool]
    var bed: Bed
    var fans: [Fan]
    
    init(from decoder: Decoder) throws {
        let rawResponse = try RawServerResponse(from: decoder)

        // Now you can pick items that are important to your data model,
        // conveniently decoded into a Swift structure
        id = String(rawResponse.id)
        username = rawResponse.user.user_name
        fullName = rawResponse.user.real_info.full_name
        reviewCount = rawResponse.reviews_count.first!.count
    }
}

struct Bed: Decodable {
    var current: Double?
    var active: Double?
    var standby: Double?
    var state: HeaterState?
    var heater: Int?
}

struct Tool: Decodable {
    var toolIndex: Int
    var current: Double?
    var active: Double?
    var standby: Double?
    var state: HeaterState?
    var heater: Int?
}

struct Fan: Decodable {
    var rpm: Int
    var percent: Int
}

enum HeaterState: String, Decodable {
    case OFF, STANDBY, ACTIVE, FAULT, TUNING, OFFLINE
}

func factoryPrintersFromMessageList(message: String) -> Printers? {
    let printers = try? JSONDecoder().decode(Printers.self, from: Data( message.utf8))
    return printers
}

func factoryPrinterFromMessage(message: String) -> Printer? {
    let result = try? JSONDecoder().decode(Printer.self, from: Data(message.utf8))
    return result
}

func factoryJobStatusFromMessage(message: String) -> JobStatus? {
    let result = try? JSONDecoder().decode(JobStatus.self, from: Data(message.utf8))
    return result
}

func factoryPrinterDataFromMessage(message: String) -> PrinterData? {
    let result = try? JSONDecoder().decode(PrinterData.self, from: Data(message.utf8))
    return result
}

