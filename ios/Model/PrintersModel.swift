
import UIKit

struct StorePrinter: Codable {
    let printerId: String
    var printer: Printer?
    var printerData: PrinterData?
    var jobStatus: JobStatus?
}

typealias StorePrinters = [StorePrinter]

// MARK: - Printer
struct Printer: Codable {
    let id: String
    let name: String?
    let enabled: Bool?
    let hostname: String?
}

typealias Printers = [Printer]

// MARK: - PrinterData
struct PrinterData: Codable {
    let printerId: String
    let status: MachineStatus?
    let tools: [Tool]?
    let bed: Bed?
    let fans: [Fan]?
    
    enum CodingKeys: String, CodingKey {
        case printerId = "printerId"
        case status, tools, bed, fans
    }
}

enum MachineStatus: String, Codable, CustomStringConvertible {
    case READING_CONFIGURATION,
         FLASHING_FIRMWARE,
         HALTED,
         OFF,
         PAUSING_PRINT,
         RESUMING_PRINT,
         PRINT_PAUSED,
         SIMULATING_PRINT,
         PRINTING,
         CHANGING_TOOL,
         BUSY,
         IDLE
    
    var description: String {
        return self.rawValue.replacingOccurrences(of: "_", with: " ").capitalized
    }

}

// MARK: - Bed
struct Bed: Codable {
    let current: Double?
    let set: Double?
}

// MARK: - Tool
struct Tool: Codable {
    let current: Double?
    let set: Double?
    let toolNumber: Int?
}

// MARK: - Fan
struct Fan: Codable {
    let rpm, percent: Int?
}

// MARK: - JobStatus
struct JobStatus: Codable {
    let printerId: String
    let progress: Double?
    let currentLayer: Int?
    let currentHeight: Double?
    let filename: String?
    let timeLeftSeconds: Int?
    
    enum CodingKeys: String, CodingKey {
        case printerId = "printerId"
        case progress, currentLayer, currentHeight, filename, timeLeftSeconds
    }
}

func factoryPrinterDataFromJson(json: String) -> PrinterData? {
    let printerData = try? JSONDecoder().decode(PrinterData.self, from: Data(json.utf8))
    return printerData
}

func factoryJobStatusFromJson(json: String) -> JobStatus? {
    let jobStatus = try? JSONDecoder().decode(JobStatus.self, from: Data(json.utf8))
    return jobStatus
}

func factoryPrintersFromJson(json: String) -> Printers? {
    let printers = try? JSONDecoder().decode(Printers.self, from: Data(json.utf8))
    return printers
}

func factoryStoreToData(data: StorePrinters) -> Data? {
    let data = try? JSONEncoder().encode(data)
    return data
}

func factoryStoreFromData(data: Data) -> StorePrinters? {
    let result = try? JSONDecoder().decode(StorePrinters.self, from: data)
    return result
}
